(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}
    ,i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

//page domain
var currentDomain = document.domain;
//default to prod ID
var uaId = "UA-4512123";

if(currentDomain.toLowerCase().indexOf("www.cq-dev") != -1) {
    uaId = "UA-45112313"; currentDomain="www.cq-dev..com";
}else if(currentDomain.toLowerCase().indexOf("cq-dev") != -1) {
    uaId = "UA-45129123"; currentDomain="cq-dev.com";
}else if(currentDomain.toLowerCase().indexOf("www.cq-qa") != -1) {
    uaId = "UA-4512942123123"; currentDomain="www.cq-qa.com";
}else if(currentDomain.toLowerCase().indexOf("cq-qa") != -1) {
    uaId = "UA-45129123"; currentDomain="cq-qa.com";
}else if(currentDomain.toLowerCase().indexOf("www.cq-uat") != -1) {
    uaId = "UA-451294212312"; currentDomain="www.cq-uat.com";
}else if(currentDomain.toLowerCase().indexOf("cq-uat") != -1) {
    uaId = "UA-4512942123"; currentDomain="cq-uat.com";
}else if(currentDomain.toLowerCase().indexOf("author.cq-prod") != -1) {
    uaId = "UA-451294212312"; currentDomain="author.cq-prod.com";
} else if(currentDomain.toLowerCase().indexOf("qualifications") != -1) {
    uaId = "UA-45129420-1"; currentDomain="qualifications.pearson.com";
}

ga('create', uaId, 'auto', {'allowLinker': true});
ga('require', 'linker');
ga('linker:autoLink', ['pearsonschoolsandfecolleges.co.uk', 'qualifications.pearson.com', 'jotformpro.com', 'submit.jotformpro.com','maxemail.emailcenteruk.com','cq-uat.onepearson.com'] );
ga('send', 'pageview');

function getTitleFromUrl(path) {
    var title = "";
    if(path.indexOf("/") > -1) {
        title   = path.split("/")[path.split("/").length - 1];
        if(title.indexOf(".") > -1) {
            title = (title.split(".")[1])? title.split(".")[0].replace(/-/g, " ") : title.replace("-", " ");
        }
    }

    if(title != "") {
        title = title.charAt(0).toUpperCase() + title.substr(1);
    }

    return title;
}

//General elements to track clicks
var elements_to_track = '.specdownload .btn';
$("#content").on('click', elements_to_track, function() {
    var eventAction, eventLabel, eventCategory, send;
    var that = $(this);

    //This is it
    //console.log("clicked " + that.attr("class"));

    //Retrieve metadata based on the type of element it is
    if (that.attr("class").indexOf("btn-primary") > -1) {
        var path, title;
        path    = that.attr("href");
        
        //get Quals family | Category | Year from the document url
        var urlPathArray = path.split( '/' );
        var newPathname = "";
        for (i = 4; i < urlPathArray.length-1; i++) {
            newPathname += urlPathArray[i];
            if(i < urlPathArray.length-2){
                newPathname += " | ";
            }
        }
        
        title = getTitleFromUrl(path);
        //eventLabel = "File download: " +  title + " : " + path;
        //eventAction = "Download";
        //eventCategory = "Download on: " + getTitleFromUrl(location.href) + " Specification page: " + location.href;
        eventLabel = path;
        eventAction = "Download Button";
        eventCategory = newPathname;
        //eventCategory = getTitleFromUrl(location.href) + " Specification page: " + location.href;


    } else {
        //Another issue
    }

//    console.log("We would like to send this information[ Event Label: " + eventLabel + " Event Action: " + eventAction + " Event Category " + eventCategory + " ] ");

    //Only send if this has been initialized
    if(eventAction && eventLabel && eventCategory) {
        //console.log("We have successfuly sent this information");
        //Compile the object
        var analyticsObj = {
            'hitType':          'event',           // Required.
            'eventCategory':    eventCategory,     // Required.
            'eventAction':      eventAction,       // Required.
            'eventLabel':       eventLabel
        };
//        console.log("Log this object");
       console.log(analyticsObj);

        //Send the event
        ga('send', analyticsObj);
    }
});


//Flyout menu to track clicks
var elements_to_track = '.mega-dropdown a';
$("#content").on('click', elements_to_track, function() {
    var eventAction, eventLabel, eventCategory, send;
    var that = $(this);

    if (that.attr("class").indexOf("flylink") > -1) {
        var path, title;
        path = that.attr("href");
        
        //get the section from the URL
        var urlPathArray = path.split( '/' );
        var newPathname = "";
        newPathname = urlPathArray[urlPathArray.length-2].replace(/-/g, " ");


        title = getTitleFromUrl(path);
        //eventLabel = getTitleFromUrl(location.href);
        eventLabel = path;
        eventAction = "Flyout";
        eventCategory = newPathname;
        //eventCategory = title+ " | " + path;
    } else {
        //Another issue
    }

    console.log("We would like to send this information[ Event Label: " + eventLabel + " Event Action: " + eventAction + " Event Category " + eventCategory + " ] ");

    //Only send if this has been initialized
    if(eventAction && eventLabel && eventCategory) {
        //console.log("We have successfuly sent this information");
        //Compile the object
        var analyticsObj = {
            'hitType':          'event',           // Required.
            'eventCategory':    eventCategory,     // Required.
            'eventAction':      eventAction,       // Required.
            'eventLabel':       eventLabel
        };
//        console.log("Log this object");
//        console.log(analyticsObj);

        //Send the event
        ga('send', analyticsObj);
    }
});

// Generic event tracker
// Implementation
// Attach listener to region - onclick="gaEventTracker(event)"
// Set element where event is tracked - data-event-category="Book now" data-event-action="Event Booking" data-event-label="{{ edexcelLink }}"
function gaEventTracker (event) {
	if(event){
		var e = this.event;
		if(e){
			var target = e.target.dataset;

			if ( target && target.eventAction != undefined ) {
					if ( target.eventAction != undefined && target.eventCategory != undefined ) {
							var analyticsObj = {
									'hitType':          'event',           // Required.
									'eventCategory':    target.eventCategory,     // Required.
									'eventAction':      target.eventAction,       // Required.
									'eventLabel':       target.eventLabel
							};
	//            console.log(analyticsObj);
							//Send the event
							ga('send', analyticsObj);
					}
			} else {
					//There is no event action defined inside the data-event-target element.
			}
		}
    
	}
}

//

function setAttributes(el, attrs) {
  for(var key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
}

//Attach onclick event to the body
$('body').attr("onclick", "gaEventTracker(event)");

var allHref = $("[href][target]");
var allMailTo = $("[href*='mailto:']");

$.each(allMailTo, function (index, value) {

    var mailAttr = this.attributes.href.value,
    studentEmail = mailAttr.indexOf("student"),
    teacherEmail = mailAttr.indexOf("teacher");

    if ( studentEmail != -1 ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Student - Email", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( teacherEmail != -1 ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Teacher - Email", "data-event-action" : "Contact Us", "data-event-label": label});
    }

});

$.each(allHref, function (index, value) {
    var hrefAttr = this.attributes.href.value,
    twitterLink = hrefAttr.indexOf("twitter"),
    facebookLink = hrefAttr.indexOf("facebook"),
    student = hrefAttr.indexOf("student"),
    Student = hrefAttr.indexOf("Student"),
    teacher = hrefAttr.indexOf("teach"),
    Teacher = hrefAttr.indexOf("Teach");

    if ( twitterLink != -1 && Student != -1 && teacher == -1 && Teacher == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Student - Twitter", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( twitterLink != -1 && student != -1 && teacher == -1 && Teacher == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Student - Twitter", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( twitterLink != -1 && Teacher != -1 && student == -1 && Student == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Teacher - Twitter", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( twitterLink != -1 && teacher != -1 && student == -1 && Student == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Teacher - Twitter", "data-event-action" : "Contact Us", "data-event-label": label});
    }




    if ( facebookLink != -1 && Student != -1 && teacher == -1 && Teacher == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Student - facebook", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( facebookLink != -1 && student != -1 && teacher == -1 && Teacher == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Student - facebook", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( facebookLink != -1 && Teacher != -1 && student == -1 && Student == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Teacher - facebook", "data-event-action" : "Contact Us", "data-event-label": label});
    }

    if ( facebookLink != -1 && teacher != -1 && student == -1 && Student == -1  ) {
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": "Teacher - facebook", "data-event-action" : "Contact Us", "data-event-label": label});
    }

});


var URLPath = window.location.pathname;

var feedbackComplaints = URLPath.indexOf("support/contact-us/feedback-and-complaints.html");

if ( feedbackComplaints != -1 ) {
    var mailTo = $("[href*='mailto:']");
    $.each(mailTo, function (index, value) {
        setAttributes(this, {"data-event-category": "Feedback and Complaints - Email", "data-event-action" : "Contact Us", "data-event-label": value});
    });
}

var socialMediaTwitter = URLPath.indexOf("footer/social-media.html");

if ( socialMediaTwitter != -1 ) {
    var twitter = $("[href*='twitter']");
    $.each(twitter, function (index, value) {
        var siblingTdText = this.parentNode.parentNode.parentNode.children[0].innerText + " ";
        setAttributes(this, {"data-event-category": siblingTdText + "- Twitter", "data-event-action" : "Contact Us", "data-event-label": value});
    });
    var facebook = $("[href*='facebook']");
    $.each(facebook, function (index, value) {
        var text = this.innerText + " ";
        setAttributes(this, {"data-event-category": text + "- Facebook", "data-event-action" : "Contact Us", "data-event-label": value});
    });
    var youtube = $("[href*='youtube']");
    $.each(youtube, function (index, value) {
        var text = this.innerText + " ";
        setAttributes(this, {"data-event-category": text + "- YouTube", "data-event-action" : "Contact Us", "data-event-label": value});
    });
}


$.each(allHref, function (index, value) {
    var str = $(this)[0].attributes.href.value;
    var res = str.substring(0, 1);
    var ukpURL = str.indexOf("uk.pearson.com");
    var twitter = str.indexOf("twitter");
    var facebook = str.indexOf("facebook");

    var currentPage = window.location;
    if ( res != "/" && ukpURL == -1 && facebook == -1 && twitter == -1 ) {
        var category = $(this)[0].innerText;
        var label = $(this)[0].href;
        setAttributes(this, {"data-event-category": category + " - " + currentPage , "data-event-action" : "External link Event", "data-event-label": label});
    }
});
