/*
    Object designed to encapsulate the functions needed to add create a video modal screen

    Functions include:
        - Changing the video presently playing
        - Checking the current state of the modal player
        - Updating the caption of the current playing video
        - Cloning the videos into the modal window
        - Closing the modal window
*/
var Youtube = {};
var scrollNode = $.browser.webkit ? $('body') : $('html');
Youtube.Modal = function(element) {
    var that        = this;
    this.element    = element;
    this.oldElement = "";
    this.videoPLayer= "";
    this.vidId      = "";

    this.close      = (this.element).find(".close");
    this.playlist   = (this.element).find(".playlist");
    this.visible    = false;

    //This will poll the to find the element over time, once it is found it will add the element to the property
    addElementToProperty(this, "modalCover", ".modal-cover", 500);

    //Update the modal list as well play the video
    function playOnUpdate (event) {
        event.target.playVideo();
    }

    //Function called when the youtube video updates
    this.updateModalPlaylist = function(id) {
        var playlist = $(".modal-cover .modal .playlist");
        playlist.find("a").show();
        //Hide the active video in the playlist
        var activeVid = (playlist).find(".vid_" + id).hide();
        //Set the title for the modal window
        $(".modal-cover .modal .modal-title").html((activeVid).data("vidtitle"));
    };

    // Determines when a click event is on the current modal element or a click off the element
    // Takes the argument of the function event, determines where the click event has
    // been fired and closes the modal when necessary
    this.clickModal = function(x) {
        var clickedModal = false;
        if($(x.target).is(this.element) || $(x.target).parents().is(this.element)) {
            clickedModal = true;
        } else if(x.target.localName == "img" && $(x.target).parents().length == 0) {
            //This is a detached image file that had been destroyed do not close on its account
            clickedModal = true;
        } else {
             this.closeModal();
        }
     };

    //Search for the elements that are not quite on the page when you load it
    //once they appear then add them to the add the element to the property
    function addElementToProperty(that, propery, selector, wait) {
        if($(selector).length) {
            that[propery] = $(selector);
        } else {
            setTimeout(function () {
                addElementToProperty(that, propery, selector, wait);
            }, wait);
        }
    }

    // Click function -- when the cross is clicked then the modal closes
    (that.close).on("click", function(event){
        event.preventDefault();
        that.closeModal();
    });

    // Takes the elements from within the modal and then adds them to the modal that
    // it needs to be a part of and sets up the video to be played
    /*this.cloneModal = function() {
        var that = this;
        var clone = that.element.clone(true, true);
        that.oldElement = that.element;
        that.element = clone;

        that.element.appendTo(this.modalCover);
    };*/

    // Resets the values of the component so that it ready to be used again
    /*this.unCloneModal = function() {
        this.element = this.oldElement;
        this.videoPLayer = ""; this.vidId = ""; this.modalCover.html('');
    };*/

    // Set the video to be played in the modal window
    this.cueVideo = function(vidId, url){
        var that = this;
        //clear the video player
        this.videoPLayer = "";

        //if a url image is successfully passed in then render the image and then play the video afterwards
        if(url && url !== "") {
            this.vidId = vidId;

            //This creates the image for preview
            that.element.find(".video-content").html("<img src='" + url + "' />");

            that.element.find(".video-content").on("click.pearsonyoutube", function(e) {
                e.preventDefault(); that.renderVideo(true);
            });
        } else {
            //Unbind the click event from the video element
            that.element.find(".video-content").unbind("click.pearsonyoutube");

            //Set the video to play
            this.vidId = vidId;

            //create the video player
            this.renderVideo();
        }

        //Always update the modal playlist
        this.updateModalPlaylist(vidId);
    };

    //Create video element on the page via a "random id"
    this.renderVideo = function(play) {
        var that = this;

        //Create the video element to be used
        var id = "vid_" + parseInt(Math.random() * 1000, 10);
        that.element.find(".video-content").html("<div class='video' id='" + id + "'></div>");

        //If the user sets play to true then we want to generate the video and then play it afterwards
        var vidEvent = (play) ? playOnUpdate : function(event){  };

        that.videoPLayer = new YT.Player(id, {
            height:     '100%',
            width:      '100%',
            videoId:    that.vidId,
            fs: 0,
            events: {
                'onReady':          vidEvent,
                'onStateChange':    vidEvent,
            }
        });

        //Fit vids get the correct aspect ratio for videos across all platforms
        if($(this.element).fitVids) {
            $(this.element).fitVids();
        }

        //Resize the modal
        that.adjustModal();
    }

    // Find out whether the modal is active or not based on its visibility
    this.isActive = function() {
        return this.visible;
    };

    // Change the caption that is currently being displayed on the modal screen
    this.updateCaption = function(linkElement) {
        (this.element).find(".caption").html((linkElement).data("vidcaption"));
    };

    //Resizes and moves the modal window depending on the window height and width
    this.adjustModal = function() {
        var that = this;
        //Get the modal cover
        var modalObject = $('.modal-cover .modal');

        // Calculate center position of modal (vertical and horizontal)
        if(that.element.outerHeight() > (scrollNode).height()) { // If height of modal is greater than
         (that.element).css('top', 0);
         (that.element).css('margin-top', 0);
        } else {
         (that.element).css('top', '50%');
         (that.element).css('margin-top', -(that.element.outerHeight()/2));
        }
        (that.element).css('margin-left', -(that.element.outerWidth()/2));
        (that.element).css('visibility', 'visible');
    }

    // Open the modal by cloning the content from the page and then adding to the modal window,
    // from there we resize the modal window so that it can be seen in the appropriate viewer
    this.openModal = function() {
         var that = this; this.visible = true;
         (this.modalCover).on("click.pearsonyoutube", function(event){ that.clickModal(event); });

         // this.cloneModal();

         // Clones the modal to the modal cover
         var clone = that.element.clone(true, true);
         that.oldElement = that.element;
         that.element = clone;
         that.element.appendTo(this.modalCover);

         // Set body class for modal mode
         (scrollNode).addClass('modal-enabled');

         // Fade in modals
         (this.modalCover).css('top', (scrollNode).scrollTop());

         // Need to access modal height here since it is the only time the height is a +ve number
        (this.modalCover).fadeIn("fast", function(){
            that.adjustModal();

            (that.element).fadeIn("fast");

            $(window).resize(function() {
                that.adjustModal();
            });
        });
     };

    // Close the modal by un-cloning it  and then resetting the
    // properties of the modal screen so that it can now be hidden
    this.closeModal = function() {
        var that = this; that.visible = false;

//      this.unCloneModal();

        this.element = this.oldElement;
        this.videoPLayer = ""; this.vidId = ""; this.modalCover.html('');

        (scrollNode).removeClass('modal-enabled');

        (that.modalCover).fadeOut("fast", function(){
            (that.modalCover).attr("style", "");
        });

        //Get rid of the click event for the modal
        (this.modalCover).unbind("click.pearsonyoutube");
    };
};