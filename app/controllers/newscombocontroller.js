'use strict';

/* Controllers */
var pearsonControllers = angular.module('pearsonNewsComboController', ['pearsonServices','pearsonCourseMaterialServices', 'pearsonFilters']);


pearsonControllers.controller('newsComboCtrl', [ '$scope', 'QuerySolr','serializeForm','resolveState','$filter','$location',
    function($scope, QuerySolr, serializeForm, resolveState, $filter, $location) {
		
		/*Trigger the page init events*/
	    $scope.loading = false;
	
		$scope.newsListQuery = function(){
			//Define servlet to be used for search in solr
			QuerySolr.configQuery("/services/pearson/solr/GET.servlet");
			
			//Perform solr query to return the news
			QuerySolr.query({fq: $scope.baseQueryTags, q: $scope.pageTypes, sort: "date desc"}, function(data){
				if(data.searchResults.solrDocuments.length > 0) {
					$scope.newsList = data.searchResults.solrDocuments;

					if($scope.newsList.length <= $scope.STATE.displayLimit)  {
					          $scope.showShowMore = false;
					}
				}
			});
		}
		
		//This function will allow the newslist to init
		$scope.init = function (pageTypes, componentTags, numberToShow, selectedArticle){
			
			//Set the state that is going to used in the other services
		    //When you init the application
		    $scope.STATE = ($scope.STATE || {});
		    $scope.STATE.displayLimit = parseInt(numberToShow);
		    $scope.searchFailed = false;
		    $scope.loading = true;
		    $scope.showShowMore = true;
		    $scope.numberToShow = numberToShow;
		    $scope.selectedArticle = selectedArticle;
		
		    //Add page types to array ready for query
		    if(pageTypes != null && pageTypes.length > 0){
			    var pageTypesInputArray = pageTypes.split(",");
				var pageTypesOutputArray = [];
				for(var i = 0; i < pageTypesInputArray.length; i++){
					pageTypesOutputArray.push("pagetype:\"" + pageTypesInputArray[i] + "\"");
				}
				$scope.pageTypes = pageTypesOutputArray;
		    }
			
		    //Setup a base array ready for query
			var baseQueryTagsArray = [];
			baseQueryTagsArray.push("category:\"page-type:news\"");
			baseQueryTagsArray.push("type:\"cq:page\"");
			
			//If tags are specified add them to the base query
			if(componentTags != null && componentTags.length > 0){
				var componentTagsArray = componentTags.split(",");
				for(var i = 0; i < componentTagsArray.length; i++){
					baseQueryTagsArray.push("category:\"" + componentTagsArray[i] + "\"");
				}
			}
			$scope.baseQueryTags = baseQueryTagsArray;
			
			//Perform Solr Search
			$scope.newsListQuery();
			
			//finished loading
			$scope.loading = false;
		}

        $scope.showAll = function(){
            $scope.STATE.displayLimit = $scope.newsList.length;;
            $scope.showShowMore = false;
        }

	} 
]);