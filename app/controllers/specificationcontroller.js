"use strict";
/* global $ */

/* Controllers */
var pearsonControllers = angular.module('pearsonSpecificationController', [ 'pearsonServices', 'CourseMaterialsConstants', 'pearsonCourseMaterialFilters']);

pearsonControllers.controller('RelatedQualificationsCtrl', [
    '$scope',
    '$log',
    '$timeout',
    'RelatedQualifications',
    '$element',
    '$filter',
    function ($scope, $log, $timeout, RelatedQualifications, $element, $filter) {
        $scope.getPages = function() {
            $scope.subjectTags = $('#related_qualifications').data('subject-tags');
            $scope.currentPage = $('#related_qualifications').data('current-page');
            $scope.geoTargetComponent = $('#related_qualifications').data('geo-target');
            $scope.specsByQualif = {};
            $scope.specsByLevel = {};
            $scope.showQualifSort = false;
            $scope.showLevelSort = false;
            $scope.showComponent = false;
            $scope.globalCount = 0;

            if (!$scope.geoTargetComponent) {
                var geoLocationStore = CQ_Analytics.ClientContextMgr.getRegisteredStore("geolocation");
                $scope.location = geoLocationStore.getProperty("address/country", false);

                if ($scope.location === "United Kingdom" || $scope.location == undefined) {
                    $scope.subjectTags = $scope.subjectTags + ",Pearson-UK:Location/UK";
                } else {
                    $scope.subjectTags = $scope.subjectTags + ",Pearson-UK:Location/International";
                }
            }

            RelatedQualifications.getSOLRSearchResult($scope.subjectTags).then(function(response) {
                //Clean up the result set so that the results are more robust
                var docs = $filter('resultCleaner')(response.searchResults.solrDocuments);
                $scope.specsByQualif = RelatedQualifications.getSpecificationsGroupedByFacet(docs,
                    $scope.currentPage,
                    "Pearson-UK:Qualification-Family/",
                    true);

                $scope.specsByLevel = RelatedQualifications.getSpecificationsGroupedByFacet(docs,
                    $scope.currentPage,
                    "Pearson-UK:Level/",
                    false);

                $scope.showQualifSort   = $scope.specsByQualif.elements.length > 0;
                $scope.showLevelSort    = $scope.specsByLevel.elements.length > 0;
                $scope.showComponent    = $scope.showLevelSort || $scope.showQualifSort;
                //            $scope.globalCount      = response.searchResults.solrDocuments.length;

                if($scope.showQualifSort) {
                    $scope.qualificationsList = $scope.specsByQualif.elements;
                    $scope.mode = "qual";
                } else if($scope.showLevelSort) {
                    $scope.qualificationsList = $scope.specsByLevel.elements;
                    $scope.mode = "lvl";
                }
                //Set the length of the global count based on all the specs that came back
            })
        };

        CQ_Analytics.ClientContextUtils.onStoreRegistered("geolocation", function() {
            var geoLocationStore = CQ_Analytics.ClientContextMgr.getRegisteredStore("geolocation");

            geoLocationStore.addListener("update", function (action, property) {
                if (action === "update") {
                    if (property === "address/country") {
                        var seeAlso_country = geoLocationStore.getProperty("address/country", false);
                        if (seeAlso_country != "" && seeAlso_country != $scope.location) {
                            console.log('Specification Controller listener country:' + seeAlso_country);
                            $scope.getPages();
                        }
                    }
                }
            }, this);

            $scope.getPages();
        });


        $scope.$watch("qualificationsList", function(newval, oldval) {
            $scope.globalCount = 0;
            angular.forEach(newval,
                function(data) {
                    for (var i = 0; i < data.subjects.length ; i++) {
                        $scope.globalCount += data.subjects[i].specificationsList.length ;
                    }
                });
        });

        $scope.toggleSort = function(val) {
            $scope.mode = val;

            if(val == "qual") {
                $scope.qualificationsList = $scope.specsByQualif.elements;
            }else if(val == "lvl") {
                $scope.qualificationsList = $scope.specsByLevel.elements;
            }

            $scope.$apply();
        };

        $scope.initAccordion = function(){
            $($element).find(".accordion-body").each(function(){

                Pearson.setupAccordionDisplay($(this));
            });
        }
    }
]);

pearsonControllers.controller('facetListCtrl', [
    '$scope',
    'QuerySolr',
    '$filter',
    'facetHandler',
    'CONST',
    function($scope, QuerySolr, $filter, facetHandler, CONST) {
      $scope.categFacets = {};
      var queryTags, displayType;
      $scope.hideApp = true;

      function addCategoryPrefix(list, addQuotes){
        var newList = [];
        if(addQuotes){
          for(var i = 0; i < list.length; i++){
            newList.push("category:" + "\"" + list[i] + "\"");
            }
        } else {
          for(var i = 0; i < list.length; i++){
                newList.push("category:" + list[i]);
          }
        }
        return newList;
      }

        $scope.init = function(type, tags, view, excludespecurl){
            queryTags = tags.replace("[", "").replace("]", "").split(", ");
            displayType = type;

            //The primary tags to search on
            var queryFilter = [ "Pearson-UK:Qualification-Subject", "Pearson-UK:Specification-Code",
                "Pearson-UK:Accreditation-From-date", "Pearson-UK:Qualification-Family" ];

          //For the course materials query we want to get the wider batch of documents and then filter then by the
          //"Qualification Size" of "Skill", then retrieve the facets on the filtered set of results and display them
          if(displayType == "coursematerials") {
              //Get the tags that match - QF/QS/SC/AFD a wider value search
              $scope.queryTags      = $filter("getLooseMatch")(queryTags, queryFilter);
              //Get the tags that do not match - we then filter the returned documents
              $scope.docFilter      = $filter("getLooseExclude")(queryTags, queryFilter);
              //The filter for the results on the spec page
              $scope.resultsFilter  = { facet: "Pearson-UK:Category" };
              //Add a prefix that makes the query tags digestible for the search
              $scope.queryTags = addCategoryPrefix($scope.queryTags, false);

              QuerySolr.configQuery("/services/pearson/search/GET.servlet");
              QuerySolr.query({filterQuery: $scope.queryTags }, function(data) {
                    //Filter the results by the docFilter = the "non-primary" tags
                    var docs    = $filter("searchResultsFilter")(data.searchResults.solrDocuments, $scope.docFilter);
                    //Get the facets from this set of documents
                    var facets  = facetHandler.getFacetsFromDocuments(docs);

                    //Simple view is to display document list instead of a facet list
                    if(view == "simple") {
                        $scope.resultsFilter  = { type: "dam:Asset" };
                        //If there is a value in the exclude url
                    	var categFacets = docs;
                        if(excludespecurl) {
                            categFacets = $filter("getLooseExclude")(docs, { "url" : excludespecurl });
                        } 
                        
                        //Filter content by dam assets
                        categFacets = $filter("getLooseMatch")(categFacets, { "type" : "dam:Asset" });
                        var exclusions = CONST.categoryExclusionList;
                    	// Loop thought the exclusion list and exclude the docs under exclusion list
                    	for (var x = 0; x < exclusions.length; x++) {
                    		var categoryExclusion = 'Pearson-UK:Category/'+exclusions[x];
                    		categFacets = $filter("getLooseExclude")(categFacets,{ "category" : categoryExclusion });
                    	}
                    	//assign it to scope
                    	$scope.categFacets=categFacets;                                                
                    } else {
                        //Don't include facets that are in the exclude list | push the facets the view "categFacets"
                        $scope.categFacets = $filter("getLooseExclude")(facets, { "facet" : CONST.categoryExclusionList });
                    }

                    //If the facets are not undefined then show the application
                    if($scope.categFacets !== undefined) {
                        $scope.setHide(($scope.categFacets.length > 0) ? false : true);
                    }
              });
          }
          if(displayType == "resources") {
              queryTags = addCategoryPrefix(queryTags, true);
              $scope.queryTags = queryTags;
              $scope.resultsFilter = { facet: "Pearson-UK:Publisher" };

              queryTags = $filter('getLooseMatch')(queryTags, ["Qualification-Family", "Qualification-Subject",
                  "Accreditation-From-date"]);

              QuerySolr.configQuery("/services/pearson/solr/GET.servlet");
              queryTags.push("type:\"cq:PaidResource\"");
              QuerySolr.query({ fq:queryTags, facet:true , facetField: "category" }, function(data){
                    $scope.categFacets = data.searchResults.facets;
                    if($scope.categFacets !== undefined) {
                        $scope.setHide(($scope.categFacets.length > 0) ? false : true);
                    }
              });
        }
      };

        $scope.repeatCallback = function(){
            var result = $filter("getLooseMatch")($scope.categFacets, $scope.resultsFilter);
            if(result.length < 1){
                $scope.setHide(true);
            }else {
                $scope.setHide(false);
            }
        }

      $scope.setHide = function(hide){
            $scope.hideApp = hide;
      }
}]);

pearsonControllers.controller('PaidResourcesCtrl', [
  '$scope',
    'Documents',
    'serializeForm',
    '$filter',
    'QuerySolr',
    function($scope, Documents, serializeForm, $filter, QuerySolr) {
    $scope.documents = {};
    $scope.resultsLength = 0;

    var filterQuery = ["type:\"cq:PaidResource\""];

    var tcQuery = QuerySolr;
    tcQuery.configQuery("/services/pearson/solr/GET.servlet");

        $scope.init = function(id) {
          $scope.queryTags = serializeForm(id, 'primary');
          var result = $filter('getLooseMatch')($scope.queryTags, ["Qualification-Family", "Qualification-Subject",
                "Accreditation-From-date"]);

          tcQuery.query({ fq: filterQuery.concat(result), facet: true, facetField: "category" }, function(data){
          $scope.documents = data.searchResults.solrDocuments;
          //console.log($scope.documents);
        });
        };
  }
]);