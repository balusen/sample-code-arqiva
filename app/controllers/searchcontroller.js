'use strict';

/* Controllers */
var searchController = angular.module('searchController', ['pearsonSearchServices', 'pearsonPastPapersServices',
    'pearsonCourseMaterialFilters']);

searchController.controller('searchController', ['$scope',
    '$rootScope',
    '$log',
    '$filter',
    '$location',
    'SearchResults',
    'pastPapersEvents',
    'resolveShowMore',

    function ($scope, $rootScope, $log, $filter, $location, SearchResults, pastPapersEvents, resolveShowMore) {
        $scope.init = function () {
            $scope.showCounter = false;
            var queryString = $location.search()['queryString'];
            if (queryString != null && queryString != undefined) {
                $scope.doSearch(queryString);
            }
        };

        $scope.doSearch = function(queryString) {
            $scope.showCounter = false;
            $scope.searchFailed = false;
            $scope.queryString = queryString;
            $scope.loading = true;
            if (queryString != "" && queryString != undefined) {
                $location.search('queryString', queryString);
                $scope.documents = $scope.getDocuments(queryString);
            } else {
                $scope.searchFailed = true;
                $scope.loading = false;
            }
        };

        $scope.getDocuments = function(queryString) {

            SearchResults.getDocuments(queryString, function(data) {
                if (data.searchResults.solrDocuments.length) {
                    $scope.documents = data.searchResults.solrDocuments;
                    $rootScope.STATE = ($rootScope.STATE || {});
                    $rootScope.data = data.searchResults.solrDocuments;
                    $rootScope.STATE.resultsLength = data.searchResults.solrDocuments.length;
                    $rootScope.STATE.displayLimit = 10;
                    resolveShowMore();

                    $rootScope.pastPapersEvents = pastPapersEvents;
                    $scope.showCounter = true;
                } else {
                    $rootScope.STATE = ($rootScope.STATE || {});
                    $rootScope.STATE.resultsLength = 0;
                    $scope.searchFailed = true;
                    $scope.showCounter = false;
                    resolveShowMore();
                }

                $scope.loading = false;
            });
        };

        //This is a function called from the "show more" button on the page, if there are more than x results on
        // the page, clicking show more will allow the display of more results : current increment = 20
        $scope.showMore = function(){
            var inc = 20;
            var limit = $scope.STATE.displayLimit;
            var resultLength = $scope.documents.length;

            $scope.STATE.displayLimit = limit + inc;

            resolveShowMore();
        };
    }
]);