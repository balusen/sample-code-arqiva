'use strict';

/* Controllers */
var pearsonNextKeyDatesController = angular.module('pearsonNextKeyDatesController', ['pearsonServices']);

pearsonNextKeyDatesController.controller('NextKeyDatesCntl', [
    '$scope',
    'QuerySolr',
    '$element',
    'resolveState',
    '$filter',
    function ($scope, QuerySolr, $element, resolveState, $filter) {

		//Capture the Dom scope in variable
		var appDom = $($element);

        $scope.resultsFilter = function(value) {
            if (new Date(value.fromdate) > new Date()) {
                return true
            }
            return false;
        }

        var tcQuery = QuerySolr;

        $scope.init = function (noOfItems) {

            $scope.STATE = ($scope.STATE || {});
            $scope.STATE.showmore = false;
            $scope.STATE.resultsLength = 0;
            $scope.STATE.displayLimit = noOfItems;

            var filterQuery = ["type:\"cq:KeyDate\""];

            tcQuery.configQuery("/services/pearson/solr/GET.servlet");

            tcQuery.query({ fq: filterQuery, facet: true, facetField: "category" }, function(data) {
                $scope.documents = data.searchResults.solrDocuments;
                $scope.STATE.resultsLength = data.searchResults.solrDocuments.length;
                $scope = resolveState($scope);
            });
        };
    }
]);
