'use strict';

/* Controllers */
var pearsonControllers = angular.module('pearsonQualificationController', ['pearsonQualificationFilters', 'pearsonQualificationDirectives']);

pearsonControllers.controller('AccordionCtrl', [
    '$rootScope',
    '$scope',
    '$log',
    '$filter',
    '$http',
    function ($rootScope, $scope, $log, $filter, $http) {
        var defDesc = "";
        $scope.resourcePath;

        $scope.init = function(defaultTag, defaultDescription, resourcePath, showmoreLimit){
        	$rootScope.groupByFacet = defaultTag;
            defDesc = defaultDescription == 'true'? true : false;
            var resourcePath = resourcePath;

            var lastSlashIndex = location.pathname.lastIndexOf(".html");
        	var path = location.pathname.substring(0,lastSlashIndex);
        	path = path + '.findqualification?resourcePath=' + resourcePath;

            $http.get(path).
                success(function (data) {
                    $scope.qualifications = data.qualFamiliesArray;
                    
                    $rootScope.STATE = ($rootScope.STATE || {});
                    $rootScope.STATE.displayLimit = 10;
                    $rootScope.STATE.resultsLength = data.qualFamiliesArray.length;
                    
                    if(showmoreLimit != null && !("" === showmoreLimit)){
                        $rootScope.STATE.displayLimit = showmoreLimit;	
                    }
                    
                    if($scope.qualifications.length > $rootScope.STATE.displayLimit) {
                         $rootScope.STATE.showmore = true;
                     }else{
                        $rootScope.STATE.showmore = false;
                        $rootScope.STATE.displayLimit = $scope.qualifications.length;
                     }

                });

        }



        //This is a function that will be called by the "repeat-callback" directive,
        //once the page has finished printing out all the elements on the page will
        //now respond
        $scope.initShowDescription = function(){
            $scope.showDescription = defDesc;
        }

        $scope.showAll = function(){
            $rootScope.STATE.displayLimit = $scope.qualifications.length;
            $rootScope.STATE.showmore = false;
        }

        $scope.showCount = function(){
            $rootScope.STATE.displayLimit = $scope.qualifications.length;
            $rootScope.STATE.showmore = true;
        }

        $scope.showLess = function(){
            $rootScope.STATE.displayLimit = 10;
            $rootScope.STATE.showmore = true;
        }


    }
]);