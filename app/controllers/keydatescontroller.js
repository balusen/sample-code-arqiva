'use strict';

/* Controllers */
var pearsonKeyDatesController = angular.module('pearsonKeyDatesController', ['pearsonServices']);

pearsonKeyDatesController.controller('KeyDatesCntl', [
    '$scope',
    'QuerySolr',
    '$location',
    '$element',
    'resolveState',
    '$filter',
    function ($scope, QuerySolr,$location, $element, resolveState, $filter) {

		//Capture the Dom scope in variable
		var appDom = $($element);
        $scope.Type = "cq:KeyDate";
        $scope.qualFamilyFacet = "Pearson-UK:Qualification-Family";
        $scope.qualification, $scope.documents; $scope.facets;
        $scope.resultsFilter = "";
        $scope.listFilter1 = { "facetKey":"Qualification-Family" };
        $scope.facetFilter1 = { "facet":"Exam-Series" }, $scope.facetFilter2 = {"facet":"FromDate" };


        //Create a new QuerySolr and point it to the correct servlet
        var tcQuery = QuerySolr;

        $scope.init = function (filterTags) {

            var filterQuery = ["type:\"cq:KeyDate\""];

            if(filterTags != null && filterTags !== ""){
                $scope.filterTags = filterTags;

                var tagArray = filterTags.split(",");
                if(tagArray.length > 0){
                    for (var x = 0; x < tagArray.length; x++){
                        filterQuery.push( "category:\"" + tagArray[x] + "\""  );
                    }
                }

            }

            tcQuery.configQuery("/services/pearson/solr/GET.servlet");

                tcQuery.query({ fq: filterQuery, facet: true, facetField: "category" }, function(data){
                	console.log(' Inside Key dates . . . '+data.searchResults);
                    $scope.qualification = data.searchResults.facets;
                });
                
                setUpSteps();
        };


        $scope.openAccordion = function (tabId) {
            $('.keyevents div.ui-accordion-content').slideUp('fast');
            var tabDiv = tabId + " > div.ui-accordion-content";
            $(tabDiv).slideDown('fast');

            // Manage current classes
            $('.step').removeClass('current');
            $(tabDiv).parent().removeClass('selection');
            $(tabDiv).parent().addClass('current');
            $scope.selectedQualificationFamily = "";
            $scope.keyDatesPageSetup();
            $scope.resetSort();
        };


        $scope.showMore = function(){

            var inc = 20;
            var limit = $scope.STATE.displayLimit;
            var resultLength = $scope.documents.length;

            $scope.STATE.displayLimit = limit + inc;

            $scope = resolveState($scope);
        };

        $scope.keyDatesPageSetup = function(){
            //Set the state that is going to used in the other services
            //When you init the application
            $scope.STATE = ($scope.STATE || {});
            $scope.STATE.displayLimit = 20;
            $scope.STATE.showmore = false;
            $scope.STATE.resultsLength = 0;
        };
        
        function encodeAMP(list) {
            var cleanList = [];
            angular.forEach(list, function (element) {
                element = element.replace("&", "%26");
                cleanList.push(element);
            });
            return cleanList;
        }
        
        function setUpSteps(){
        	var selectedQualification = $location.search()['stp1'];
        	if(selectedQualification){
        		$scope.getKeyDatesSearch(selectedQualification);
        	}
        }
        
        $scope.getKeyDates = function (qualification) {
        	console.log("Selected qualification",qualification);
        	var urlString = [];
        	urlString.push("stp1=" + qualification);
        	urlString = encodeAMP(urlString);
            $location.url("?" + urlString.join("&"));
        };
        
        $scope.$watch(function () {
            return $location.search();
        }, function (newUrl, oldUrl) {
        	console.log("Inside the location watch function");
        	if(JSON.stringify(newUrl) === JSON.stringify(oldUrl) && document.readyState == "complete") { return; }
        	setUpSteps();
        	
        	
        });
        
        

        $scope.getKeyDatesSearch = function (qualification) {
            $('.keyevents div.ui-accordion-content').slideUp('fast');
            $('#step2 > div.ui-accordion-content').slideDown('fast');

            // Manage current classes
            $('.step').removeClass('current');
            $('#step2').addClass('current');
            // Mark progressed steps
            $('#step1').addClass('selection');

            $scope.selectedQualificationFamily = (qualification).replace(/-/g, ' ');

            var stepQuery = ["type:\"cq:KeyDate\""];
            stepQuery.push("category:\"" + $scope.qualFamilyFacet + "/" + qualification +"\"");

            if($scope.filterTags && $scope.filterTags !== ""){

                var tagArray =  $scope.filterTags.split(",");
                for (var x = 0; x < tagArray.length; x++){
                    stepQuery.push( "category:\"" + tagArray[x] + "\""  );
                }
            }

			tcQuery.query({ fq: stepQuery, facet: true, facetField: "category" }, function(data){
				console.log("Inside the second query.....");
				$scope.documents = data.searchResults.solrDocuments;
				$scope.documents = $filter('removeOldKeyDates')($scope.documents);
				$scope.facets = data.searchResults.facets;
                $scope.STATE.resultsLength = data.searchResults.solrDocuments.length;
                $scope = resolveState($scope);
                
            });
        };

		$scope.changeDropDown = function(attrValue, attrType){
			$scope.keyDatesPageSetup();
			if(attrType == "clear"){
				$scope.resultsFilter = "";
			}else if(attrType == "location"){
            	$(appDom).find("#dateDrpdn").val('');
            	attrValue.replace(/ /g, '-');
                $scope.resultsFilter = { "category": attrValue };
            }else if(attrType == "date"){
            	$(appDom).find("#locationDrpdn").val('');
                var Datefilter = $filter('date');
                $scope.resultsFilter = function(value){
                    //results whos month == attr.filter
                    var date = Datefilter(value.fromdate, 'MMMM yyyy');
                    if(date == attrValue){
                        return true
                    }
                    return false;
                }
            }
		}

		$scope.resetSort = function(){
			$scope.keyDatesPageSetup();
			$scope.resultsFilter = "";

			$(appDom).find("#dateDrpdn").val('');
			$(appDom).find("#locationDrpdn").val('');

		}

        $scope.keyDatesPageSetup();
    }
]);
