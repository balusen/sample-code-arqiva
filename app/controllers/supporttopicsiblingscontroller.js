'use strict';

/* Controllers */
var pearsonSupportTopicSiblingsController = angular.module('pearsonSupportTopicSiblingsController', []);

pearsonSupportTopicSiblingsController.controller('SupportTopicSiblingsCtrl', [
    '$scope',
    'QuerySolr',
    function ($scope, QuerySolr) {

        $scope.init = function(pagePath, suffix, categoryPath, audienceTags, filterTags, disableTargeting) {

            CQ_Analytics.ClientContextUtils.onStoreRegistered("geolocation", function() {

                var geoLocationStore = CQ_Analytics.ClientContextMgr.getRegisteredStore("geolocation");

                geoLocationStore.addListener("update", function (action, property) {
                    if (action === "update") {
                        if (property === "address/country") {
                            var listener_country = geoLocationStore.getProperty("address/country", false);
                            if (listener_country != "" && listener_country != $scope.location) {
                                $scope.getPages(pagePath, suffix, categoryPath, audienceTags, filterTags, disableTargeting);
                            }
                        }
                    }
                }, this);

                $scope.getPages(pagePath, suffix, categoryPath, audienceTags, filterTags, disableTargeting);

            });
        };

        $scope.getPages = function(pagePath, suffix, categoryPath, audienceTags, filterTags, disableTargeting) {
            if (!disableTargeting) {
                var geoLocationStore = CQ_Analytics.ClientContextMgr.getRegisteredStore("geolocation");
                $scope.location = geoLocationStore.getProperty("address/country", false);

                if (filterTags.length > 0) {
                    filterTags = filterTags + ",";
                }
                if ($scope.location === "United Kingdom" || location == undefined) {
                    filterTags = filterTags + "Pearson-UK:Location/UK";
                } else {
                    filterTags = filterTags + "Pearson-UK:Location/International";
                }
            }

            //Create a new QuerySolr and point it to the correct servlet
            var tcQuery = QuerySolr;
            tcQuery.configQuery("/services/pearson/supporttopicsiblings/GET.servlet");
            var filterQuery = [$scope.queryType];
            tcQuery.query({pagePath: pagePath, suffix: suffix, categoryPath: categoryPath, audienceTags: audienceTags,
                            tags: filterTags }, function (data) {
                $scope.supportTopicSiblings = data;
            });
        };
    }
]);

