'use strict';

/* Controllers */
var pearsonControllers = angular.module('pearsonCourseMaterialsController', []);

pearsonControllers.controller('courseMaterials', [
    '$scope',
    'serializeForm',
    'QuerySolr',
    '$filter',
    'resolveState',
    '$rootScope',
    '$location',
    'CONST',
    'facetHandler',
    'analyticsService',
    function($scope, serializeForm, QuerySolr, $filter, resolveState, $rootScope, $location, CONST, facetHandler, analyticsService) {
        /*Trigger the page init events*/
        $scope.loading = false;

        $scope.orderProp = "title";
        $scope.resultOrder = "title"; //** FIX -- result order is a fix **//
        $scope.orderReverse = false;
        $scope.visited = false;

        $scope.valueCache = [];
        $scope.stateCache = [];

        $scope.startConfig = function(pageSelector){
            //Based on the page selector passed in, set boolean values to control the visibility of certain sort options
            $scope.pageSelector = pageSelector;
            if(pageSelector == "coursematerials"){
                $scope.showAZ = false;
                $scope.showDate = true;
                $scope.showDocType = true;
                $scope.showExam = false;
            }else if (pageSelector == "resources"){
                $scope.showAZ = true;
                $scope.showDate = true;
                $scope.showDocType = false;
                $scope.showExam = false;
                $scope.showPrice = true;

                $scope.orderProp = "ukprice";
                $scope.resultOrder = "-priceSort";
                $scope.orderReverse = true;
            }
        }

        //This is to cater for the special cases of the sort functions
        $scope.specialOrdering = function (){
            //console.log("Inside Special ordering");
            if($scope.orderProp == "examSeries"){
                $scope.resultOrder = ["examSeries", "-UnitVal", "docType","title"];
            }else if($location.hash().indexOf("Specification-and-sample-assessments") > -1 && $scope.orderProp == "docType"){
                $scope.resultOrder = "";
                $scope.documents = $scope.documents.sort(function(a, b){
                    // All specs need to be shown at the top
                    // All others are grouped by doctype and sorted by title
                    if (a.docType == "Specification" && b.docType != "Specification") {
                        return -1;
                    } if (b.docType == "Specification" && a.docType != "Specification") {
                        return 1;
                    }

                    var compareValue = a.docType.localeCompare(b.docType);
                    if (compareValue != 0) {
                        return compareValue;
                    } else {
                        return $scope.sortTitle(a, b);
                    }
                });
            }else if($scope.orderProp == "ukprice"){
                $scope.resultOrder = "-priceSort";
            }else if($scope.orderProp == "docType"){
                $scope.resultOrder = "";
                $scope.documents = $scope.documents.sort(function(a, b){
                    if (a.docType != undefined && b.docType != undefined) {
                        var compareValue = a.docType.localeCompare(b.docType);
                        if (compareValue != 0) {
                            return compareValue;
                        } else {
                            return $scope.sortTitle(a, b);
                        }
                    } else {
                        return $scope.sortTitle(a, b);
                    }
                });
            }else{
                $scope.resultOrder = $scope.orderProp;
            }
            //console.log( $scope.resultOrder);
            //console.log($scope.documents);
        };

        $scope.sortTitle = function(a, b) {
            var aMixed = $scope.normalizeMixedDataValue( a.title );
            var bMixed = $scope.normalizeMixedDataValue( b.title );
            return( aMixed < bMixed ? -1 : 1 );
        };

        // I take a value and try to return a value in which
        // the numeric values have a standardized number of
        // leading and trailing zeros. This *MAY* help makes
        // an alphabetic sort seem more natural to the user's
        // intent.
        $scope.normalizeMixedDataValue = function(value) {
            var padding = "000000000000000";
            // Loop over all numeric values in the string and
            // replace them with a value of a fixed-width for
            // both leading (integer) and trailing (decimal)
            // padded zeroes.
            value = value.replace(
                /(\d+)((\.\d+)+)?/g,
                function( $0, integer, decimal, $3 ) {
                    // If this numeric value has "multiple"
                    // decimal portions, then the complexity
                    // is too high for this simple approach -
                    // just return the padded integer.
                    if (decimal !== $3) {
                        return(padding.slice( integer.length ) +
                            integer + decimal
                        );
                    }
                    decimal = (decimal || ".0");
                    return(
                        padding.slice( integer.length ) +
                        integer + decimal +
                        padding.slice( decimal.length )
                    );
                }
            );
            //console.log(value);
            return(value);
        };

        //The runtime sort function for changing the sort options once the application has loaded
        //due to the complex nesting of some directives, these functions need to be added to the rootscope
        $scope.sortFunction = function(option){
            //console.log(option);
            switch(option){
                case "title":
                    $scope.orderProp = "title";
                    $scope.orderReverse = false;
                    break;
                case "date":
                    $scope.orderProp = "date";
                    $scope.orderReverse = false;
                    break;
                case "examSeries":
                    $scope.orderProp = "examSeries";
                    $scope.orderReverse = true;
                    break;
                case "docType":
                    $scope.orderProp = "docType";
                    $scope.orderReverse = false;
                    break;
                case "ukprice":
                    $scope.orderProp = "ukprice";
                    $scope.orderReverse = true;
                    break;
                default:
                    $scope.orderProp = option;
                    $scope.orderReverse = false;
                    break;
            }
            $scope.specialOrdering();
        };

        //Sort function that initialises the key pieces of functionality
        $scope.setSort = function(val){
            switch(val){
                case "Pearson-UK:Category/exam-materials":
                case "Pearson-UK:Category/Exam-materials":
                case "Pearson-UK:Category/external-assessment":
                case "Pearson-UK:Category/External-assessment":
                case "Pearson-UK:Category/Question-papers-and-mark-schemes":
                    $scope.showExam = true;
                    $scope.sortFunction("examSeries");

                    //order the collections appropriately
                    $scope.facets = $filter('reorderFacets')($scope.facets, "questionPaper");
                    break;
                case "Pearson-UK:Category/Teaching-materials":
                    $scope.showExam = false;
                    $scope.sortFunction("docType");

                    $scope.facets = $filter('reorderFacets')($scope.facets, "teaching");
                    break;
                case "Pearson-UK:Category/Forms-and-administration":
                    $scope.showExam = false;
                    $scope.sortFunction("docType");

                    $scope.facets = $filter('reorderFacets')($scope.facets, "forms");
                    break;
                case "Pearson-UK:Category/Specification-and-sample-assessments":
                    $scope.showExam = false;
                    $scope.sortFunction("docType");

                    $scope.facets = $filter('reorderFacets')($scope.facets, "specification");
                    break;
                default:
                    $scope.showExam = false;
                    $scope.sortFunction("docType");
                    break;
            }
        };

        //This is a function called from the "show more" button on the page, if there are more than x results on
        // the page, clicking show more will allow the display of more results : current increment = 20
        $scope.showMore = function(){
            var inc = 20;
            var limit = $scope.STATE.displayLimit;
            var resultLength = $scope.documents.length;

            $scope.STATE.displayLimit = limit + inc;

            $scope = resolveState($scope);
        };

        // The facets are no longer based on a hierarchy.
        //
        // When a facet group is selected, a query is done removing the selected facet group from the query.
        // The query that needs to be run for each facet group is the list of all selected facets
        // minus any that have been selected for the facet group
        //
        // i.e. if Question paper and June 13 have been selected
        // the query to retrieve the document type count will be June 13
        // and the query for exam series will be Question paper (The category will also be in the query)
        //
        $scope.cacheValues = function(facet){
            if ($scope.pageSelector == "resources") {
                var index;
                if(facet.facet.indexOf("Category") > -1)                index = CONST.CATEGORY;
                else if(facet.facet.indexOf("Document-Type") > -1)      index = CONST.DOCUMENTTYPE;
                else if(facet.facet.indexOf("Qualification-Size") > -1) index = CONST.QUALIFICATIONSIZE;
                else if(facet.facet.indexOf("Level") > -1)              index = CONST.LEVEL;
                else if(facet.facet.indexOf("Skill") > -1)              index = CONST.SKILL;
                else if(facet.facet.indexOf("Exam-Series") > -1)        index = CONST.EXAMSERIES;
                else if(facet.facet.indexOf("Unit") > -1) 		        index = CONST.UNIT;
                else if(facet.facet.indexOf("Format") > -1) 	        index = CONST.FORMAT;
                else if(facet.facet.indexOf("Resource-Type") > -1)      index = CONST.RESOURCETYPE;
                else if(facet.facet.indexOf("Publisher") > -1)          index = CONST.PUBLISHER;
                else if(facet.facet.indexOf("Price") > -1)              index = CONST.PRICE;

                //Reset value cache
                $scope.valueCache = [];
                $scope.stateCache = [];

                //Based on the index provided, work from the index "n" back down to "0" -
                //capturing the values from the given facet Group up to the first
                for(index; index > -1; index--){
                    $scope.valueCache[index] =  $scope.facets[index].coll;
                }
                return;
            }

            //Reset value cache
            $scope.valueCache = [];
            $scope.stateCache = [];

            var secondaryTags = serializeForm('searchForm', 'courseMatFilters');
            var selectedFacets = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var queries = [[]];
            var facetKey = [ "Category", "Document-Type", "Qualification-Size", "Level", "Skill", "Exam-Series", "Unit",
                "Format", "Resource-Type", "Publisher", "Format", "Price" ];

            // This array needs to be populated before the next step can be done
            // This array contains a count of facets selected in each facet group
            for (var x = 0; x < CONST.PRICE; x++) {
                selectedFacets[x] = $filter("getLooseMatch")(secondaryTags, facetKey[x]).length;
            }

            // This creates an array with the queries that need to be run for retrieve the correct
            // counts for each facet group
            for (var x = 0; x < CONST.PRICE; x++) {
                var q = [];
                if (selectedFacets[x] > 0) {
                    for (var y = 0; y < CONST.PRICE; y++) {
                        if (y != x && selectedFacets[y] > 0) {
                            q = q.concat($filter("getLooseMatch")(secondaryTags, facetKey[y]));
                        }
                    }
                }
                queries[0, x] = q;
            }

            // This runs the queries for the queries generated above
            var results = [];
            for (var x = 0; x < queries.length; x++) {
                if (queries[x].length > 0) {
                    results[x] = $scope.facetSearch(queries[x]);
                } else {
                    results[x] = "";
                }
            }

            // Set the counts for any selected facet groups
            for (var x = 1; x < CONST.PRICE + 1; x++) {
                if (selectedFacets[x] > 0) {
                    $scope.valueCache[x] = results[x][x].coll;
                }
            }

            // Retain the values for categories, otherwise the tab disappear
            $scope.valueCache[CONST.CATEGORY] = $scope.facets[CONST.CATEGORY].coll;
        }

        // This function filters $scope.allDocs by the facets passed in
        // $scope.allDocs should contain all docs returned by a query done using just the category
        // This may not be the case for bookmarked course material pages
        $scope.facetSearch = function(queryForFacets) {
            $scope.filterTags = queryForFacets;
            var allFilteredDocuments = $filter("filter")($scope.allDocs, docResultFilter);
            var fctArray = [ "Category", "Document-Type", "Qualification-Size", "Exam-Series", "Unit", "Format", "Level"];
            //Check to add certain facets based on diff scenarios
            if($filter("hasLooseMatch")($scope.primaryTags, "Qualification-Family/functional-skills")){
                if($filter("hasLooseMatch")($scope.primaryTags, "Qualification-Subject/English")){
                    fctArray.push("Skill");
                }
            }
            var gfacets = facetHandler.getFacetsFromDocuments(allFilteredDocuments);
            var facetsUsingSelectedFacetGroup = $filter('groupFacets')(gfacets, fctArray);
            return JSON.parse(JSON.stringify(facetsUsingSelectedFacetGroup));
        }

        //Taking the arguments of both the new facets generated from the results and the cached facets
        //we replace the values of the the new facets with the values that have been cached
        $scope.replaceValues = function(facet){
            for(var i = 0; i < $scope.valueCache.length; i++){
                if ($scope.valueCache[i]) {
                    facet[i].coll = $scope.valueCache[i];
                }
            }
//            for(var i = 0; i < $scope.stateCache.length; i++){ //Loop through the value cache
//                //Loop through the attributes on each "stateCache" object and then apply them
//                //to the appropriate object
//                $.each($scope.stateCache[i], function(key, value){
//                    facet[i][key][value];
//                });
//            }
            return facet;
        }

        //When you click on a facet it caches the valued based on that facet and updates the url with the facet selected
        $scope.facetClick = function(facet){
            $scope.cacheValues(facet);

            //Serialize form grabs the selected facets from the html body
            $scope.secondaryTags = serializeForm('searchForm', 'courseMatFilters');
            if(facet.facet.indexOf("Category") > -1) {
                $location.hash("filterQuery=" + facet.facet);
            } else if($scope.secondaryTags.length > 0) {
                $location.hash("filterQuery=" + $scope.secondaryTags.join("&filterQuery="));
            } else {
                $location.hash("");
            }

            //Use scope apply since the function may be called out of the context of CQ, you may need to call $scope.$apply
            //in order to trigger a digest cycle within angular js
            $scope.$apply();
        };

        $scope.analyticsClick = function(item) {


            //Add the event action
               if($scope.pageSelector == "resources") {
                     item["eAction"] = "Download Published Resource";
                }else if($scope.pageSelector == "coursematerials"){
                     item["eAction"] = "Download Course Materials";
                }
                
            analyticsService.sendEventFromDocument(item);
        };

        //Change the dropdown on the sort potions for the mobile view
        $scope.changeDropDown  = function(attrValue) {
            $scope.sortFunction(attrValue);
        }

        //This function is called within the ng-repeat within the view to check to see if the category facet group is
        //present, if so, then style the categories accordingly
        $scope.repeatCallback = function(){
            var height = jQuery('#Category').outerHeight(true) + 10;
            jQuery('.results').css({ marginTop: height + "px" });
            jQuery('.facets-list').css({ marginTop: height + "px"});
        };

        // ----------- MAIN FUNCTION --------------- //
        //Page setup includes querying solr for the results as well as setting setting up the information to make it
        //usable in the view
        $scope.courseMaterialsPageSetup = function(){
            //Set the state that is going to used in the other services
            //When you init the application
            $scope.STATE = ($scope.STATE || {});
            $scope.STATE.displayLimit = 20;
            $scope.STATE.showmore = false;

            $scope.searchFailed = false;
            $scope.visited = true;
            $scope.loading = true;
            //Serializes the form that is output on the jsp, these are the tags that are on the page
            //this component is embedded on
            $scope.primaryTags = serializeForm('searchForm', 'primary');

            //Loading the page when there is a query string
            if($location.hash() !== "" || window.location.hash != ""){
                var locationTags;
                if($location.hash() !== "")
                    locationTags = $location.hash();
                else{
                    locationTags= window.location.hash;
                    locationTags= locationTags.replace("#/","");
                }
                //$location.hash()=locationTags;
                $scope.secondaryTags = locationTags.replace("filterQuery=", "").split("&filterQuery=");
                $scope.queryTags = $scope.primaryTags.concat($scope.secondaryTags);

                if($scope.pageSelector == "resources") {
                    resourceQuery(false);
                }else if($scope.pageSelector == "coursematerials"){
                    courseMaterialsQuery(false);
                }
            }else{ //First time load, updates the $location.hash() value, and it triggers it from there
                $scope.queryTags = $scope.primaryTags;

                if($scope.pageSelector == "resources") {
                    resourceQuery(true);
                }else if($scope.pageSelector == "coursematerials"){
                    courseMaterialsQuery(true);
                }
            }
        };

        function courseMaterialsQuery(firstVisit){
            QuerySolr.configQuery("/services/pearson/search/GET.servlet");
            //Search for the documents based on the page identity tags rendered on the page + the category tag
            var queryTags = $scope.primaryTags; //.concat($filter("getLooseMatch")($scope.secondaryTags, "Category"));
            //List of Facets to be displayed on the page
            var fctArray = [ "Category", "Document-Type", "Qualification-Size", "Exam-Series", "Unit", "Format", "Level"];

            //Check to add certain facets based on diff scenarios
            if($filter("hasLooseMatch")($scope.primaryTags, "Qualification-Family/functional-skills")){
                //fctArray.push("Level");-no need as we've added it in the original array
                if($filter("hasLooseMatch")($scope.primaryTags, "Qualification-Subject/English")){
                    fctArray.push("Skill");
                }
            }

            if(!firstVisit){
                QuerySolr.query({ filterQuery: queryTags }, function(data) {
                    //Check to see if the results are empty
                    if(data.searchResults.solrDocuments.length) {
                        //Clean the data, this adds all the neccessary attributes to the data for sorting and for the facets
                        data.searchResults.solrDocuments = $filter('resultCleaner')(data.searchResults.solrDocuments);

                        if($scope.secondaryTags.length == 1){
                            $scope.valueCache = [];
                            facetHandler.clearFacetList();
                        }

                        //Get category facets from the first set of results and then add them to the cache
                        addCategoriesToFacets(data);

                        //Filter results by category
                        $scope.filterTags = $filter('getLooseMatch')($scope.secondaryTags, ["Category"]);
                        var docs = $filter("filter")(data.searchResults.solrDocuments, docResultFilter);

                        //Filter results by the secondary tags
                        $scope.filterTags = $scope.secondaryTags;
                        $scope.documents = $filter("filter")(docs, docResultFilter);

                        $scope.allDocs = docs;

                        //Create a list of facets based on ALL the results
                        var gfacets = facetHandler.getFacetsFromDocuments(docs);
                        var groupedFacets = $filter('groupFacets')(gfacets, fctArray);

                        //Facet numbers will be updated based on the filtered results set
                        gfacets = facetHandler.getFacetsFromDocuments($scope.documents);
                        var groupedFacets = $filter('groupFacets')(gfacets, fctArray);

                        $scope.facets = JSON.parse(JSON.stringify($scope.replaceValues(groupedFacets)));

                        $scope.STATE.resultsLength = $scope.documents.length;
                        $scope = resolveState($scope);
                    }else{
                        $scope.searchFailed = true;
                    }

                    $scope.loading = false;
                });
            }else{
                QuerySolr.query({ filterQuery: queryTags, resourceTypeQuery: true }, function (data) { 		//set callback
                    if(data.resourceTypeFacetResults.length) {
                        //this is fix for Specification/Specification and sample assessments tab not being selected as default
                        //before we were just returning newTag = "category:" + data.resourceTypeFacetResults[0].facet;
                        var newTag = "";
                        for(var i = 0; i < data.resourceTypeFacetResults.length; i++){
                            if(data.resourceTypeFacetResults[i].facet.indexOf("Specification") > -1){
                                newTag = "category:" + data.resourceTypeFacetResults[i].facet;
                                break;
                            }
                        }

                        if(newTag == ""){
                            newTag = "category:" + data.resourceTypeFacetResults[0].facet; //get the first category tag
                        }

                        $scope.queryTags.push(newTag);
                        $scope.secondaryTags = [newTag];
                        $location.hash("filterQuery=" + newTag);
                    }else{
                        //Display failed message
                        $scope.searchFailed = true;
                    }
                    $scope.loading = false;
                });
            }
        }

        function resourceQuery(firstVisit){
            //define what query tags to use in your search
            //THIS IS THE BASE QUERY -  a single, direct and cache-able of all the documents
            var queryTags = $filter('getLooseMatch')($scope.queryTags, ["Qualification-Family", "Qualification-Subject",
                "Accreditation-From-date"]);
            var fctArray = [ "Price", "Publisher", "Resource-Type" ];
            queryTags.push(CONST.paidResources);
            QuerySolr.configQuery("/services/pearson/solr/GET.servlet");

            QuerySolr.query({fq: queryTags, facet: true, facetField: "category"}, function(data){
                //Get filterTags
                $scope.filterTags = $filter('getLooseMatch')($scope.queryTags, fctArray);

                //Filter the documents based on the facets selected
                var docs = $filter("filter")(data.searchResults.solrDocuments, docResultFilter);
                $scope.documents = $filter('resultCleaner')(docs);

                $scope.allDocs = docs;

                //Generate facets for the page based on the base request
                var gfacets = facetHandler.getFacetsFromDocuments(data.searchResults.solrDocuments);
                var groupedFacets = $filter('groupFacets')(gfacets, fctArray);

                //Update the counts based on the filtered data
                gfacets = facetHandler.getFacetsFromDocuments($scope.documents);
                var groupedFacets = $filter('groupFacets')(gfacets, fctArray);

                //This is so that we pass through an exact copy of the object not a reference of it
                $scope.facets = JSON.parse(JSON.stringify($scope.replaceValues(groupedFacets)));

                $scope.STATE.resultsLength = $scope.documents.length;
                $scope = resolveState($scope);
                $scope.loading = false;
            });
        }

        // -- Pass through a list of facets based on the url and apply them to
        //the facet caches as though they have been clicked
        function generateFacetCache(facets){
            angular.forEach(facets, function(data){
                $scope.cacheValues(data);
            });
        }

        //this is used to filter out the relevant documents
        function docResultFilter(doc){
            var matches = {};
            var result = true;
            var filterTags = $scope.filterTags;

            //check the facet groups available
            angular.forEach(filterTags, function(data) {
                matches[data.replace("category:", "").split("/")[0]] = false;
            });

            //Check to see what facet groups apply to this doc
            for(var i = 0; i < filterTags.length; i++) {
                var cleanTag = filterTags[i].replace("category:", "");
                if (doc.category.indexOf(cleanTag) > -1) {
                    matches[cleanTag.split("/")[0]] = true;
                }
            }

            //To be included at one facet from each facet group must apply to this document
            $.each(matches, function(key, value) {
                if(result) {
                    if (!value) result = false;
                }
            });

            //Several more matching rules can be implemented here

            return result;
        }

        /*
         * WS-1527 Need to exclude specified list of categories from showing
         */
        function removeCategoryExclusions(facet) {
            // Build list of exclusions
            var exclusions = CONST.categoryExclusionList;
            // Loop thought the exclusions and check the facet is not one of them
            for (var x = 0; x < exclusions.length; x++) {
                var exclusion = exclusions[x].trim();
                if(facet.facet.indexOf(exclusion) > -1){ return false; }
            }
            return true;
        }

        //Sets the facets on the cached results so that the category facets will always be present
        function addCategoriesToFacets(data) {
            if (!$scope.categoryFacets) {
                // Get facets from search results
                var gfacets = facetHandler.getFacetsFromDocuments(data.searchResults.solrDocuments);
                // Apply filter to return only Category tags
                var categoryFacets = $filter("getLooseMatch")(gfacets, { "facet" : "Category" });
                // Then filter again against list of excluded tags
                $scope.categoryFacets = $filter("filter")(categoryFacets, removeCategoryExclusions);
                $scope.facets = $filter("groupFacets")($scope.categoryFacets, ["Category"]);
                $scope.categoryFacets = $scope.facets;

                $scope.cacheValues($scope.categoryFacets[CONST.CATEGORY].coll[0]);
                facetHandler.clearFacetList();
            } else {
                $scope.valueCache[CONST.CATEGORY] = $scope.categoryFacets[CONST.CATEGORY].coll;
            }
        }

        $scope.$watch(function () {
            if ($location.hash() != "")
                return $location.hash();
            else if (window.location.hash != "")
                return window.location.hash.replace("#/","");
        }, function(newurl, oldurl) {
            if($scope.visited) {
                //if your going back to the page without any #-value
                //force your way off the page with another back command
                if($scope.pageSelector == "coursematerials" && (newurl == "" || newurl == undefined)) {
                    window.history.back();
                } else {
                    if(newurl == "")//when the url is clear, clear the cache
                        $scope.valueCache = "";

                    $scope.courseMaterialsPageSetup();
                }
            } else {
                $scope.courseMaterialsPageSetup();
            }
        });
    }
]);
