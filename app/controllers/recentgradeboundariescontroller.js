'use strict';

/* Controllers */
var pearsonRecentGradeBoundairesController = angular.module('pearsonRecentGradeBoundairesController', ['pearsonPastPapersFilters']);

pearsonRecentGradeBoundairesController.controller('recentGradeBoundariesAccordionCtrl', [
    '$scope',
    '$log',
    'QuerySolr',
   	'$filter',
    'analyticsService',
    function ($scope, $log, QuerySolr, $filter, analyticsService) {
    	var QfacetQuery = QuerySolr;
    	var simpleQuery = QuerySolr;
    	
    	$scope.loading = false;
    	
    	var facetQuery = ["Pearson-UK:Document-Type/Grade-boundaries",
    	                  "Pearson-UK:Category/Support"];
    	QfacetQuery.query({filterQuery: facetQuery, getQFFacets:true}, function(data){
    		var gradeBoundariesFamList = [];
    		angular.forEach(data.searchResults, function(fct){
    			var entry = {family: fct.facet, doc:""}; 
    			gradeBoundariesFamList.push(entry);
    		});
    		
    		var sQuery = ["Pearson-UK:Document-Type/Grade-boundaries"];
        	simpleQuery.query({filterQuery: sQuery, doSimpleSearch: true}, function(data){
        		var container   = [];
        		var finalList   = [];
                var qfList      = {};
        		
        		container = $filter('resultCleaner')(data.searchResults.solrDocuments);
        		container = $filter('orderBy')(container, 'examSeries', true);
                
                angular.forEach(gradeBoundariesFamList, function(family) {
                	family.doc = $filter('filter')(container, {category: family.family},true);
                    //If there is no exam series - do not use it
                    if(family.doc.length) {
                        //Get the latest exam series from the doc list
                        var series = family.doc[0].examSeries;
                        //The remove all the other exam series
                        family.doc = $filter('filter')(family.doc, {examSeries: series});

                        //If the qualification family has been returned then don't use it
                        if(qfList[family.family.toLowerCase()]){
                            return;
                        } else {
                            //Otherwise add it to the list
                            qfList[family.family.toLowerCase()] = true;
                        }

                        //If the list of docs
                        finalList.push(family);
                    }
                });
                
                $scope.gradeBoundaries = $filter('orderBy')(finalList, 'family');
            });
    	});

        $scope.analyticsClick = function(item) {
            analyticsService.sendEventFromDocument(item);
        };
	}
]);