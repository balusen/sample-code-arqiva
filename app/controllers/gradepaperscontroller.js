'use strict';

/* Controllers */
var pearsonControllers = angular.module('pearsonGradePapersController', ['pearsonPastPapersFilters']);

pearsonControllers.controller('AccordionCtrl', [
    '$rootScope',
    '$scope',
    '$log',
    'QualificationFamily',
    'PastPapersDocuments',
    'pastPapersEvents',
    '$location' ,
    'QuerySolr',
    'resolveShowMore',
    '$filter',
    'facetHandler',
    'analyticsService',
    function ($rootScope, $scope, $log, QualificationFamily, PastPapersDocuments, pastPapersEvents, $location, QuerySolr, resolveShowMore, $filter, facetHandler, analyticsService) {

        $scope.gradeDocType = "Pearson-UK:Document-Type";
        $scope.gradePapersFacet = "Pearson-UK:Category/Support";
        $scope.groupByFacet = "Pearson-UK:Exam-Series";
        $scope.qualFamilyFacet = "Pearson-UK:Qualification-Family";

        $scope.init = function (gradeDocType) {
            $scope.gradeDocType = "Pearson-UK:Document-Type/" + gradeDocType;

            //Get the grade boundary documents
            QuerySolr.configQuery("/services/pearson/solr/GET.servlet");
            QuerySolr.query({ filterQuery: ["category:\"" + $scope.gradeDocType + "\"",
                              "category:\"" + $scope.gradePapersFacet + "\""],
                              facetField: "category" },
                function(data){
                    //Filter out the qualification family facets
                    var facets = $filter("getLooseMatch")(data.searchResults.facets, { "facet" : "Pearson-UK:Qualification-Family" });
                    $scope.families = facets;
            });
        };

        $scope.$watch(function() { return $location.search(); }, function (newUrl, oldUrl){
            var qualificationFamilyInUrl = (newUrl["Qualification-Family"] != undefined);
            if (qualificationFamilyInUrl) {
                $scope.getGradePapers($location.search()['Qualification-Family']);
                $scope.openAccordion('#step2');
            } else {
                $scope.selectedQualificationFamily = '';
                $scope.openAccordion('#step1');
            }
        });

        $scope.openAccordion = function (tabId) {
            $('.findgradepapers div.ui-accordion-content').slideUp('fast');
            var tabDiv = tabId + " > div.ui-accordion-content";
            $(tabDiv).slideDown('fast');

            // Manage current classes
            $('.step').removeClass('current');
            $(tabDiv).parent().removeClass('selection');
            $(tabDiv).parent().addClass('current');
            // If Step 1 selected then return component to default none selected
            if(tabId == '#step1'){
            	$scope.selectedQualificationFamily = "";
            }
            $scope.gradePapersPageSetup();
        };

        $scope.setQualificationFamilyUrl = function (qualification) {
            $location.url('?Qualification-Family=' + qualification.facetValue);

            $('#step1').addClass('selection');
            if(!$scope.$$phase){
                $scope.$apply();
            }
        };
        
        $scope.gradePapersPageSetup = function(){
            //Set the state that is going to used in the other services
            //When you init the application
        	 $rootScope.STATE = ($rootScope.STATE || {});
             $rootScope.data = null;
             $rootScope.STATE.resultsLength = 0;
             $rootScope.STATE.displayLimit = 10;
             resolveShowMore();
        };

        $scope.getGradePapers = function (qualification) {
//            $('.findgradepapers div.ui-accordion-content').slideUp('fast');
//            $('#step2 > div.ui-accordion-content').slideDown('fast');

            $scope.selectedQualificationFamily = (qualification).replace(/-/g, ' ');

            var tagsArray = [$scope.gradePapersFacet, $scope.gradeDocType, $scope.qualFamilyFacet + "/" + qualification];

            QuerySolr.configQuery("/services/pearson/search/GET.servlet");
            QuerySolr.query({filterQuery: tagsArray, doSingleSearch: true}, function (data) {
                $scope.documents = $filter('resultCleaner')(data.searchResults.solrDocuments);
                $scope.documents.sort(function (a, b) {

                    var examSeriesOne = jQuery(b.category).filter(function () {
                        return (this.indexOf("Exam-Series") > -1 );
                    });
                    if(examSeriesOne != null && examSeriesOne.length > 0){
                    	examSeriesOne = examSeriesOne[0].replace($scope.groupByFacet + "/", "").replace("-", " 1,");
                    } else {
                    	examSeriesOne = "June 1,1970";
                    }

                    var examSeriesTwo = jQuery(a.category).filter(function () {
                        return (this.indexOf("Exam-Series") > -1 );
                    });
                    if(examSeriesTwo != null && examSeriesTwo.length > 0){
                    	examSeriesTwo = examSeriesTwo[0].replace($scope.groupByFacet + "/", "").replace("-", " 1,");
                    }else {
                    	examSeriesTwo = "June 1,1970";
                    }
                    
                    return Date.parse(examSeriesOne) - Date.parse(examSeriesTwo);
                });

                $rootScope.STATE = ($rootScope.STATE || {});
                $rootScope.data = data;
                $rootScope.STATE.resultsLength = data.searchResults.solrDocuments.length;
                $rootScope.STATE.displayLimit = 10;
                resolveShowMore();
            });

            /*Add events to the scope to be used on the page*/
            $scope.pastPapersEvents = pastPapersEvents;
        };

        $scope.analyticsClick = function(item) {
            analyticsService.sendEventFromDocument(item);
        };
    }
]);

