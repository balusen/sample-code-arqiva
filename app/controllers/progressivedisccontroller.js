/* Controllers */
var progressiveDisclosureControllers = angular.module('progressiveDisclosureControllers', ['pearsonServices']);

progressiveDisclosureControllers.controller('progressiveDisclosureThreeStep', [
    '$scope',
    'QuerySolr',
    '$filter',
    '$location',
    '$element',
    '$anchorScroll',
    'pageConfig',
    'facetHandler',
    '$http',
    function ($scope, QuerySolr, $filter, $location, $element, $anchorScroll, pageConfig, facetHandler, $http) {


        //Capture the Dom scope in variable
        var appDom = $($element), step1Selection = "", step2Selection = "";
        $scope.subjects, $scope.qualification, $scope.documents, $scope.facets;
        //PAGE CONFIG URL : <Component Directory>/config.jsp

        //This will go through the page config <dir_of_app>/config.jsp you set up and then attach the values to the scope object
        pageConfig.attachAttributes($scope);

        $scope.resultsFilter    =   "";
        //Init the variable to limit the results
        var defaultLimit        =   20;
        $scope.resultsLimit     =   defaultLimit;
        $scope.showModal        =   true;
        //Boolean values to control what edit buttons should be showing
        $scope.edit1 = false,   $scope.edit2 = false;

        $scope.edexcelLinkDirect = 'https://www.edexcelonline.com/TrainingBookings/ViewEventDetails.aspx?Function=2&CourseEventId=';

        $scope.$watch(function () {
            return $location.search();
        }, function (newUrl, oldUrl) {
            //For some reason this function still gets run when the hash gets called so we need to check
            //if the two values are exactly the same then don't bother to process it
            // When the page loads multiple change events are triggered where the old and new url are the same
            // for book marked urls these events cannot be ignored
            if(JSON.stringify(newUrl) === JSON.stringify(oldUrl) && document.readyState == "complete") { return; }

            //Based on Url determine the step
            var stp1 = $location.search()['stp1'];
            var stp2 = $location.search()['stp2'];

            if (stp2 !== undefined) {
                $scope.setCurrentTabs(3);
                updateStep(2, $location.search()['stp1']);
                updateStep(3, $location.search()['stp2']);
            } else if (stp1 !== undefined) {
                $scope.setCurrentTabs(2);
                updateStep(2, $location.search()['stp1']);
            } else { //default
                $scope.setCurrentTabs(1);
            }

            //This should now only be called when the search string is now changed
            $location.hash('');
            $location.hash('step1');
            $anchorScroll();
        });

        //Create a new QuerySolr and point it to the correct servlet
        var tcQuery = QuerySolr;
        tcQuery.configQuery("/services/pearson/solr/GET.servlet");
        var filterQuery = [$scope.queryType];
        var facetPrefix = ($scope.facetPrefix)? $scope.facetPrefix : "";
        tcQuery.query({ fq: filterQuery, facet: true, facetField: "category", facetPrefix: facetPrefix, rows: 0 }, function (data) {
            $scope.qualification = facetHandler.getFacetsFromFacets(data.searchResults.facets, true);
        });

        function encodeAMP(list) {
            var cleanList = [];
            angular.forEach(list, function (element) {
                element = element.replace("&", "%26");
                cleanList.push(element);
            });
            return cleanList;
        }

        //Updates the url in an appropriate way
        $scope.changeStep = function (step, facet) {
            var urlString = [];

            if (step == 2) {
                step1Selection = facet;
                step2Selection = "";
            }
            if (step == 3) {
                step1Selection = $location.search()['stp1'];
                step2Selection = facet;
            }

            if (step1Selection != "")
                urlString.push("stp1=" + step1Selection);
            if (step2Selection != "")
                urlString.push("stp2=" + step2Selection);

            urlString = encodeAMP(urlString);

            $location.url("?" + urlString.join("&"));
        }

        //This changes the tabs based on a numberical number
        //this also calls the function open accordion to physically change the steps
        $scope.setCurrentTabs = function (num) {

            appDom.find(".step").removeClass("selection");
            appDom.find(".step").removeClass("current");
            appDom.find("#step" + num).addClass("current");

            $scope.openAccordion("#step" + num);

            for (var i = 1; i < num; i++) {
                appDom.find("#step" + i).addClass("selection");
            }

            if (1 == num) {
                $scope["step1Header"] = '';
                $scope["step2Header"] = '';
                $location.url($location.url().substring(0, $location.url().indexOf('stp1')));
            } else if (2 == num) {
                $scope["step2Header"] = '';
                $location.url($location.url().substring(0, $location.url().indexOf('stp2')));
            }
        }

        //This function physically changes the state of the page
        $scope.openAccordion = function (tabId) {
            var tabDiv = tabId + " > div.ui-accordion-content";
            var activeTab = $(appDom).find(tabDiv);
            $(appDom).find('div.ui-accordion-content').not(activeTab).slideUp(500);

            $(activeTab).slideDown(500);
        };

        function updateStep(step, facet) {
            if (facet != undefined) {                 //There will only be a facet argument if you are stepping forward
                var currStep = step - 1;            //current change the header of the current step
                $scope["step" + currStep + "Header"] = facet;
                $scope["edit" + currStep] = true;
                for (var i = step; i < 5; i++) { //clear headers and steps after this point
                    $scope["step" + i + "Header"] = "";
                    $scope["edit" + i] = false;
                }
                var stepQuery = filterQuery.slice();//Get the value from the original filter Query
                var facetPrefix;

                if ($location.search()['stp1'] !== undefined) {
                    stepQuery.push("category:\"" + $location.search()['stp1'] + "\"");
                    facetPrefix = ($scope.facetPrefix)? $scope.facetPrefix : "";
                }
                if ($location.search()['stp2'] !== undefined && step == 3) {
                    stepQuery.push("category:\"" + $location.search()['stp2'] + "\"");
                    facetPrefix = "";
                }

                //createPageQuery(stepQuery);
                tcQuery.query({ fq: stepQuery, facet: true, facetField: "category", facetPrefix: facetPrefix}, getCallback(step));
            }
        }

        function getCallback(num) {
            if (num == 3) {
                return function (data) {
                    $scope.resultsFilter = "";
                    $scope.documents = $filter("resultCleaner")(data.searchResults.solrDocuments);
                    //$scope.documents = $filter("publisherGroupBy")(dataDocuments);
                    //console.log($scope.documents);
                    $scope.resetLimit($scope.documents);
                    $scope.resultsLimit = 20;
                    $scope.facets = facetHandler.getFacetsFromFacets(data.searchResults.facets, true);
                }
            } else if (num == 2) {
                return function (data) {
                    $scope.subjects = facetHandler.getFacetsFromFacets(data.searchResults.facets, true);
                }
            }
        }

        function createPageQuery(list) {
            $scope.pageQuery = "#fq=" + list.join("fq=");
        }

        $scope.changeDropDown = function (attrValue, attrType, that) {
            $scope.resultsLimit = 20;
            if (attrType == "clear") {
                $scope.resultsFilter = "";
                attrValue.replace(/ /g, '-');
                $scope.resetLimit($scope.documents);
            } else if (attrType == "date") {
                $(appDom).find(".custom-select select").not(that).val('');
                var Datefilter = $filter('date');
                $scope.resultsFilter = function (value) {
                    //results whos month == attr.filter
                    var date = Datefilter(value.fromdate, 'MMMM yyyy');
                    if (date == attrValue) {
                        return true
                    }
                    return false;
                }
            } else {
                $(appDom).find(".custom-select select").not(that).val('');
                $scope.resultsFilter = { "category": attrValue }; //clear each dropdown
            }
        }

/*        $scope.modal = function(doc){
            //TODO create a new modal based on the doc
            if(typeof doc != 'undefined'){
                //Get the modal to show up on the page
                $scope.showModal = true;
                //document to be used to configure the display of the modal
                $scope.modalDoc = doc;
            }else{
                $scope.showModal = true;
                $scope.modalDoc = {};
            }
        }*/

        $scope.resetLimit = function (documents) {
            $scope.resultsLength = documents.length;
            $scope.resolveShowMore();
        };

        $scope.showMore = function(){
            $scope.resultsLimit += 20;
            $scope.resolveShowMore();
        }

        $scope.resolveShowMore = function(){
            if ($scope.resultsLength > $scope.resultsLimit) {
                $scope.showmore = true;
            } else {
                $scope.showmore = false;
                $scope.resultsLimit = $scope.resultsLength;
            }
        }

        $scope.resetSort = function () {
            $scope.resultsFilter = "";
            $scope.resetLimit($scope.documents);

            $(appDom).find("#dateDrpdn").val('');
            $(appDom).find("#locationDrpdn").val('');
        };
    }
]);