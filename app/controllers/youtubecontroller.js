'use strict';

/* Controllers */
var pearsonYoutubeController = angular.module('pearsonYoutubeController', ['pearsonServices', 'youtubeServices']);

pearsonYoutubeController.controller('videoController', [
	'$scope',
    '$filter',
    'VideoList',
    'pageConfig',
    function($scope, $filter, VideoList, pageConfig) {
        $scope.showPlaylist = false;
        $scope.loading      = false;
        $scope.failed       = false;
        $scope.modal = "/etc/designs/ped/pei/uk/pearson-uk-new/angularJS/app/partials/youtube/youtube-modal.html"

        //Get videos
        pageConfig.attachAttributes($scope);

        //Setup class names to be used to govern the setup of the component
        // -- Depending on certain factors these variables can change to give a clearer page setup
        $scope.changeVideo = function(vid){
            $scope.play = vid;

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        }

        //If there are videos
        if ($scope.videosUrls.length > 0) {
            //Create videos from url list
            VideoList.createFromList($scope.videosUrls);

            $scope.videos   = VideoList;
            $scope.play     = $scope.videos[0];

            $scope.failed = false;
        }
	}
]);