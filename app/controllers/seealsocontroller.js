'use strict';

/* Controllers */
var pearsonSeeAlsoController = angular.module('pearsonSeeAlsoController', []);

pearsonSeeAlsoController.controller('SeeAlsoCtrl', [
    '$scope',
    '$rootScope',
    'QuerySolr',
    'resolveShowMore',
    'resolveState',
    function ($scope, $rootScope, QuerySolr, resolveShowMore, resolveState) {

        $scope.init = function(pagePath, displayLimit, disableTargeting, searchTags) {

            $scope.searchTags = searchTags;

            if (displayLimit === undefined || displayLimit == null || displayLimit == "") {
                $scope.displayLimit = 3;
            } else {
                $scope.displayLimit = displayLimit;
            }

            $rootScope.STATE = ($scope.STATE || {});
            $rootScope.STATE.displayLimit = $scope.displayLimit;

            CQ_Analytics.ClientContextUtils.onStoreRegistered("geolocation", function() {

                var geoLocationStore = CQ_Analytics.ClientContextMgr.getRegisteredStore("geolocation");

                geoLocationStore.addListener("update", function (action, property) {
                    if (action === "update") {
                        if (property === "address/country") {
                            var seeAlso_country = geoLocationStore.getProperty("address/country", false);
                            if (seeAlso_country != "" && seeAlso_country != $scope.location) {
                                console.log('SeeAlso listener country:' + seeAlso_country);
                                $scope.getPages(pagePath, disableTargeting);
                            }
                        }
                    }
                }, this);

                $scope.getPages(pagePath, disableTargeting);
            });
        };

        $scope.getPages = function(pagePath, disableTargeting) {
            if (!disableTargeting) {
                var geoLocationStore = CQ_Analytics.ClientContextMgr.getRegisteredStore("geolocation");
                $scope.location = geoLocationStore.getProperty("address/country", false);
                var filterTags = "";

                if ($scope.location === "United Kingdom" || $scope.location == undefined) {
                    filterTags = "Pearson-UK:Location/UK";
                } else {
                    filterTags = "Pearson-UK:Location/International";
                }
            }

            //Create a new QuerySolr and point it to the correct servlet
            var tcQuery = QuerySolr;
            tcQuery.configQuery("/services/pearson/seealso/GET.servlet");
            var filterQuery = [$scope.queryType];
            var tags = [$scope.searchTags];

            tcQuery.query({pagePath: pagePath, tags: filterTags, searchTags: tags}, function (data) {

                $rootScope.STATE = ($rootScope.STATE || {});
                $rootScope.STATE.displayLimit = $scope.displayLimit;
                $rootScope.STATE.resultsLength = data.seeAlsoPages.pageList.length;
                resolveShowMore();

                $scope.seeAlsoPages = data.seeAlsoPages.pageList;
                $scope.hideComponent = data.seeAlsoPages.hide;
            });
        };

        $scope.showMore = function(){
            $rootScope.STATE.displayLimit = $rootScope.STATE.resultsLength + 3;
            $scope = resolveState($scope);
        };
    }
]);