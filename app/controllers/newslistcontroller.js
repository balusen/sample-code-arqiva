'use strict';

/* Controllers */
var pearsonControllers = angular.module('pearsonNewsListController', ['pearsonServices','pearsonCourseMaterialServices', 'pearsonFilters']);


pearsonControllers.controller('newsListCtrl', [ '$scope', 'QuerySolr','serializeForm','resolveState','$filter','$location',
    function($scope, QuerySolr, serializeForm, resolveState, $filter, $location) {
		
		/*Trigger the page init events*/
	    $scope.loading = false;
	    $scope.orderProp = "sortDate";
	    $scope.resultOrder = "sortDate"; //** FIX -- result order is a fix **//
	    $scope.orderReverse = true;
	    $scope.searchFailed = true;

	    //Only one sort by available currently
	    $scope.showDate = true;
	
	    var pageTagsArray = {};

		$scope.newsListQuery = function(){
			//Define servlet to be used for search in solr
			QuerySolr.configQuery("/services/pearson/solr/GET.servlet");
			
			//Perform solr query to return the news and news facets
			QuerySolr.query({fq: $scope.baseQueryTags, q: $scope.pageTags, facet: true, facetField: "pagetype", sort: "date desc"}, function(data){
				$scope.newsList = [];
				if(data.searchResults.solrDocuments.length > 0) {
					//$scope.newsList = data.searchResults.solrDocuments;
					$scope.newsList = $filter('filterItems')(data.searchResults.solrDocuments, pageTagsArray);
				}
				if(data.searchResults.facets.length > 0) {
					$scope.facets = buildFacets($scope.newsList);
				}
				function buildFacets(list){
		    		var facets = [];
		    		for(var j = 0; j < list.length; j++){
		    			if(facets.length==0){
		        			//add first facet object
		        			facets.push({facet:list[j].pagetype,formattedFacetValue:list[j].pagetype,count:1});
		        		}
		    			else{
		    				var matched = false;
		    				for(var i = 0; i < facets.length; i++){
		            			if (facets[i].facet == list[j].pagetype){
		            				facets[i].count++;
		            				matched = true;
		            				break;
		            			}
		            		}
		    				if(!matched){
		    					//we didn't find facet for this pagetype so add new one
		    					facets.push({facet:list[j].pagetype,formattedFacetValue:list[j].pagetype,count:1});
		    				}
		    			}
		    			
		    		}
		    		return facets;
		    	};
		    	
				$scope.STATE.resultsLength = $scope.newsList.length;
				$scope.resolveState();
				if($scope.newsList.length > 0){
					$scope.firstNewsItem =  $scope.newsList[0];
				}
				
			});
		}
		
		//This function will allow the newlist to init
		//with a specific news type shown by default
		$scope.init = function (selectedFacets, pageTags, pageTypes){
			
			//Set the state that is going to used in the other services
		    //When you init the application
		    $scope.STATE = ($scope.STATE || {});
		    $scope.STATE.displayLimit = 20;
		    $scope.STATE.showmore = false;
		    $scope.searchFailed = false;
		    $scope.loading = true;
		
		    //var pageTagsArray;
		    if(pageTags.length > 0){
		    	pageTagsArray = pageTags.split(",");
		    }
		    
		    	    
			switch(selectedFacets){
                case "news":
                case "news-all":
                	/*if(pageTagsArray.length > 0){
                		$scope.pageTags = "category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Qualification-Family"], true) + "\"";
                	}*/
                	$scope.defaultFacet = selectedFacets;
                	break;
                case "subject-update":
                case "subject-update-all":
                	/*if(pageTagsArray.length > 0){
                		$scope.pageTags = "category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Subject-Family"], true) + "\"";
                	}*/
                	$scope.defaultFacet = selectedFacets;
                	break;
                case "press-release":
                case "press-release-all":
                	/*if(pageTagsArray.length > 0){
                		$scope.pageTags = ["category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Subject-Family"], true) + "\"","category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Qualification-Subject"], true) + "\""];
                	}*/
                	$scope.defaultFacet = selectedFacets;
                	break;
                case "all":
                	//Will extend this to include tags later
                	/*$scope.pageTags = ["*.*"];*/
                	$scope.defaultFacet = selectedFacets;
                	break;
                case "component":
                    //Component override regarding
                    $scope.pageTags = [];
                    for(var i =0; i < pageTagsArray.length; i++) {
                        $scope.pageTags.push("category:\"" + pageTagsArray[i] + "\"");
                    }
                    //Set the facets back to something that is usable for rendering the component
                    $scope.defaultFacet = selectedFacets = "all";
                    break;
                default:
                	//$scope.pageTags = ["category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Subject-Family"], true) + "\"","category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Qualification-Subject"], true) + "\""];
                	$scope.defaultFacet = "all";
                    break;
			}
			
			
			//Set the pagetags to all the news matching subject family / qual family irrespective of facets
			if(selectedFacets != "component"){
				if(pageTagsArray.length > 0){
					var subjectFamily = $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Subject-Family"], true);
					var qualFamily = $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Qualification-Family"], true);
					var pageTags = [];
            		if(subjectFamily){
            			pageTags.push("category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Subject-Family"], true) + "\"");
            		}
            		if(qualFamily){
            			pageTags.push("category:\"" + $filter('getLooseMatch')(pageTagsArray, ["Pearson-UK:Qualification-Family"], true) + "\"");
            		}
            		
            		if(pageTags.length > 1){
            			$scope.pageTags = pageTags;
            		}else if(pageTags.length = 1) {
            			$scope.pageTags = pageTags[0] ;
            		}
            	}
			}
			
			
			//If pageTypes are set then add those to the base query tags for search
			var baseQueryTagsArray = [];
			baseQueryTagsArray.push("category:\"page-type:news\"");
			baseQueryTagsArray.push("type:\"cq:page\"");
			if(pageTypes.length > 0){
				var pageTypesArray = pageTypes.split(",");
				var pageTypesString = "";
				for(var i = 0; i < pageTypesArray.length; i++){
					pageTypesString = pageTypesString + "pagetype:\"" + pageTypesArray[i] + "\"";
					//Add 'or' when there's more than one page type
					if((pageTypesArray.length > 1) && (i == 0 || i < (pageTypesArray.length - 1))){
						pageTypesString = pageTypesString + " | ";
					}
				}
				baseQueryTagsArray.push(pageTypesString);
			}
			$scope.baseQueryTags = baseQueryTagsArray;
			
			//Perform Solr Search
			$scope.newsListQuery();
			
			if(selectedFacets.length > 0){
				$scope.selectedFacetsArray = [selectedFacets];				
			} else {
				$scope.selectedFacetsArray = [""];
			}
			
			//finished loading
			$scope.loading = false;
		}
		
		//This function will allow the news list to 
		//be filtered using the facet options
		$scope.facetClick = function () {    		
			//Serialize form grabs the selected facets from the html body
            var selectedFacets = serializeForm('searchForm', '');
            var selectedFacetsArray = [];
            for(var i = 0; i < selectedFacets.length; i++){
            	if(selectedFacets[i] == "all"){
            		selectedFacetsArray.push("all");
            	} else {
            		selectedFacetsArray.push(selectedFacets[i]);
            	}
            }                      
			$scope.selectedFacetsArray = selectedFacetsArray;
            $scope.$apply();            
        };
        
        //This will loop through selected facets and return only matching news items
        $scope.resultsFilter = function(value) {
            //If the article displaying is the same as this element filter it out
            var search = decodeURIComponent(location.search.substr(9)).replace(".html", "");
            search = search.substr(0, (search.indexOf("&") > -1)? search.indexOf("&") : search.length);
            var url = value.id.replace(".html", "");
            if(search == url) {
                return false
            }

        	var pageType = value.pagetype;
        	if(pageType.length > 0){
        		var selectedFacet = $scope.selectedFacetsArray[0];
            	if(selectedFacet != "" && selectedFacet.indexOf("all") == -1){
	            	for(var i = 0; i < $scope.selectedFacetsArray.length; i++){
	    	            if (pageType == $scope.selectedFacetsArray[i]){
	    	                return true;
	    	            }
	                } 
            	} else {
            		return true;
            	}
            }
            return false;        	
        }
        
        //This is a function called from the "show more" button on the page, if there are more than x results on
        // the page, clicking show more will allow the display of more results : current increment = 20
        $scope.showMore = function(){
            var inc = 20;
            var limit = $scope.STATE.displayLimit;
            var resultLength = $scope.STATE.resultsLength;

            $scope.STATE.displayLimit = limit + inc;

            $scope.resolveState();
        };
        
        //The runtime sort function for changing the sort options once the application has loaded
        //due to the complex nesting of some directives, these functions need to be added to the rootscope
        $scope.sortFunction = function(option){
            switch(option){
                case "sortDate":
                    $scope.orderProp = "sortDate";
                    $scope.orderReverse = true;
                    break;
                default:
                    $scope.orderProp = option;
                    $scope.orderReverse = false;
                    break;
            }
        };
        
        $scope.resolveState = function() {
            if ($scope.STATE.resultsLength > $scope.STATE.displayLimit) {
                $scope.STATE.showmore = true;
            } else {
                $scope.STATE.showmore = false;
                $scope.STATE.displayLimit = $scope.STATE.resultsLength;
            }

            $scope.searchFailed = ($scope.STATE.resultsLength == 0);
        }

    }
]);
