'use strict';

/* Controllers */
var pearsonControllers = angular.module('pearsonPastPapersController', ['pearsonPastPapersFilters', 'pearsonCourseMaterialServices', 'pearsonServices']);

pearsonControllers.controller('AccordionCtrl', [
    '$scope',
    '$log',
    'QualificationFamily',
    'Subjects',
    'PastPapersDocuments',
    'ExamSeries',
    'pastPapersEvents',
    '$location' ,
    '$anchorScroll',
    '$element',
    '$filter',
    'Facet',
    function ($scope, $log, QualificationFamily, Subjects, PastPapersDocuments, ExamSeries, pastPapersEvents, $location, $anchorScroll, $element, $filter, Facet) {
    	
    	var appDom = $($element);
    	
    	$scope.QFsToIgnore = "";
    	$scope.pastPapersFacet = [];
    	//GA sample Stuff
        var dimensions = {
            "qualfam": "dimension6",
            "qualsubj": "dimension7",
            "examseries": "dimension8",
            "accredfromdate":"dimension9",
            "doctype":"dimension10",
            "specificationstatus":"dimension11"
        };
    	
        $scope.init = function (tags,resultDocTypes) {
        		console.log(resultDocTypes);
        		if(resultDocTypes){
        			$scope.pastPapersFacet = resultDocTypes.split(",");
        		}else{
        			$scope.pastPapersFacet = [ "Pearson-UK:Document-Type/Question-paper", "Pearson-UK:Document-Type/Mark-scheme",
           		                            "Pearson-UK:Document-Type/Examiner-report", "Pearson-UK:Document-Type/Listening-Examinations-MP3","Pearson-UK:Document-Type/Data-files" ];
           		
        		}
        		console.log($scope.pastPapersFacet);
        		$scope.QFsToIgnore = tags;
        		 $scope.getStarted();
        		
        };
        
        $scope.getStarted = function(){
        	var appDom = $($element);
            $scope.groupByFacet = ["UnitVal", "-docType"];
            //This is the value to be stamped on as a header for each group
            $scope.headerVal = "Unit";
            $scope.reverse = false;
            $scope.showSubjectResults = false;
            $scope.showLiveStatus = false;
            $scope.showArchivedStatus = false;
            QualificationFamily.getFamilies($scope.pastPapersFacet);
        };
    	      

        $scope.$watch(function () { return $location.search(); }, function (newUrl, oldUrl) {
            var qualificationFamilyInUrl = (newUrl["Qualification-Family"] !== undefined);
            var qualificationSubjectInUrl = (newUrl["Qualification-Subject"] !== undefined);
            var examSeriesInUrl = (newUrl["Exam-Series"] !== undefined);
            // flag indicating the Edit link was clicked
            var editPrev = (newUrl["editPrev"] !== undefined);

           

            if (!qualificationFamilyInUrl && !qualificationSubjectInUrl && !examSeriesInUrl && !editPrev) {
                $scope.selectedQualificationFamily = '';
                $location.hash('step1');
                $scope.openAccordion('#step1');
            }

            $scope.setGlobalVariables($location.search()['Qualification-Family'],
                $location.search()['Qualification-Subject'],
                $location.search()['Status'],
                $location.search()['Specification-Code'],
                $location.search()['Exam-Series']);

            if (qualificationFamilyInUrl && !qualificationSubjectInUrl && !examSeriesInUrl) {
                $location.hash('step2');
                $scope.getSubjects($location.search()['Qualification-Family']);
                $scope.setCurrentTabs(2);
            } else if (qualificationFamilyInUrl && qualificationSubjectInUrl && !examSeriesInUrl) {
                $location.hash('step3');
                if ($scope.groupedSubjects == undefined) {
                    $scope.getSubjects($location.search()['Qualification-Family']);
                }
                $scope.getExamSeries($location.search()['Qualification-Subject'],
                    $location.search()['Status'], $location.search()['Specification-Code']);
                $scope.setCurrentTabs(3);
            } else if (examSeriesInUrl && qualificationSubjectInUrl && examSeriesInUrl) {
                $location.hash('step4');
                if ($scope.groupedSubjects == undefined) {
                    $scope.getSubjects($location.search()['Qualification-Family']);
                }
                if ($scope.examSeries == undefined) {
                    $scope.getExamSeries($location.search()['Qualification-Subject'],
                        $location.search()['Status'], $location.search()['Specification-Code']);
                }
                $scope.getPastPapers($location.search()['Exam-Series'], $location.search()['Specification-Code']);
                $scope.setCurrentTabs(4);
            } else {
                $location.hash('step1');
                $scope.setCurrentTabs(1);
            }

            // $anchorScroll();
            // $location.hash('');
        });

        

        $scope.setGlobalVariables = function (qualificationFamily, qualificationSubject, status, specificationCodes, examSeries) {
            if (qualificationFamily != undefined) {
                $scope.qualificationFamily = qualificationFamily;
                $scope.selectedQualificationFamily = (qualificationFamily).replace(/-/g, ' ');
            }

            if (qualificationSubject != undefined && status != undefined) {
                $scope.qualificationSubject = qualificationSubject;
                $scope.status = status;
                if(status.split("/")[1] == "Live") {
                    $scope.statusText = 'Current';
                } else if(status.split("/")[1] == "Archived") {
                    $scope.statusText = 'Legacy';
                }
                $scope.selectedSubject = qualificationSubject + " (" + $scope.statusText + ")";
            }

            if (specificationCodes != undefined) {
                $scope.specificationCodes = [];
                if (typeof specificationCodes === 'string') {
                    $scope.specificationCodes.push(specificationCodes);
                } else {
                    $scope.specificationCodes = $scope.specificationCodes.concat(specificationCodes);
                }
            }
        };

        //when you init the page set the tabs up correctly
        $scope.setCurrentTabs = function (num) {
            appDom.find(".step").removeClass("selection");
            appDom.find(".step").removeClass("current");
            appDom.find("#step" + num).addClass("current");

            $scope.openAccordion("#step" + num);

            for (var i = 1; i < num; i++) {
                appDom.find("#step" + i).addClass("selection");
            }

            if (1 == num) {
                $scope.selectedQualificationFamily = '';
                $scope.selectedSubject = '';
                $scope.selectedExamSeries = '';
                $location.url($location.url().substring(0, $location.url().indexOf('Qualification-Family')));
            } else if (2 == num) {
                $scope.selectedSubject = '';
                $scope.selectedExamSeries = '';
                $location.url($location.url().substring(0, $location.url().indexOf('Qualification-Subject')));
            } else if (3 == num) {
                $scope.selectedExamSeries = '';
                $location.url($location.url().substring(0, $location.url().indexOf('Exam-Series')));
            }
        }

        $scope.openAccordion = function (tabId) {
            var tabDiv = tabId + " > div.ui-accordion-content";
            var activeTab = $(tabDiv);
            $('div.ui-accordion-content').not(activeTab).slideUp(500);

            $(activeTab).slideDown(500);
        };

        $scope.setSubjectsUrl = function (qualification) {
            $location.url('?Qualification-Family=' + qualification.facetValue);
            $scope.qualificationFamilyVar = "Pearson-UK:Qualification-Family/" + qualification.facetValue;
            $scope.selectedQualificationFamily = (qualification.facetValue).replace(/-/g, ' ');
        };

        $scope.getSubjects = function (qualification) {
            $scope.qualificationFamilyVar = "Pearson-UK:Qualification-Family/" + qualification;
            $scope.selectedQualificationFamily = (qualification).replace(/-/g, ' ');

            $scope.selectedSubject = "";
            $scope.groupedSubjects = [];

            $scope.subjectStatus = "Pearson-UK:Status/Archived";

            var tagsArray = []; tagsArray = tagsArray.concat($scope.pastPapersFacet);
            tagsArray.push($scope.qualificationFamilyVar, $scope.subjectStatus);

            Subjects.getSubjectsForQualification(tagsArray).then(function (subjects) {
                $scope.showArchivedStatus = (subjects.searchResults.solrDocuments.length > 0);
                $scope.showSubjectResults = (subjects.searchResults.solrDocuments.length > 0);
                $scope.groupedSubjects = Subjects.getSubjectsGroupedByFirstLetter(subjects.searchResults.solrDocuments,
                    $scope.subjectStatus);

                $scope.subjectStatus = "Pearson-UK:Status/Live";
//                $scope.subjectStatus = "Pearson-UK:Status/Live";

                var tagsArray = []; tagsArray = tagsArray.concat($scope.pastPapersFacet);
                tagsArray.push($scope.qualificationFamilyVar, $scope.subjectStatus);
                Subjects.getSubjectsForQualification(tagsArray).then(function (subjects) {
                    $scope.showLiveStatus = (subjects.searchResults.solrDocuments.length > 0);
                    $scope.showSubjectResults = (subjects.searchResults.solrDocuments.length > 0) ? true : $scope.showSubjectResults;

                    if ($scope.showLiveStatus) {
                        $scope.groupedSubjects = Subjects.getSubjectsGroupedByFirstLetter(subjects.searchResults.solrDocuments,
                            $scope.subjectStatus);
                    }else {
                    	if($scope.showArchivedStatus){
                    		$scope.subjectStatus = "Pearson-UK:Status/Archived";
                    	}
                    }

                    $scope.showTabs = $scope.showLiveStatus && $scope.showArchivedStatus;

                    $scope.showTitles = !$scope.showTabs;
                });
            });
        };

        $scope.setSort = function(facet){
            if(facet == "Unit"){
                //Sorts the units by a sortable value "unitVal" --This is added to the data via the result cleaner
                $scope.groupByFacet = ["UnitVal", "-docType"];
                //This is the value to be stamped on as a header for each group
                $scope.headerVal = "Unit";
            }else if(facet == "docType"){
                $scope.groupByFacet = ["-docType", "UnitVal"];
                $scope.headerVal = "docType";
            }
        }

        $scope.setExamSeriesUrl = function (subject) {
            $scope.specificationCodes = [];

            angular.forEach(subject.category, function (value, index) {
                if (value.lastIndexOf('Pearson-UK:Specification-Code', 0) === 0) {
                	var specArray = value.split("/");
                	if(specArray.length === 2){
                		$scope.specificationCodes.push(value);
                	}

                }
            });

            var specificationCodesUrlString = '';
            angular.forEach($scope.specificationCodes, function (value, index) {
                specificationCodesUrlString = specificationCodesUrlString + '&Specification-Code=' + value;
            });

            $location.url('?Qualification-Family=' + $scope.selectedQualificationFamily.replace(/ /g, '-') +
                '&Qualification-Subject=' + subject.title + '&Status=' + $scope.subjectStatus + specificationCodesUrlString);

        };

        $scope.getExamSeries = function (subject, status, specificationCodes) {
            if(status.split("/")[1] == "Live") {
                $scope.statusText = 'Current';
            } else if(status.split("/")[1] == "Archived") {
                $scope.statusText = 'Legacy';
            }
            $scope.selectedSubject = subject + " (" + $scope.statusText + ")";
            $scope.selectedExamSeries = "";

            var tagsArray = []; tagsArray = tagsArray.concat($scope.pastPapersFacet);

            if (specificationCodes != undefined) {
                if (typeof specificationCodes === 'string') {
                    tagsArray.push(specificationCodes);
                } else {
                    tagsArray = tagsArray.concat(specificationCodes);
                }
            }

            ExamSeries.getExamSeries(tagsArray);
        };

        $scope.setPastPapersUrl = function (examSeries) {
            var specificationCodesUrlString = '';
            angular.forEach($scope.specificationCodes, function (value, index) {
                specificationCodesUrlString = specificationCodesUrlString + '&Specification-Code=' + value;
            })

            $location.url('?Qualification-Family=' + $scope.qualificationFamily.replace(/ /g, '-')
                + '&Qualification-Subject=' + $scope.qualificationSubject + '&Status=' + $scope.status
                + specificationCodesUrlString + '&Exam-Series=' + examSeries.facetValue);

            $scope.selectedExamSeries = (examSeries.facetValue).replace(/-/g, ' ');

        };

        $scope.getPastPapers = function (examSeries, specificationCodes) {
            $scope.selectedExamSeries = (examSeries).replace(/-/g, ' ');

            var tagsArray = []; tagsArray = tagsArray.concat($scope.pastPapersFacet);
            tagsArray.push("Pearson-UK:Exam-Series/" + examSeries);

            if (specificationCodes != undefined) {
                if (typeof specificationCodes === 'string') {
                    tagsArray.push(specificationCodes);
                } else {
                    tagsArray = tagsArray.concat(specificationCodes);
                }
            }

            PastPapersDocuments.getPastPapers(tagsArray, function(data){
                $scope.documents = $filter('resultCleaner')(data.searchResults.solrDocuments);
            });

            /*Trigger the page init events*/
            pastPapersEvents.pageSetup(50);

            /*Add events to the scope to be used on the page*/
            $scope.pastPapersEvents = pastPapersEvents;
        };

        function areResultsSecure(data) {
            for(var i = 0; i < data.length; i++){
                var element = data[i];
                if(element.id.indexOf("/content/dam/secure") > -1) {
                    return true;
                }
            }
        }

        $scope.showZipDownload = function(data) {
        	if(typeof data == "undefined") {
        		return false;
        	}

            if(data.searchResults.solrDocuments.length == 1) {
                $(".ui-accordion-content.results").addClass("single-result");
                return false;
            } else if(areResultsSecure(data.searchResults.solrDocuments)) {
                return false;
            } else {
                $(".ui-accordion-content.results").removeClass("single-result");
                return true;
            }
        }

        $scope.downloadPastPapers = function (selectedDoc) {
            var eventCategory, eventAction, eventLabel, accredFromDate = "", docType = "";

            //Selected doc is undefined when you download the zip
            if(typeof selectedDoc === 'undefined') {
                var path = $scope.selectedQualificationFamily + "-" + $scope.selectedSubject +
                    "-" + $scope.selectedExamSeries;

                //The download path for the pastpapers zip
                var url = path + ".pastpapers.zip";
                url = url.concat("?filterQuery=" + encodeURIComponent("Pearson-UK:Exam-Series/") + encodeURIComponent($location.search()['Exam-Series']));

                //If there is an array of Spec codes loop over them
                if($location.search()['Specification-Code'] instanceof Array) {
                    angular.forEach($location.search()['Specification-Code'], function(specCode) {
                        url = url.concat("&filterQuery=" + encodeURIComponent(specCode));
                    });
                } else { //Otherwise add the one spec code
                    url = url.concat("&filterQuery=" + encodeURIComponent($location.search()['Specification-Code']));
                }

                //Loop over the past papers facets there are now more than one
                angular.forEach($scope.pastPapersFacet, function(facet){
                    url = url.concat("&categoryFacets=" + encodeURIComponent(facet));
                });

                url = url.concat("&_charset_=utf-8");

                window.location.replace(url);

                eventAction = "Download Zip";
                eventLabel = "Zip Download: " + path;
            }else{ //Single Document
                //Subject map allows us to get the doc based on the url
                var sMap            = new SubjectMap();
                var doc             = sMap.getSubject(selectedDoc);

                //get the accreditation from date for the document
                accredFromDate  = $filter('getLooseMatch')(doc.category, "Accreditation", true);
                docType  = $filter('getLooseMatch')(doc.category, "Document-Type", true);

                var accredFacet = new Facet(accredFromDate);
                var docFacet    = new Facet(docType);

                accredFromDate  =   (accredFacet.formattedFacetValue)?  " from "    +   accredFacet.formattedFacetValue     :   "";
                docType         =   (docFacet.formattedFacetValue)?     " | "       +   docFacet.formattedFacetValue        :   "";

                eventAction = "Download Course Materials";
                //210515---- eventLabel = "File Download: " + doc.title;
                eventLabel = path;
            }

            //Set the event category once -- If the accreditation from date or the document type is incorrect, it is
            //blank and does not affect the overall display on the page
            eventCategory = $scope.qualificationFamily.replace("-", " ") + " | " + $scope.qualificationSubject +
                 accredFromDate + " (" + $scope.statusText + ") | " + $scope.selectedExamSeries + " " + docType;

            //Google Analytics code - GA code
            if (typeof ga !== 'undefined') {
                //Init the query object
                var analyticsObj = {
                    'hitType':          'event',           // Required.
                    'eventCategory':    eventCategory,     // Required.
                    'eventAction':      eventAction,       // Required.
                    'eventLabel':       eventLabel
                };

                //set the custom variables for GA
                analyticsObj[dimensions.qualfam]            =   $scope.selectedQualificationFamily;
                analyticsObj[dimensions.qualsubj]           =   $scope.selectedSubject;
                analyticsObj[dimensions.examseries]         =   $scope.selectedExamSeries;
                analyticsObj[dimensions.accredfromdate]     =   accredFromDate.replace(" from ", "");  //HACK remove the "from" that will be placed inside the value
                analyticsObj[dimensions.doctype]            =   docType.replace(" | ", "");
                analyticsObj[dimensions.specificationstatus]=   $scope.statusText;

                ga('send', analyticsObj);
            }
        };

        //Creates a map based on the results that have been returned from the search results
        //it creates a map with the paths of the pdf's as the object reference
        function SubjectMap(){
            var list = {};

            angular.forEach($scope.data.searchResults.solrDocuments, function(subject){
                if(!list[subject.id.replace(" ", "-")]){
                    list[subject.id.replace(" ", "-")] = subject;
                }
            });

            this.getSubject = function(id){
                return list[id.replace(" ", "-")];
            }
        }
    }
]);

