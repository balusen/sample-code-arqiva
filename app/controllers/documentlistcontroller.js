'use strict';

/* Controllers */
var pearsonDocumentListController = angular.module('pearsonDocumentListController', ['pearsonDocumentListServices', 'pearsonPastPapersServices', 'pearsonCourseMaterialFilters', "pearsonServices", "pearsonFilters"]);

pearsonDocumentListController.controller('AccordionCtrl', ['$scope', '$rootScope', '$filter', 'DocumentListDocuments', 'pastPapersEvents', 'resolveShowMore', 'SolrDoc', 'analyticsService',

    function ($scope, $rootScope, $filter, DocumentListDocuments, pastPapersEvents, resolveShowMore, SolrDoc, analyticsService) {

        $scope.tags = "";
        $scope.specificationDocPath = "";

        $scope.init = function (documentSource, tags, documents, specificationDocPath) {
            $('div.ui-accordion-content').slideUp('fast');
            $('#step1 > div.ui-accordion-content').slideDown('fast');
            
            $scope.specificationDocPath = specificationDocPath.trim();
            //console.log("$scope.specificationDocPath " + $scope.specificationDocPath);
            
            if (documentSource === "documents") {
                $scope.documents = $scope.getDocumentsById(documents);
            } else {
                if (tags != null && tags != "[]") {
                    $scope.documents = $scope.getDocuments(tags);
                }
            }
        };

        $scope.getDocuments = function(tags) {
        	var tagArray = tags.replace("[", "").replace("]", "").split(",");
            var formattedTagArray = [];

            for (var x = 0; x < tagArray.length; x++) {
            	var tag = tagArray[x].trim();
                if (tag.substring(0,10) == "Pearson-UK") {
                    formattedTagArray.push( "category:\"" + tag + "\""  );
                } else if (tag.substring(0,4) == "type") {
                	formattedTagArray.push( "type:\"" + tag.substring(5) + "\""  );
                } else {
                    formattedTagArray.push( tag );
                }
            }

            DocumentListDocuments.getDocuments(formattedTagArray, function(data) {
                //Process the information
                var documents = $filter('resultCleaner')(data.searchResults.solrDocuments);
                $scope.documents = $filter('orderBy')(documents, 'title');
                $rootScope.STATE = ($rootScope.STATE || {});
                $rootScope.data = data;
                $rootScope.STATE.resultsLength = data.searchResults.solrDocuments.length;
                $rootScope.STATE.displayLimit = 10;
                resolveShowMore();

                $rootScope.pastPapersEvents = pastPapersEvents;
            });
        };

        $scope.analyticsClick = function(item) {
            analyticsService.sendEventFromDocument(item);
        };

        $scope.getDocumentsById = function(documents) {
            var documentArray           = documents.split(",");
            var formattedDocumentArray  = [];

            for (var x = 0; x < documentArray.length; x++) {
                formattedDocumentArray.push( "\"" + documentArray[x].trim() + "\"" );
            }

            DocumentListDocuments.getDocumentsById(formattedDocumentArray, function(data) {

                var documents = $filter('resultCleaner')(data.searchResults.solrDocuments);
                for ( var x = 0; x < documents.length; x++) {
                	var doc = documents[x];
                	var re = /(?:\.([^.]+))?$/;
                	var ext = re.exec(doc.url)[1];
                	//console.log("Obtained extension is",ext);
                	if(doc.extension == 'JPEG' && ext == "xlsm"){
                		doc.extension = ext;
                	}
                }
                //console.log('Inside getDocumentsById',documents);

                // Sort results into order specified in component
                var orderedDocuments = [];
                for (var x = 0; x < documentArray.length; x++) {
                	var matchResult = $scope.findDocument(documentArray[x].trim(), documents);
                	if(matchResult) {
                        //SolrDoc does all of the result cleaning stuff and makes it compliant with secure content
                		orderedDocuments.push(new SolrDoc(matchResult));
                	}
                }

                $scope.documents                = orderedDocuments;
                $rootScope.STATE                = ($rootScope.STATE || {});
                $rootScope.data                 = data;
                $rootScope.STATE.resultsLength  = $scope.documents.length;
                $rootScope.STATE.displayLimit   = 10;
                resolveShowMore();
                $rootScope.pastPapersEvents = pastPapersEvents;
            });
        };

        $scope.findDocument = function (id, array) {
            for ( var x = 0; x < array.length; x++) {
                if (array[x].id == id) {
                    return array[x];
                }
            }
        };
    }
]);

