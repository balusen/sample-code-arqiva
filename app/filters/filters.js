'use strict';

/* Filters */

var pearsonFilters = angular.module('pearsonFilters', [ ]);

pearsonFilters.filter('rendition', [function() {
    return function(rendition) {
        //return input ? '\u2713' : '\u2718';
    	if (typeof rendition != "undefined") {
	        if(rendition[1]){
	            return rendition[1]
	        }
	        else if(rendition[0]){
	            return rendition[0]
	        }
	        else{
	            return "#";
	        }
    	}
    };
}]);

pearsonFilters.filter('cleanHtml', [function() {
    return function(value) {
    	value = value.replace(/<[^>]*>/g, ""); 		//Get rid of html characters
    	value = value.replace(/&rsquo;/g, "'"); 	//replace encoded &rsquo; with '
    	value = value.replace(/&nbsp;/g, " "); 		//replace encoded &nbsp; with " " 
    	
    	
    	return value;
    }
}]);

pearsonFilters.filter('hasLooseMatch', [ "$filter", function($filter){
	return function(data, match){
		var result = $filter('getLooseMatch')(data, match);
		
		if (result.length > 0){
			return true;
		}else{
			return false;
		}
	}
}]);


//We want to filter the list - Excluding the entries that matched
pearsonFilters.filter('getLooseExclude', [ "$filter", function($filter) {
    return function(list, match) {
        var newList = [];
        var result = $filter('getLooseMatch')(list, match);

        //Add the results that were not in the results list
        angular.forEach(list, function(l) {
            if(result.indexOf(l) == -1 ) {
                newList.push(l);
            }
        });

        return newList;
    }
}]);

// Match items in a list based on a partial match e.g
// list ["aaabbbb", "aaaacccc", "aaaaaddddd"] can be matched on the value "aaa"
//
// You can also match items based on key value pairs e.g
// a list of objects [{ name: "herotime", legend: "The future", powers: "lazer beams" } ...]
// can be matched on on the value { name: "hero" } to return results
//
// The value in the object matcher can also be an array { name: ["hero", "villain", "reporter" }
// which returns the object that partially matches on any of these criteria
//
// When the option for #{ single } is true, the function returns a single value otehrwise the full
// array of matched values is returned
pearsonFilters.filter('getLooseMatch', [ function() {
    return function(list, match, single) {
    	var newlist = [];
    	if(match == undefined){
            return newlist;
        }
    	angular.forEach(list, function(data){
    		if(match instanceof Array){ 			//Is Array
    			angular.forEach(match, function(matcher) {
    				if(typeof matcher == 'object'){
    					$.each(matcher, function(key, value) {
                            if(data[key] instanceof Array) { //If the attribute is an array then iterate through to get the infromation
                                angular.forEach(data[key], function(entry) {
                                    if(String(entry).indexOf(value) > -1) {
                                        if(!$.contains(entry, newlist)) //make sure the filter only return unique values
                                            newlist.push(data);
                                    }
                                })
                            } else {
                                if(String(data[key]).indexOf(value) > -1){
                                    if(!$.contains(data[key],newlist)) //make sure the filter only return unique values
                                        newlist.push(data);
                                }
                            }
    					});
    				}else{
						if(data.indexOf(matcher) > -1){
		    				if(!$.contains(data,newlist)) //make sure the filter only return unique values
		    					newlist.push(data);
		        		}
    				}
    			});    			
    		}else{
    			if(typeof match == 'object'){
					$.each(match, function(key, value) {
                        if(value instanceof Array){
                            angular.forEach(value, function(entry){
                                if(String(data[key]).indexOf(entry) > -1){
                                    if(!$.contains(data[key],newlist)) { //make sure the filter only return unique values
                                        newlist.push(data);
                                    }
                                }
                            });
                        } else {
                            if(String(data[key]).indexOf(value) > -1){
                                if(!$.contains(data[key],newlist)) //make sure the filter only return unique values
                                    newlist.push(data);
                            }
                        }
					});
				}else{
					if(data.indexOf(match) > -1){
	    				if(!$.contains(data,newlist)) //make sure the filter only return unique values
	    					newlist.push(data);
	        		}
				}
    		}
    	});
    	if(single){
    		return newlist[0];
    	}else{
    		return newlist;
    	}
    }
}]);

pearsonFilters.filter('searchResultsFilter', [ function() {
    return function(collection, filtertags){
        var newlist = [];

        //Iterate through the results
        angular.forEach(collection, function(solrDoc) {
            //Check to see if the result is relevant
            if(validateDoc(solrDoc, filtertags)) {
                newlist.push(solrDoc);
            }
        });

        function validateDoc(doc, tags) {
            var matches = {};
            var result = true;
            var filterTags = tags;

            //check the facet groups available
            angular.forEach(filterTags, function(data){
                matches[data.replace("category:", "").split("/")[0]] = false;
            });

            //Check to see what facet groups apply to this doc
            for(var i = 0; i < filterTags.length; i++) {
                var cleanTag = filterTags[i].replace("category:", "");
                if (doc.category.indexOf(cleanTag) > -1) {
                    matches[cleanTag.split("/")[0]] = true;
                }
            }

            //To be included at one facet from each facet group must apply to this document
            $.each(matches, function(key, value){
                if(result) {
                    if (!value) result = false;
                }
            });

            //Several more matching rules can be implemented here

            return result;
        }

        return newlist;
    }
}]);


pearsonFilters.filter('cleanTags', [function() {
    return function(tag) {
    	if(tag != undefined){
			tag = tag.split("/");
			var val = tag[tag.length - 1]
			val = val.replace(/-/g, ' ');
			return val;
    	}else{
    		return "";
    	}
    }
}]);


pearsonFilters.filter('titleCaseFilter', [ function() {
    return function(cleanTitle) {

        if (cleanTitle == undefined) {
            return "";
        }
    	cleanTitle =  cleanTitle.replace(/-/g, ' ');
    	var ignoreWords = ['the','in','and'];
    	return cleanTitle.replace(/\w\S*/g, function(txt){
            if(ignoreWords.indexOf(txt)===-1){
            	if(txt.toLowerCase().indexOf("btec") === 0){
            		txt = txt.toUpperCase();	
            	}
              return txt.charAt(0).toUpperCase() + txt.substr(1);
            } else {
              return txt;
            }
          });

    };
}]);

/*
 * Iterates over the list and splits the list by the factor and then returns requested segment i.e
 *
 * list[].length = 8; factor = 2; segment = 1
 *
 * return list[][0-4]
 */
pearsonFilters.filter('splitList', function() {
    return function (input, factor, segment) {
        if(input && segment && input.length > 0){
        	var inputCopy = [];
		    for ( var i = 0; i < input.length; i++) { inputCopy.push(input[i]); }
            var segmentLength = Math.round(inputCopy.length / factor);
            return inputCopy.splice(segmentLength * (segment - 1), segmentLength * segment);
        }
        return input;
    };
});

/*pearsonFilters.filter('splitListNew', function() {
    return function (input, factor, segment) {
    	console.log('Inside SplitListNew',input,factor,segment);
        if(input && segment && input.length > 0){
        	var inputValue = input;
            var segmentLength = Math.round(inputValue.length / factor);
            return inputValue.splice(segmentLength * (segment - 1), segmentLength * segment);
        }
        return input;
    };
});

pearsonFilters.filter('dummyReturn', function() {
	return function (input){
		if(input){
			console.log('Dummy Return Input is :: ',input);
			var arrayCopy = [];
		    for ( var i = 0; i < input.length; i++) { arrayCopy.push(input[i]); }
			return arrayCopy;
		}
		return input;
	};
});*/

//check that the items in the list are unique, you can also check to see
//if the items are unique based on a certain field
pearsonFilters.filter("unique", function() {
    return function(list, uniqueField){
        var cleanList = [];
        var compareList = {};

        if(uniqueField){
            angular.forEach(list, function (data) {
                if (data[uniqueField]) { //it must contain that field
                    if (!compareList[data[uniqueField]]) {           //Doesn't have | data["sortValue"] = "hero" > compareList["hero"]
                        compareList[data[uniqueField]] = data[uniqueField];
                        cleanList.push(data);                       //Add the object
                    }
                }
            });

            return cleanList;
        }

        return list;
    }
});

pearsonFilters.filter("", [function(){
    return function(){


        return "";
    }
}]);

pearsonFilters.filter('sortKeyDates', [ "$filter", function ($filter) {
    return function (collection) {

        if (collection === null) return;

        if (typeof collection != "undefined") {
            var result = collection;

            result.sort(function(a, b) {
                var aDate = new Date(a.fromdate);
                var bDate = new Date(b.fromdate);

                var aYYYYMM = $filter('date')(a.fromdate, 'yyyy-MM');
                var bYYYYMM = $filter('date')(b.fromdate, 'yyyy-MM');

                if (aYYYYMM == bYYYYMM) {
                    return aDate - bDate;
                } else  {
                    return aDate-bDate;
                }
            });
            return result;
        }
    };
}]);


pearsonFilters.filter('removeOldKeyDates', [ "$filter", function ($filter) {
    return function (collection) {
    	console.log('Inside removeOldKeyDates');
    	var currentDate = new Date();
    	currentDate.setHours(0,0,0,0);
    	console.log(currentDate);
    	var resultList = [];
        if (collection === null) return;
        if (typeof collection != "undefined") {
            var result = collection;
            angular.forEach(collection, function (data) {
            	//console.log(data.fromdate,new Date(data.fromdate));
            	if(new Date(data.fromdate) >= currentDate){
            		resultList.push(data);
            	}
            });
            return resultList;
        }
    };
}]);
