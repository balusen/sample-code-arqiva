'use strict';

/* Filters */

var pearsonPastPapersFilters = angular.module('pearsonPastPapersFilters', []);

pearsonPastPapersFilters.filter('groupBy', [ function () {
    return function (collection, key) {
    	
    	console.log("Inside the pearsonPastPapersFilters");

        if (collection === null) return;

        if (typeof collection != "undefined") {
            var result = [];
            var unitTagStart = key + "/";
            var unitTag = "DEFAULT";

            for (var i = 0; i < collection.length; i++) {

               var value = collection[i]['category'];
               value = value.toString();
               var UnitVal = "DEFAULT";

               var indexOfUnitTagStart = value.indexOf(unitTagStart);

               if (indexOfUnitTagStart == -1) {
                   if (result.indexOf("Unknown Unit") == -1) {
                       result.push("Unknown Unit");
                   }
                   continue;
               }

               var indexOfNextComma = value.substring(indexOfUnitTagStart).indexOf(",");

               if (indexOfNextComma == -1) {
                    UnitVal = value.substring(indexOfUnitTagStart);
               } else {
                    UnitVal = value.substring(indexOfUnitTagStart).substring(0,indexOfNextComma);
               }

               UnitVal = UnitVal.replace(unitTagStart, "");
               UnitVal = UnitVal.split("/").pop();

               if (result.indexOf(UnitVal) == -1) {
                   result.push(UnitVal);
               }
            }

            result.sort();
            if (unitTagStart === "Pearson-UK:Document-Type/"){
                var sortedResult = [];
                var docTypes = [];
                
                angular.forEach($scope.pastPapersFacet, function(category){
            		docTypes.push(category.replace("Pearson-UK:Document-Type/",""));
            	});
                console.log(docTypes);
                
                if ($.inArray("Question-paper", result) > -1) {
                    sortedResult.push("Question-paper");
                }
                if ($.inArray("Mark-scheme", result) > -1) {
                    sortedResult.push("Mark-scheme");
                }
                if ($.inArray("Examiner-report", result) > -1) {
                    sortedResult.push("Examiner-report");
                }
                if ($.inArray("Listening-examinations-MP3", result) > -1) {
                    sortedResult.push("Listening-examinations-MP3");
                }
                if ($.inArray("Data-files", result) > -1) {
                    sortedResult.push("Data-files");
                }
                return sortedResult;
            } else if (unitTagStart === "Pearson-UK:Exam-Series/") {
                result.sort(function(a, b) {
                    return Date.parse(b.replace("-"," 1,")) - Date.parse(a.replace("-"," 1,"));
                });
            }
            return result;
        }
    };
}]);

pearsonPastPapersFilters.filter('sortBy', [ function() {
    return function(collection, facetId, sortByFacet, counter) {
    	var allowedCategories =  $scope.pastPapersFacet;

        var result = [];
        var questionPapers = [];
        var marksSchemes = [];
        var examinerReport = [];
        var listeningExaminations = [];
        var dataFiles = [];
        var others = [];
        var unitTagStart = sortByFacet + "/" + facetId;

        for (var i = 0; i < collection.length && i< counter; i++) {
            var value = collection[i]['category'];
            value = value.toString();

            var UnitVal;
            if(sortByFacet == 'Pearson-UK:Unit'){
                var indexOfUnitTagStart = value.indexOf(sortByFacet + "/");
                if(indexOfUnitTagStart > -1) {

                       var indexOfNextComma = value.substring(indexOfUnitTagStart).indexOf(",");

                       if (indexOfNextComma == -1) {
                        UnitVal = value.substring(indexOfUnitTagStart);
                       } else {
                        UnitVal = value.substring(indexOfUnitTagStart).substring(0,indexOfNextComma);
                       }

                       UnitVal = UnitVal.replace(unitTagStart, "");
                       UnitVal = UnitVal.split("/").pop();

                }
            }

            if (value.indexOf(unitTagStart) > -1 ||
                    (sortByFacet == 'Pearson-UK:Unit' && facetId == 'Unknown Unit'
                                     && value.indexOf('Pearson-UK:Unit') == -1) ||
                                     (UnitVal == facetId)) {
                /*if(value.indexOf("Pearson-UK:Document-Type/Examiner-report") > -1){
                    examinerReport.push(collection[i]);
                } else if(value.indexOf("Pearson-UK:Document-Type/Mark-scheme") > -1){
                    marksSchemes.push(collection[i]);
                } else if(value.indexOf("Pearson-UK:Document-Type/Question-paper") > -1){
                    questionPapers.push(collection[i]);
                } else if(value.indexOf("Pearson-UK:Document-Type/Listening-Examinations-MP3") > -1){
                    listeningExaminations.push(collection[i]);
                }else if(value.indexOf("Pearson-UK:Document-Type/Data-files") > -1){
                	dataFiles.push(collection[i]);
                }else {
                        others.push(collection[i]);
                }*/
            	
            	angular.forEach(allowedCategories, function(category){
            		if(value.indexOf(category) > -1){
            			result.push(collection[i]);
            		}else {
                        others.push(collection[i]);
                    }
            	});
            		
            	
            }
        }

       // result = questionPapers.concat(marksSchemes);
        //result = result.concat(examinerReport);
        //result = result.concat(listeningExaminations);
        //result = result.concat(dataFiles);
        result = result.concat(others);
        return result;
    };
}]);

pearsonPastPapersFilters.filter('cleanTitle', [ function(){
    return function(cleanTitle) {
    	if(cleanTitle == 'Unknown Unit'){
    		return "Unit not specified";
    	}
       return cleanTitle.replace(/-/g, ' ');
    };
}]);

pearsonPastPapersFilters.filter('cleanQFTitle', [ function() {
    return function(cleanTitle) {
    	if(cleanTitle == 'Unknown Unit'){
    		return "Unit not specified";
    	}

    	cleanTitle =  cleanTitle.replace(/-/g, ' ');
        return cleanTitle.replace( /(^| )(\w)/g, function(x){return x.toUpperCase();} );
    };
}]);


pearsonPastPapersFilters.filter('showQF', [ function(){
    return function(facets, QFsToIgnore) {
    	
    	var resList = [];
    	
    	angular.forEach(facets, function(data){
    		if(QFsToIgnore.indexOf(data.facet) > -1){
    			resList.push(data);
    		}
    	})
    	
    	return resList;
    };
}]);

pearsonPastPapersFilters.filter('titleInitials', [ function(){
     return function(titleInitials) {
         var result = '',
            letters = titleInitials.replace(/-/g, ' ').split(' ');

         for (var i = 0; i< letters.length; i++) {
             result += letters[i].charAt(0);
         }
        return result;
    };
}]);

pearsonPastPapersFilters.filter('extractDocumentType', [ function(){
    return function(extractDocumentType) {

        var result = '';
        var facetValue;
        for (var i = 0; i < extractDocumentType.length; i++) {
            if (extractDocumentType[i].indexOf('Pearson-UK:Document-Type') > -1) {
                facetValue = Pearson.wes.common.getFacetValue(extractDocumentType[i]);
            }
        }
         var letters = facetValue.replace(/-/g, ' ').split(' ');

        for (var i = 0; i< letters.length; i++) {
            result += letters[i].charAt(0);
        }
       return result;
   };
}]);

pearsonPastPapersFilters.filter('getQualificationSubjectTag', [ function(){
    return function(getQualificationSubjectTag) {
        var result = getQualificationSubjectTag.title;
        var tags = getQualificationSubjectTag.category;

        for (var i = 0; i < tags.length; i++) {
            if (tags[i].indexOf('Pearson-UK:Qualification-Subject') > -1) {
                result = Pearson.wes.common.getFacetValue(tags[i]);
            }
        }
        return result;
    };
}]);


pearsonPastPapersFilters.filter('filterPageProperties', [ function(){
    return function(modalSubject,value) {
        var result = "";
        if(modalSubject.pageProperties != undefined){
            var input = modalSubject.pageProperties;
            for (var i = 0; i < input.length; i++) {
                if(input[i].indexOf(value) > -1){
                    result = input[i].substring(input[i].indexOf(":") + 1);
                    break;
                }
            }
        }


        if(value.indexOf("Title") > -1 && result == ""){
            result = modalSubject.title;
            result = result.replace(/-/g, ' ');
        }
        return result;
    };
}]);


pearsonPastPapersFilters.filter('generateId', function() {
    return function(title) {
       return title.replace(" ", "-");
    };
});