'use strict';

/* Filters */

var pearsonNewsListFilters = angular.module('pearsonNewsListFilters', []);

pearsonNewsListFilters.filter('cleanNewsType', [function() {
    return function(inputString) {
        if(inputString === "news"){
            output = "Qualification news";
        } else {
            var output = inputString.replace(/-/g, ' ');
            if(output){
                output = output.substring(0, 1).toUpperCase() + output.substring(1).toLowerCase();
            }
        }
        return output;
    };
}]);

pearsonNewsListFilters.filter('displayDate', [function() {
    //Example input: "2014-06-23 | 15:54:10"
    return function(inputString) {
        var dateString = inputString.split('|')[0].trim();
        var dateParams = dateString.split("-");
        var date = new Date(dateParams[0], dateParams[1]-1, dateParams[2]);

        if(date){
            var monthNames = [ "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December" ];
            dateString = date.getDate() + " " + monthNames[date.getMonth()] + " " + date.getFullYear();
        }
        return dateString;
    };
}]);

pearsonNewsListFilters.filter('filterItems', [ function() {
    return function(list, pageTagsArray) {
        //this is hack - to filter the results returned from Solr as we can not combine all scenarios at the moment
        var newlist = [];
        var tagFilterList = ["Pearson-UK:Qualification-Family","Pearson-UK:Qualification-Subject","Pearson-UK:Accreditation-From-date","Pearson-UK:Specification-Code"];
        var searchTags = filterList(pageTagsArray, tagFilterList);
        var tagFilterList2 = ["Pearson-UK:Qualification-Family","Pearson-UK:Qualification-Subject","Pearson-UK:Specification-Code"];
        var tagFilterList3 = ["Pearson-UK:Qualification-Family","Pearson-UK:Accreditation-From-date"];
        var tagFilterListScenario6 = ["Pearson-UK:sector"];
        var searchTags2 = filterList(pageTagsArray, tagFilterList2);
        var searchTags3 = filterList(pageTagsArray, tagFilterList3);
        var searchTagsScenario6 = filterList(pageTagsArray, tagFilterListScenario6);
        var subjectFamilyTags = filterList(pageTagsArray,["Pearson-UK:Subject-Family"]);
        var exclude = ["Pearson-UK:Qualification-Subject","Pearson-UK:Specification-Code","Pearson-UK:Accreditation-From-date","Pearson-UK:sector","Pearson-UK:Subject-Family"];
        var exclude2 = ["Pearson-UK:Qualification-Subject"];
        var excludeScenario6 = ["Pearson-UK:Qualification-Subject","Pearson-UK:Specification-Code","Pearson-UK:Accreditation-From-date","Pearson-UK:Subject-Family"];
        
        //Adding scenario 7 - WES-942
        //Qual News:::  Qual family and subject family
        var tagFilterListScenario7 = ["Pearson-UK:Subject-Family"];
        var searchTagsScenario7 = filterList(pageTagsArray, tagFilterListScenario7);
        var excludeScenario7 = ["Pearson-UK:Qualification-Subject","Pearson-UK:Specification-Code","Pearson-UK:Accreditation-From-date","Pearson-UK:sector",];
        
        

        //                              Scenario 1  Scenario 2  Scenario 3  Scenario 4  Scenario 5  Scenario 6   Scenario 7
        // Qualification family             y           y           y           y                       y            y
        // Qualification subject            y           y           n           n                       n			 n
        // Accreditation from date          y           n           n           y                       n			 n			 
        // Spec code                        y           y           n           n                       n            n
        // Sector                           n/a         n/a         n           n/a                     y            n
        // subject family					n/a         n/a         n           n/a                     n            y

        // Page type - subject update                                                       y
        // Subject family                                                                   y

        //loop through the results and build the news list
        for(var i = 0; i < list.length; i++){
            // Scenario 5:
            // WHEN page-type=subject-update
            // MATCH: qualFamily - what we did the search on to start with
            // IF spec page has SF tag then allow 'subject-updates' that are tagged with the same SF tag
            if(list[i].pagetype == "subject-update"){
                if (subjectFamilyTags.length > 0) {
                    if (hasOneTag(list[i],subjectFamilyTags) && newlist.lastIndexOf(list[i]) == -1) {
                        newlist.push(list[i]);
                    }
                }
            }

            // Scenario 1: Has all tags
            else if(hasTags(list[i],searchTags) && newlist.indexOf(list[i])==-1){
                newlist.push(list[i]);
            }

            // Scenario 2: Has QF, QS, Spec-code and doesn't have
            // accreditation from date
            else if(hasTags(list[i],searchTags2) && newlist.indexOf(list[i])==-1){
                newlist.push(list[i]);
            }

            // Scenario 3:
            // MATCH: qualFamily - what we did the search on to start with
            // EXLCUDE qual-subject, spec-code, Accred datedate or sector
            else if (excludesTags(list[i],exclude) && newlist.lastIndexOf(list[i]) == -1) {
                newlist.push(list[i]);
            }

            // Scenario 4:
            // MATCH: qualFamily - what we did the search on to start with
            // EXLCUDE qual-subject, spec-code or Accred datedate
            else if (hasTags(list[i],searchTags3) && excludesTags(list[i],exclude2) && newlist.lastIndexOf(list[i]) == -1) {
                newlist.push(list[i]);
            }

            // Scenario 6:
            // MATCH: Has QF and sector
            // EXLCUDE qual-subject, spec-code or Accred datedate
            else if (hasTags(list[i], searchTagsScenario6)
                && excludesTags(list[i],excludeScenario6) && newlist.lastIndexOf(list[i]) == -1) {
                newlist.push(list[i]);
            }
            
         // Scenario 7:
            // MATCH: Has QF and subject family
            // EXLCUDE qual-subject, spec-code or Accred datedate or sector
            else if (hasTags(list[i], searchTagsScenario7)
                && excludesTags(list[i],excludeScenario7) && newlist.lastIndexOf(list[i]) == -1) {
                newlist.push(list[i]);
            }
        }

        function filterList(tagList, filter){
            var newTagList = [];
            for(var i = 0; i < tagList.length; i++){
                for(var j = 0; j < filter.length; j++){
                    if(tagList[i].indexOf(filter[j]) > -1){
                        if (newTagList.indexOf(tagList[i]) == -1){
                            newTagList.push(tagList[i]);
                        }
                    }
                }
            }
            return newTagList;
        };

        function hasTags(item, tags){
            var hasAllTags = true;
            for(var j = 0; j < tags.length; j++){
                if (item.category.indexOf(tags[j]) == -1){
                    hasAllTags = false;
                    break;
                }
            }

            return hasAllTags;
        };

        function hasOneTag(item, tags){
            var hasOneTag = false;
            for(var j = 0; j < tags.length; j++){
                if (item.category.indexOf(tags[j]) > -1){
                    hasOneTag = true;
                    break;
                }
            }

            return hasOneTag;
        };

        function excludesTags(item, tags){
            var excludesAllTags = true;
            for(var j = 0; j < tags.length; j++){
                if (item.category.toString().indexOf(tags[j]) > -1){
                    excludesAllTags = false;
                    break;
                }
            }

            return excludesAllTags;
        };


        return newlist;

    }
}]);

