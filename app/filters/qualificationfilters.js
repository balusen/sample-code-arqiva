'use strict';

/* Filters */

var pearsonQualificationFilters = angular.module('pearsonQualificationFilters', []);

pearsonQualificationFilters.filter('groupBy', [ function(){
        return function (collection, key) {

            if (collection === null) return;

            if (typeof collection != "undefined") {
                var result = [];
                
                if(key.lastIndexOf("Tags") != -1) {
                	for (var i = 0; i < collection.length; i++) {
                        var value = collection[i][key].split(",");
                        if (value.length > 1) {
                            for (var j = 0; j < value.length - 1; j++) {
                                if (result.indexOf(value[j]) == -1) {
                                    result.push(value[j]);
                                }
                            }
                        }
                    }
                } else {
                	//Default to most popular
                	result.push("popular");
                }
                
                result.sort();
                return result;
            }
        }
    }]);

pearsonQualificationFilters.filter('sortBy', [ function(){
    return function (collection, facetId, sortByFacet, counter) {
        var result = [];
        if (sortByFacet === 'popular') {
            for (var i = 0; i < collection.length && i < counter; i++) {
                result.push(collection[i]);
            }
        } else {
            for (var i = 0; i < collection.length; i++) {
                var tags = collection[i][sortByFacet].split(",");
                if (tags.length > 1) {
                    for (var j = 0; j < tags.length - 1; j++) {
                        if (tags[j] == facetId) {
                            result.push(collection[i]);
                        }
                    }
                }
            }
            result.sort(function(a, b){
                return a.title.localeCompare(b.title);
            });
        }
    
        if (sortByFacet === 'popular') {
        	return result;
        }
        
        //In order to generate list vertically, we will massage the list so that it can be added 
        // horizontally but list populated in the right order. And this is only applicable when
        // facetId NOT popular
        var resultLength = result.length;
        var split = parseInt(resultLength / 2 ) + (resultLength % 2);
        
        var result1List = [];
        var result2List = [];
        for (var i=0 ; i < resultLength; i++){
        	if(i < split){
        		result1List.push(result[i]);
        	} else {
        		result2List.push(result[i]);
        	}

        }
        var finalResult = [];
        for (var j = 0; j < resultLength; j++){

    		if((j + 1) <= result1List.length){
    			finalResult.push(result1List[j]);	
    		}
    		if((j + 1) <= result2List.length){
    			finalResult.push(result2List[j]);
    		}

        }
        
        return finalResult;
    };
}]);