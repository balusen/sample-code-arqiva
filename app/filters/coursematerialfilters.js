'use strict';

/* Filters */

var pearsonCourseMaterialFilters = angular.module('pearsonCourseMaterialFilters', ['pearsonCourseMaterialServices', 'pearsonServices']);

/*Models passed to the view that will filter out the correct information for the facet lists*/
pearsonCourseMaterialFilters.factory('filterFacets', [ function() {
    return {
        accreditiationFromYear: { accreditiationFromYear: 'true' },
        category: { category: 'true' },
        documentType: { documentType: 'true' },
        examSeries: { examSeries: 'true' },
        format: { format: 'true' },
        unit: { unit: 'true' },
        level: {level: 'true'}
    };
}]);


/* Disclaimer: Needs a real refactor -- needs to have logic abstracted to a model
 OR the information need to be more usable in terms of what is returned from solr */
pearsonCourseMaterialFilters.filter('resultCleaner', [ "$filter", "SolrDoc", function($filter, SolrDoc) {
    return function(dataList){
        var cleanData = [];
        angular.forEach(dataList, function(data){

            var cleanObj = new SolrDoc(data);

            cleanData.push(cleanObj);
        });

        return cleanData;
    };
}]);

pearsonCourseMaterialFilters.filter('addHeaders', [ "$filter", "Facet", function($filter, Facet){
    return function(dataList, headerProp){
        var cleanList = [];
        var header = "";

        angular.forEach(dataList, function(data){
            data.showHeader = false;
            data.header = "";

            if (headerProp == "title"){
                //We do not want to show titles when the lists are ordered alphabetically
//	            var attrField = data.title.charAt(0).toUpperCase();
//	            if (attrField != header){
//	                header = attrField;
////	                data.showHeader = true; //for the view
//	                data.header = attrField;//for the view
//	                header = data.header;   //for compare
//	            }
            }else if (headerProp == "examSeries"){
                if (data.examSeriesDisplay != header){
                    data.showHeader = true; //for the view
                    data.header = data.examSeriesDisplay;//for the view
                    header = data.header;   //for compare
                }
            }else if (headerProp == "date"){
                if (data.date != header){
                    //data.showHeader = true; //for the view
                    data.header = data[headerProp];//for the view
                    header = data.header;   //for compare
                }
            }else if (headerProp == "fromdate"){
                var mHeader = $filter('date')(data[headerProp], 'MMMM yyyy');

                if (mHeader != header){
                    data.showHeader = true;
                    data.header = 	mHeader;	//for the view
                    header 		= 	mHeader;   	//for compare
                }
            }else if (headerProp == "ukprice"){
                //Get the price tag
                var mHeader = $filter('getLooseMatch')(data.category, "Price/", true);
                var facet = new Facet(mHeader);

                if (facet.formattedFacetValue != header){
                    data.showHeader = true;
                    data.header = 	facet.formattedFacetValue;	//for the view
                    header 		= 	facet.formattedFacetValue;   	//for compare
                }
            } else { //Map data attributes directly
//                if(typeof headerProp == 'object'){
//                    for(var key in headerProp){
//                        var facet = $filter('getLooseMatch')(data[key], headerProp[key], true);
//
//
//                        break;
//                    }
//                }
                if (data[headerProp] != header){
                    data.showHeader = true; 			//for the view
                    data.header 	= data[headerProp];	//for the view
                    header 			= data.header;   	//for compare
                }
            }

            cleanList.push(data);
        });

        return cleanList;
    }
}]);

pearsonCourseMaterialFilters.filter('cleanUnit', [ "$location", "CONST", function($location, CONST) {
    return function(data){
        if(data.indexOf("Unit") > -1){
            var arr = data.split("/");
            if(arr){
                data = arr[arr.length -1];
            }
        }
        return data;
    }
}]);

pearsonCourseMaterialFilters.filter('groupFacets', [ "$location", "CONST", "$filter", function($location, CONST, $filter) {
    return function(collection, facetGroups){
        //Disclaimer : Needs to ba abstracted to a service object that
        // has accessor methods and functions to manipulate this data
        var newCollection = [
            { "name": "Categories", 	"id": "Category", 	"coll": [], "allCheck": false, "isEmpty": true,
                "inputType": "radio", "showAll": false, "change": true, "displayLimit": 5, "sort": ["sortValue",'formattedFacetValue'] },
            { "name": "Content type","	id":"document-type","coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "" },
            { "name": "Qualification size","	id":"qualification-size","coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "" },
            { "name": "Level","	id":"level","coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "facet" },
            { "name": "Skill","	id":"skill","coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "" },
            { "name": "Exam Series", 	"id":"exam-series", "coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "-sortValue" },
            { "name": "Unit", 			"id":"unit", 		"coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "sortValue" },
            { "name": "Format", 		"id":"format", 		"coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "" },
            { "name": "Resource Type", 	"id":"resource-type", 	"coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "" },
            { "name": "Publisher", 		"id":"publisher", 	"coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "" },
            { "name": "Price Range",    "id":"price", 	    "coll": [], "allCheck": true, "isEmpty": true,
                "inputType": "checkbox", "showAll": true, "change": true, "displayLimit": 4, "sort": "sortValue" }
        ];

        angular.forEach(collection, function(value){
            value.checked = false;

            angular.forEach(facetGroups, function(groupName){
                var constGroup = groupName.replace("-", "").toUpperCase();
                if(value.facetKey.indexOf(groupName) > -1){
                    newCollection[CONST[constGroup]].coll.push(value);
                    newCollection[CONST[constGroup]].isEmpty = false;
                    if ($location.hash().indexOf(value.facet) > -1) {
                        newCollection[CONST[constGroup]].allCheck = false;
                    }
                }
            });
        });

        //Categories were not ordering correctly ------------ this is a FIX -----------
        for(var i = 0; i < newCollection[CONST.CATEGORY].coll.length; i++){
            if (newCollection[CONST.CATEGORY].coll[i].formattedFacetValue == "Specification and sample assessments") {
                newCollection[CONST.CATEGORY].coll.unshift(newCollection[CONST.CATEGORY].coll[i]);
                newCollection[CONST.CATEGORY].coll.splice(i+1, 1);
            }
        }

        return newCollection;
    }
}]);

//The reordering of the facets should be abstracted to the "model" for the facetGroups
pearsonCourseMaterialFilters.filter('reorderFacets', [ "CONST", function(CONST){
    return function(collection, option){
        //reset the sorting
        collection[CONST.DOCUMENTTYPE].coll.sort(function(a, b){
            return a.formattedFacetValue.localeCompare(b.formattedFacetValue);
        });

        if (option == "questionPaper"){
            for (var i = 0; i < collection[CONST.DOCUMENTTYPE].coll.length; i++){
                if (collection[CONST.DOCUMENTTYPE].coll[i].formattedFacetValue == "Examiner report") {
                    collection[CONST.DOCUMENTTYPE].coll.unshift(collection[CONST.DOCUMENTTYPE].coll[i]);
                    collection[CONST.DOCUMENTTYPE].coll.splice(i+1, 1);
                }
            }
            for (var i = 0; i < collection[CONST.DOCUMENTTYPE].coll.length; i++){
                if (collection[CONST.DOCUMENTTYPE].coll[i].formattedFacetValue == "Mark scheme") {
                    collection[CONST.DOCUMENTTYPE].coll.unshift(collection[CONST.DOCUMENTTYPE].coll[i]);
                    collection[CONST.DOCUMENTTYPE].coll.splice(i+1, 1);
                }
            }
            for (var i = 0; i < collection[CONST.DOCUMENTTYPE].coll.length; i++){
                if (collection[CONST.DOCUMENTTYPE].coll[i].formattedFacetValue == "Question paper") {
                    collection[CONST.DOCUMENTTYPE].coll.unshift(collection[CONST.DOCUMENTTYPE].coll[i]);
                    collection[CONST.DOCUMENTTYPE].coll.splice(i+1, 1);
                }
            }
        }if (option == "specification"){
            for (var i = 0; i < collection[CONST.DOCUMENTTYPE].coll.length; i++){
                if (collection[CONST.DOCUMENTTYPE].coll[i].formattedFacetValue == "Specification") {
                    collection[CONST.DOCUMENTTYPE].coll.unshift(collection[CONST.DOCUMENTTYPE].coll[i]);
                    collection[CONST.DOCUMENTTYPE].coll.splice(i+1, 1);
                }
            }
        }

        return collection;
    }
}]);



pearsonCourseMaterialFilters.filter('examSeriesGroupBy', [ "$filter", function($filter){
    return function(dataList,option){
        if(option == "examSeries"){
            //console.log("Inside Exam series sorting");
            var noExamSeriesList=[];
            var resultList = [];
            var foundNoExamSeries=false;
            var noExamSeriesHeader = "No Exam Series";
            angular.forEach(dataList, function(value) {
                if(value.header){
                    if(noExamSeriesHeader == value.header)
                        foundNoExamSeries = true
                    else
                        foundNoExamSeries = false
                }
                if(foundNoExamSeries){
                    var result = value;
                    result.header="";
                    result.showHeader=false;
                    noExamSeriesList.push(result);
                }else{
                    resultList.push(value);
                }
            });
            //console.log(dataList);
            //console.log('The unsorted sublist is :: ',noExamSeriesList);
            noExamSeriesList = $filter('orderBy')(noExamSeriesList,'title');
            if(noExamSeriesList.length >0){
                noExamSeriesList[0].header=noExamSeriesHeader;
                noExamSeriesList[0].showHeader = true;
            }
            //console.log('The sorted sublist is :: ',noExamSeriesList);
            resultList=resultList.concat(noExamSeriesList);
            //console.log("Appended list is :: ",resultList);
            return resultList;
        }else{
            return dataList;
        }
    }
}]);

pearsonCourseMaterialFilters.filter('getFirstParagraph', [ "$filter", function($filter){
    return function(description){
        if (description) {
            var paragraphs = description.split("</p>");
            var shortDesc = "";
            var trail;
            var numberOfWords = 20;

            // Doesn't contain any paragraphs so split on 20 words
            if (paragraphs[0] == description) {
                return $filter('getFirst20Words')(description);
            }

            shortDesc = paragraphs[0];
            shortDesc = shortDesc.replace("<p>", "");
            shortDesc = shortDesc.replace("</p>", "");
            shortDesc = shortDesc.trim();
            shortDesc = shortDesc.split(".")[0]

            trail = (paragraphs.length > 1 && paragraphs[1] != "") ? "..." : ".";
            return shortDesc + trail;
        }
    }
}]);

pearsonCourseMaterialFilters.filter('hasSecondParagraph', [ "$filter", function($filter){
    return function(description){
        if (description) {
            var paragraphs = description.split("</p>");

            if (paragraphs[0] == description) {
                return $filter('hasMoreThan20Words')(description);
            }
            return (paragraphs.length > 1 && paragraphs[1] != "");
        }
    }
}]);

pearsonCourseMaterialFilters.filter('getFirst20Words', [ function(){
    return function(description){
        if (description) {
            var shortDesc = "";
            var numberOfWords = 20;

            var words = description.split(" ");

            for (var x =0; x < words.length && x < numberOfWords; x++) {
                if (x != 0) { shortDesc = shortDesc + " "}
                shortDesc = shortDesc + words[x];
            }

            var trail =  (words.length > numberOfWords) ? "..." : "";
            return shortDesc + trail;
        }
    }
}]);

pearsonCourseMaterialFilters.filter('hasMoreThan20Words', [ function(){
    return function(description){
        if (description) {
            var words = description.split(" ");
            return (words.length > 20);
        }
    }
}]);
