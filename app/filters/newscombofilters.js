'use strict';

/* Filters */

var pearsonNewsComboFilters = angular.module('pearsonNewsComboFilters', []);

pearsonNewsComboFilters.filter('customFilter', [function() {
	return function(newsList, selectedArticle) {
		var cleanList = [];
		angular.forEach(newsList, function(news){
			var hide = jQuery.inArray('hideOnFeatureComponents:true', news.pageProperties) > -1;
	        var isSelected = (news.id === selectedArticle) ? true : false;
			if(!hide && !isSelected){
	        	cleanList.push(news);
	        }
		});
		return cleanList;
	}
}]);







