'use strict';

/* Filters */

var searchFilters = angular.module('searchFilters', ['pearsonFilters']);

searchFilters.filter('generateDocInfo', ["$filter", function($filter) {
    return function(categories) {

        var qf =  $filter('getLooseMatch')(categories, ["Qualification-Family"], true);
        qf = $filter('cleanTags')(qf);
        var qs =  $filter('getLooseMatch')(categories, ["Qualification-subject"], true);
        qs = $filter('cleanTags')(qs);
        var dt =  $filter('getLooseMatch')(categories, ["Document-Type"], true);
        dt = $filter('cleanTags')(dt);
        var afd =  $filter('getLooseMatch')(categories, ["Accreditation-From-date"], true);
        afd = $filter('cleanTags')(afd);
        var c =  $filter('getLooseMatch')(categories, ["Category"], true);
        c = $filter('cleanTags')(c);

        var info = "";
        if (qf != "") {
            info = info + qf;
        }
        if (qs != "") {
            if (info.length > 0 && info.substr(info.length - 2) != "| ") {
                info = info + " | ";
            }
            info = info + qs;
        }
        if (dt != "") {
            if (info.length > 0 && info.substr(info.length - 2) != "| ") {
                info = info + " | ";
            }
            info = info + dt;
        }
        if (c != "") {
            if (info.length > 0 && info.substr(info.length - 2) != "| ") {
                info = info + " | ";
            }
            info = info + c;
        }
        if (afd != "") {
            if (info.length > 0 && info.substr(info.length - 2) != "| ") {
                info = info + " | ";
            }
            info = info + afd;
        }
        return info;
    };
}]);

searchFilters.filter('getTarget', [function($filter) {
    return function(docType) {
        return (docType == "Webpage") ? "_self" : "_blank";
    };
}]);