'use strict';

/* Filters */

var progressiveDisclosureFilters = angular.module('progressiveDisclosureFilters', []);

progressiveDisclosureFilters.filter('getFirstSentence', [ function(){
	return function(description){
		var shortDesc = description.split(".")[0];
        var trail = (description.split(".").length > 1)? "..." : ".";
		return shortDesc + trail;
	}
}]);

progressiveDisclosureFilters.filter('hasSecondSentence', [ function(){
    return function(description){
        if(description){
            return description.split(".").length > 1
        }
    }
}]);

progressiveDisclosureFilters.filter('addHash', [ function(){
	return function(href){ //Pass in the category tags and return true if there is an Online-Event 
		return "#"+href;
	}
}]);

progressiveDisclosureFilters.filter('encodeBulletPoints', [ function(){
	return function(text){ //Pass in the category tags and return true if there is an Online-Event 
		text = text.replace(/\n/g, "<br />");
		text = text.replace(":", ":<br />");
		
		return text;
	}
}]);

progressiveDisclosureFilters.filter('isOnlineEvent', [ function(){
	return function(list){ //Pass in the category tags and return true if there is an Online-Event 
		var isOnline = false;
		angular.forEach(list, function(data){
			if(data.indexOf('Online-Event') > -1){
				isOnline = true;
			}
		});
		return isOnline;
	}
}]);

progressiveDisclosureFilters.filter('uniqueMonths', [ "$filter", function($filter){
	return function(list){ 
		//get month matches
		var months = [];
		//results list
		var resList = [];

		angular.forEach(list, function(data){
			var date = $filter('date')(data.facetValue, 'MMMM');
			if($.inArray(date, months) === -1){
 				resList.push(data);
				months.push(date); //Only place in months that aren't there
			}
		});

		var monthsList = {
			January: 0, February: 1, March: 2, April: 3, May: 4, June: 5,
			July: 6, August: 7, September: 8, October: 9, November:10, December:12
		};

		resList.sort(function (a, b) {
			var adate = $filter('date')(a.facetValue, 'MMMM');
			var bdate = $filter('date')(b.facetValue, 'MMMM');
			return monthsList[adate] - monthsList[bdate];
		});

		return resList;
	}
}]);

progressiveDisclosureFilters.filter('getReadableTime', [ function(){
	return function(time){ //Pass in the time and then format it 
		var timeArr = time.split(":");
		var a = "pm";
		var newTime = "";

		//sort out the hours
		if((timeArr[0] - 12) > 0){
			newTime += (timeArr[0] - 12);
		}else if(timeArr[0] == 12){
			newTime += timeArr[0];
		}else{
			newTime += timeArr[0];
			a = "am";
		}

		//sort out the minutes
		if(timeArr[1] != "00"){
			newTime += ":"+timeArr[1];
		}
		
		newTime += a;
		
		return newTime;
	}
}]);

progressiveDisclosureFilters.filter('extractUniqueMonths', [ "$filter", function($filter){
	return function(list){ 
		var months = [];
		
		angular.forEach(list, function(data){
			var date = $filter('date')(data.fromdate, 'MMMM yyyy');

			if($.inArray(date, months) === -1){
				months.push(date);
			}
   	});

	    var ad = new Date(),
        bd = new Date(),
        monthsList = {
            January: 0, February: 1, March: 2, April: 3, May: 4, June: 5,
            July: 6, August: 7, September: 8, October: 9, November:10, December:11
        };

    months.sort(function (a,b) {
        var as = a.split(' '),
            bs = b.split(' ');

        ad.setFullYear(as[1]);
        ad.setMonth(monthsList[as[0]]);
        bd.setFullYear(bs[1]);
        bd.setMonth(monthsList[bs[0]]);

        return ad - bd;
    });
    
   	return months;

	}
}]);



progressiveDisclosureFilters.filter('sortExamSeries', [ "$filter", function($filter){
	return function(list){ 

		var results = list;
		
        var monthsList = {
                January: 0, Feburary: 1, March: 2, April: 3, May: 4, June: 5,
                July: 6, August: 7, September: 8, October: 9, November:10, December:12
            };
		
		angular.forEach(results, function(data){
			var formattedDate = data.formattedFacetValue;
			
			var date = new Date();
			date.setFullYear(formattedDate.split(' ')[1]);
			date.setMonth(monthsList[formattedDate.split(' ')[0]]);
			date.setDate("01");
			data.examSeriesDate = date;

   	});
		return results;
	}
}]);


progressiveDisclosureFilters.filter('removeNonNumbers', [ function(){
	return function(alphaNumericString){
        //var eventId = eventIdFullString.substr(1, 4);
        var numbers = alphaNumericString.replace(/[^0-9\.]+/g, '');
		return numbers;
	}
}]);

progressiveDisclosureFilters.filter('publisherGroupByOrderBy', [ "$filter",function($filter){
	return function(dataList,orderByParams){
		if(dataList){
			console.log('publisherGroupByOrderBy',dataList.length);
			var pearsonPublished=[];
			var nonPearsonPublished=[];
			 angular.forEach(dataList, function(data){
				 var pearsonPublisher = $filter('getLooseMatch')(data.category, "Pearson-UK:Publisher/Pearson", true);
				 if(pearsonPublisher){
					 pearsonPublished.push(data);
				 }else{
					 nonPearsonPublished.push(data);
				 }
			 });
			 var returnData = [];
			 if(orderByParams){
				 pearsonPublished = $filter('orderBy')(pearsonPublished,orderByParams);
				 nonPearsonPublished = $filter('orderBy')(nonPearsonPublished,orderByParams);
			 }
			 returnData=returnData.concat(pearsonPublished);
			 returnData=returnData.concat(nonPearsonPublished);
			 return returnData;
		}
	};
}]);