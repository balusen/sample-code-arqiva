var CourseMaterialsConstants = angular.module('CourseMaterialsConstants', []);

CourseMaterialsConstants.constant('CONST', {
    CATEGORY : 0,
    DOCUMENTTYPE : 1,
    QUALIFICATIONSIZE: 2,
    LEVEL: 3,
    SKILL: 4,
    EXAMSERIES : 5,
    UNIT : 6,
    FORMAT : 7,
    RESOURCETYPE : 8,
    PUBLISHER : 9,
    PRICE : 10,
    paidResources : "type:\"cq:PaidResource\"",
    damAsset:       "type:dam:Asset",
    categoryExclusionList : [
        "Support",
        "Testing",
        "Teaching-delivery",
        "Centre-administration",
        "Exams",
        "Assessment-verification",
        "Quality-assurance",
        "Registration-and-entries",
        "Registrations-and-entries",
        "Results",
        "Understanding-our-qualifications"
    ]
});