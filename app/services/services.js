'use strict';

/* Services */
/*
    You can use the $rootScope to interact with the scope of the
    application and modify variables that are relevant to the page
*/
var pearsonServices = angular.module('pearsonServices', ['ngResource', 'LocalStorageModule', 'pearsonFilters']);

/* Query factory : Use the create query function to enter a url into the ajax request -- */
pearsonServices.factory('Query',['$resource',
    function($resource){
        return {
            createQuery: function(url, params){
                return $resource(url, {}, {
                    query: { method:'GET', params: params, responseType: "json" }
                });
            }
        }
    }
]);

pearsonServices.service("appScope", [ "$rootElement", function($rootElement){
    this.getScope = function(){
        var element = $rootElement;
        return $(element).scope();
    }
}]);

//Changed this to a factory so that you can return more than one instance of the Query Object
//and for it to have its own configuration
pearsonServices.factory("QuerySolr", [ "Query", "$rootScope", "localStorageService",
    function(Query, $rootScope, localStorageService) {
        function QuerySolr() {
            var result;
            var initQuery;

            this.configQuery = function (path) {
                initQuery = path;//Query.createQuery(path);
            }

            this.query = function (params, callback) {
                $rootScope.loading = true;
                var query = Query.createQuery((initQuery || "/services/pearson/search/GET.servlet"));//(|| Query.createQuery("/services/pearson/search/GET.servlet"));
                result = query.get(params, function (data) {
                    $rootScope.loading = false;

                    if (callback) {
                        callback(data);
                    } else {
                        $rootScope.data = data;
                    }
                });

                return result;
            }
        }

        return new QuerySolr();
}]);

pearsonServices.service('resolveShowMore', [ "$rootScope", function($rootScope) {
    return function() {
        if ($rootScope.STATE.resultsLength > $rootScope.STATE.displayLimit) {
            $rootScope.STATE.showmore = true;
        } else {
            $rootScope.STATE.showmore = false;
            $rootScope.STATE.displayLimit = $rootScope.STATE.resultsLength;
        }
    }
}]);

//var config = (rootElement).find(".config");

pearsonServices.factory("pageConfig", [ "$rootElement",
    function(rootElement) {

        //loop through the key and values of the configuration
        function parseConfig(that, element) {
            if((element).data("angularConfig")) {
                $.each((element).data("angularConfig"), function (key, value) {
                    that[key] = value;
                });
            }
        }

        function Config(element) {
            //this.config = $(rootElement).data("angularConfig");
            parseConfig(this, element);
        }

        Config.prototype.attachAttributes = function(object){
            //Get all the properties from this object > attach them to the passed in object
            for(var key in this){
                object[key] = this[key];
            }
        }

        return new Config(rootElement);
    }
]);

pearsonServices.factory("Facet", [ "$filter", "DateUtil", function($filter, DateUtil){
    function setFormattedValue(fKey, fVal){
        if(fKey.indexOf("Price") > -1) {
            return "£" + fVal.replace("-", "-£");
        } else if(fKey.indexOf("Unit") > -1) {
        	fVal = fVal.replace(/-/g, " ");
        	//put back one dash in case of unit facets
        	return fVal.replace("   ", " - ");
        } else { //default
            return capitilize(fVal).replace(/-/g, " ");
        }
    }

    function capitilize(word) {
        if(word) {
            return word.charAt(0).toUpperCase() + word.substr(1);
        }
        return "";
    }


    function setSortValue(facet){
        if(facet.indexOf(":Price") > -1){
            return parseInt(facet.match(/-?\d+/)[0], 10);
        }else if(facet.indexOf(":Unit") > -1){
            if(facet.match(/\d+/) != null){
                var unit = facet.match(/\d+/)[0];
                if(unit.length == 1){
                    var newUnit = "00" + unit;
                    return facet.replace(unit, newUnit);
                }
                else if(unit.length == 2){
                    var newUnit = "0" + unit;
                    return facet.replace(unit, newUnit);
                }
                //default
                else{
                    return facet;
                }
            }
        }else if(facet.indexOf(":Exam-Series") > -1){
            var date = DateUtil.resolveExamSeriesDate(facet.split("/")[facet.split("/").length -1].replace("-", " "));
            return date;
        }else if(facet.indexOf(":Category") > -1){
            //Category Facets ordering -- Alphabetical > Specification always goes first
            if(facet.indexOf("Specification") > -1){
                return "aaaaaaaaaaa";
            }else{
                return facet.formattedFacetValue;
            }
        }

        return "";
    }

    return function(facetString) {
        if(facetString) {
            this.facet = facetString;
            this.count = 1;
            this.facetValue = facetString.split("/")[facetString.split("/").length - 1];
            this.facetKey = facetString.split("/")[0];
            this.formattedFacetValue = setFormattedValue(this.facetKey, this.facetValue);
            this.sortValue = setSortValue(this.facet);
        }
    }
}]);

pearsonServices.service('facetHandler',['$filter', 'Facet', function($filter, Facet){
    var finalList       = [];
    var facetContainer  = {};

    function clearCounts(){
        if(finalList.length > 0){
            angular.forEach(finalList, function(facet){
                facet.count = 0;
            });
        }
    }

        this.getFacetsFromDocuments = function(documents) {
            var fullFacetList = [];
            clearCounts();

            //Step one: Get all the facets into one list
            angular.forEach(documents, function (data) {
                fullFacetList = fullFacetList.concat(data.category);
            });

            //Step two: get the facet information out and do a count
            angular.forEach(fullFacetList, function (data) {
                if (facetContainer[data]) {
                    facetContainer[data].count = facetContainer[data].count + 1;
                } else {
                    facetContainer[data] = new Facet(data);

                    //Push a reference to the flat and final list
                    finalList.push(facetContainer[data]);
                }
            });

            return finalList;
        }
        this.getFacetsFromList = function(stringList) {
            var facetList = [];
            angular.forEach(stringList, function (data) {
                facetList.push(new Facet(data));
            });

            return facetList;
        }
        this.clearFacetList = function() {
            finalList = [];
            facetContainer = {};
        }
        this.getFacetsFromFacets = function(facetList, discard){
            var list = [];
            angular.forEach(facetList, function (facet) {
                //See if results have a leaf value
                if(facet.facet.split("/").length && facet.facet.split("/").length > 1 && facet.facet.split("/")[1] !== "") {
                    list.push(new Facet(facet.facet));
                }
            });

            if(!discard){
                facetList.push(list);
            }

            return list;
        }
    }
]);

pearsonServices.factory("SolrDoc", [ "DateUtil", function(DateUtil){
        //Get the document and assign all its values to this
        function assignToThis(that, doc){
            for(var att in doc){
                that[att] = doc[att];
            }
        };

        function resolveRendition(doc, entity) {
            if(doc.extension != undefined && doc.rendition != undefined) {
                if(doc.rendition[0].indexOf("#") == 0) {
                    entity.rendition = "/etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/images/no_image.png";
                } else if (doc.extension.toLowerCase() != ("pdf" && "html") || doc.rendition == (null || undefined)){
                    entity.rendition = "/etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/images/icons/" + doc.extension.toLowerCase() + ".png";
                } else if (doc.rendition instanceof Array) {
                    var filteredRendtion = jQuery(doc.rendition).filter(function () {
                        return (this.indexOf("319.319") > -1 );
                    });

                    if (filteredRendtion[0] && filteredRendtion[0].length) {
                        entity.rendition = filteredRendtion[0];
                    } else if (entity.ukprice && entity.rendition[0]) {
                        if (entity.rendition[0].indexOf("http") == -1) {
                            //If there is a "/" at the beggining of the string, get rid of it
                            if (entity.rendition[0].indexOf("/") == 0) {
                                entity.rendition = entity.rendition[0];
                            } else {
                                entity.rendition = "http://" + entity.rendition[0];
                            }
                        } else {
                            entity.rendition = entity.rendition[0];
                        }
                    } else {
                        entity.rendition = "/etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/images/icons/" + doc.extension.toLowerCase() + ".png";
                    }
                }
            }

            // ---- EXTENSION ---- //
            if(doc.extension && entity.category){
                entity.category.push("Pearson-UK:Format/" + doc.extension.toLowerCase());
            }
        }

        function resolveExamSeries(doc, entity){
            // ---- EXAM SERIES ----
            var filteredExam = jQuery(doc.category).filter(function() {
                return (this.indexOf(":Exam-Series") > -1 );
            });
            if(filteredExam[0] && filteredExam[0].length) {
                for (var i = 0; i < filteredExam.length; i += 1){
                    var hero = filteredExam[0].split("/")[1]
                    if(hero){
                        entity.examSeries = hero.replace(/-/g, " ");

                        if(doc.examSeries == "No date"){
                            entity.examSeries = "1999-09-09";
                            entity.examSeriesDisplay = "No Exam Series";
                        }
                        else {
                            entity.examSeriesDisplay = doc.examSeries;
                            entity.examSeries = DateUtil.resolveExamSeriesDate(doc.examSeries);
                        }

                        break;
                    }
                }
            }else{
                entity.examSeries = "1999-09-09";
                entity.examSeriesDisplay = "No Exam Series";
            }
        }

        function resolveDate(doc, entity){
            //HAAAACKKKKK -  DATE FILTERING
            if (doc.date){
                entity.date = doc.date.split("|")[0];
                var num     = entity.date.match(/\d+/);
                var month   = entity.date.match(/\D+/);

                entity.sortDate = DateUtil.resolveExamSeriesDate(month + " " + num[1], num[0]);
            }
        }

        function resolveDocumentType(doc, entity){
            // ---- DOCUMENT TYPE ---- //
            var filteredDoc = jQuery(doc.category).filter(function() {
                return (this.indexOf("Document-Type") > -1 );
            });
            if(filteredDoc[0] && filteredDoc[0].length) {
                for (var i = 0; i < filteredDoc.length; i += 1){
                    var hero = filteredDoc[0].split("/")[1];
                    if(hero){
                        entity.docType = hero.replace(/-/g, " ");
                        break;
                    }
                }
            }
        }

        function resolveUnit(doc, entity){
            // ---- UNIT ---- //
            var filteredUnit = jQuery(doc.category).filter(function() {
                return (this.indexOf(":Unit") > -1 );
            });
            if(filteredUnit[0] && filteredUnit[0].length) {
                for (var i = 0; i < filteredUnit.length; i += 1){
                    var hero = filteredUnit[0].split("/");
                    if(hero){
                        entity.Unit = hero[hero.length -1].replace(/-/g, " "); //operate on the last element

                        //TODO: check to see if there is a number
                        var unitNo = entity.Unit.search(/\d+/);

                        //Check to see if a number exists
                        if( unitNo > -1 ){
                            //get the number
                            var unit = entity.Unit.match(/\d+/)[0];

                            //If its not longer than one
                            //Insert that value back into the string as the comparing string "UnitVal"
                            if(unit.length == 1){
                                var newUnit1 = "00" + unit;
                                entity.UnitVal = entity.Unit.replace(unit, newUnit1);
                            }else if(unit.length == 2){
                                var newUnit2 = "0" + unit;
                                entity.UnitVal = entity.Unit.replace(unit, newUnit2);
                            }
                            else{
                                entity.UnitVal = entity.Unit;
                            }

                            break;
                        }else{
                            //If there is no number just place in the unit for comparison as normal
                            entity.UnitVal = entity.Unit;
                            break;
                        }
                    }
                }
            }else{
                entity.Unit = "Unit not specified";
            }
        }

        function resolveQualFamily(doc, entity){
            // ---- QUALIFICATION FAMILY ---- //
            var filteredQualfam = jQuery(doc.category).filter(function() {
                return (this.indexOf(":Qualification-Family") > -1 );
            });
            if(filteredQualfam[0] && filteredQualfam[0].length) {
                for (var i = 0; i < filteredQualfam.length; i += 1){
                    var hero = filteredQualfam[0].split("/")[1]
                    if(hero){
                        entity.qualFam = hero.replace(/-/g, " ");
                        break;
                    }
                }
            }
        }

        function resolveLevel(doc, entity){
            // ---- LEVEL ---- //
            var filteredLevel = jQuery(doc.category).filter(function() {
                return (this.indexOf(":Level") > -1 );
            });
            if(filteredLevel[0] && filteredLevel[0].length) {
                for (var i = 0; i < filteredLevel.length; i += 1){
                    var hero = filteredLevel[0].split("/")[1]
                    if(hero){
                        entity.level = hero.replace(/-/g, " ");
                        var num = hero.match(/\d+/);

                        if(num && num.length && num.length > 0){
                            entity.sortLevel = num[0];
                        }else{
                            entity.sortLevel = 0;
                        }

                        break;
                    }
                }
            }
        }

        function resolvePrice(doc, entity) {
            // ---- PRICE ---- //
           if (doc.ukprice) {
                var list = doc.ukprice.match(new RegExp("-?\\d+", "g"));

                if (list != null) {
                    if (list.length < 3) {
                        list[2] = "0";
                        list[3] = "00";
                    }

                    var result = parseInt(list[0] + "." + list[1]) + parseInt(list[2] + "." + list[3]);

                    if (result == "0") {
                        entity.ukprice = "FREE";
                        entity.priceSort = "FREE";
                    } else {
                        entity.priceSort = result;
                    }
                } else {
                    if (doc.ukprice.toLowerCase() == "free" || doc.ukprice == "0") {
                        entity.ukprice =  "FREE";
                        entity.priceSort =  "FREE";
                    } else {
                        entity.priceSort = doc.ukprice;
                    }
                }
            }
        }

        function resolveSecure(doc, entity) {
            entity.secure = false;

            if(doc.id.indexOf("/content/dam/secure/") > -1) {
                if(doc.id.indexOf("gold") > -1) {
                    entity.padlock = "/etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/images/icons/icon_padlock_Gold.png";
                } else if (doc.id.indexOf("silver") > -1) {
                    entity.padlock = "/etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/images/icons/icon_padlock_Silver.png";
                }
                entity.url = entity.url + "?" + Math.floor(Math.random() * 100000000000000) + 1;
                entity.secure    = true;
            }
        }

        function resolvePageProperties(doc, entity) {
            var pageProperties = {};
            if(doc.pageProperties) {
                angular.forEach(doc.pageProperties, function(entry) {
                    if(entry.indexOf(':') > -1) {
                        var pageProp = entry.split(':');
                        if(pageProp.length > 1) {
                            pageProperties[pageProp[0]] = pageProp[1];
                        }
                    }
                });
            }

            //If the page properties do not have a title then use the doc title
            if(!pageProperties.modalTitle) {
                pageProperties.modalTitle = doc.title;
            }

            entity.properties = pageProperties;
        }

        function cleanData(doc) {
            var entity = doc;

            resolveRendition(doc, entity);
            resolveExamSeries(doc, entity);
            resolveDate(doc, entity);
            resolveDocumentType(doc, entity);
            resolveUnit(doc, entity);
            resolveQualFamily(doc, entity);
            resolveLevel(doc, entity);
            resolvePrice(doc, entity);
            resolveSecure(doc, entity);
            resolvePageProperties(doc, entity);

            return entity;
        }

        //Cleans the data so it is usable on the front end
        //Assign it to "this"
        return function(doc) {
            assignToThis(this, cleanData(doc));
        }
    }
]);

pearsonServices.factory("DateUtil", [ function() {
    return {
        resolveExamSeriesDate: function(date, day){
            //Get the date from the exam series weird format
            var day = (day || "01"),
                month = date.split(" ")[0],
                year = date.split(" ")[1];

            switch(month) {
                case "January"  : month = "01"; break;
                case "February" : month = "02"; break;
                case "March"    : month = "03"; break;
                case "April"    : month = "04"; break;
                case "May"      : month = "05"; break;
                case "June"     : month = "06"; break;
                case "July"     : month = "07"; break;
                case "August"   : month = "08"; break;
                case "September": month = "09"; break;
                case "October"  : month = "10"; break;
                case "November" : month = "11"; break;
                case "December" : month = "12"; break;
            }
            return year + "-" + month + "-" + day;
        }
    }
}]);

/*
    A google analytics wrapper

    The main aim of this class is to provide common simple way to send google analytics information as well as providing a simpler way to send extra "dimension" information
*/
pearsonServices.factory("googleAnalytics", [ function() {
    var send,hitType,eventCategory,eventAction,eventLabel;
    /* Dimensions key is a way to create a more readable index of the dimensions currently in our google analytics account.
    Within google they have more accurate descriptions, but in terms of indexing the data you have to use their default
    dimension name */
    var dimensionKey = {
        "qualfam": "dimension6",
        "qualsubj": "dimension7",
        "examseries": "dimension8",
        "accredfromdate":"dimension9",
        "doctype":"dimension10",
        "specificationstatus":"dimension11"
    };

    //Send the events object and filter through the valid and invalid arguments that have been passed in
    function sendGoogleAnalytics(argHitType, argEventCategory, argEventAction, argEventLabel, argDimensions) {
        var analyticsObj = { };

        analyticsObj['hitType']         = argHitType;
        analyticsObj['eventCategory']   = argEventCategory;
        analyticsObj['eventAction']     = argEventAction;
        analyticsObj['eventLabel']      = argEventLabel;

        //If the dimensions object has been passed
        if (argDimensions) {
            $.each(argDimensions, function(key, value) {
                //If the dimension key exists
                if(dimensionKey[key]) {
                    //Check that it is not undefined
                    if(argDimensions[key]) {
                        //Get the value from the dimensions passed
                        analyticsObj[dimensionKey[key]] = argDimensions[key];
                    }
                }
            });
        }

        //If all the basic attributes are present then send it
        if(analyticsObj['hitType'] && analyticsObj['eventCategory'] &&
            analyticsObj['eventAction'] && analyticsObj['eventLabel']) {
                console.log(analyticsObj);
                ga('send', analyticsObj);
        }
    }

    //Constructor
    return function() {
        //Getter setters for the diffCategories
        this.send = sendGoogleAnalytics;

        //Keys to use when creating dimension objects to pass in
        this.QF     = "qualfam";
        this.QS     = "qualsubj";
        this.ES     = "examseries";
        this.AFD    = "accredfromdate";
        this.DT     = "doctype";
        this.SS     = "specificationstatus";
    }
}]);

/* Service that allows you to send a google analytics event by extracting the relevant data from a solrDocument */
pearsonServices.service("analyticsService", [ "googleAnalytics", "Facet", "$filter",
    function(googleAnalytics, Facet, $filter) {

        //Create the query parameters from the SolrDocument
        this.sendEventFromDocument = function(item) {
            var eventCategory = ""; var eventAction = ""; var eventLabel = ""; var dimension = {}; var google = new googleAnalytics();

            //If there is category data - then get it
            if(item.category && item.category.length) {
                var qualFam         = new Facet($filter('getLooseMatch')(item.category, "Qualification-Family", true));
                var qualSubj        = new Facet($filter('getLooseMatch')(item.category, "Qualification-Subject", true));
                var accredFromDate  = new Facet($filter('getLooseMatch')(item.category, "Accreditation", true));
                var examSeries      = new Facet($filter('getLooseMatch')(item.category, "Exam-Series", true));
                var docType         = new Facet($filter('getLooseMatch')(item.category, "Document-Type", true));

                //Add the event categories
                eventCategory += (qualFam.facet)?             qualFam.formattedFacetValue        : "";
                eventCategory += (qualSubj.facet)?      " | " + qualSubj.formattedFacetValue       : "";
                eventCategory += (accredFromDate.facet)?" | " + accredFromDate.formattedFacetValue : "";
                eventCategory += (examSeries.facet)?    " | " + examSeries.formattedFacetValue     : "";
                eventCategory += (docType.facet)?       " | " + docType.formattedFacetValue        : "";
                if(item.eAction == "Download Published Resource"){
                    eventCategory = item.title;
                    //console.log(eventCategory);
                }
                
                eventAction = 'Download';
                eventLabel = item.url;
                //if(eventLabel == ""){console.log(eventLabel);}
                //console.log(eventLabel);

                //Add the dimensions
                dimension[google.QF] = qualFam.formattedFacetValue;
                dimension[google.QS] = qualSubj.formattedFacetValue;
                dimension[google.AFD]= accredFromDate.formattedFacetValue;
                dimension[google.ES] = examSeries.formattedFacetValue;
                dimension[google.DT] = docType.formattedFacetValue;
            } else {
                eventCategory = "Download on: " + location.href;
            }
            console.log(eventCategory+", "+eventAction+", "+eventLabel);
            //google.send('event', eventCategory, 'Download', "File Download: " + item.title + " path: " + item.url, dimension);
            google.send('event', eventCategory, eventAction , eventLabel, dimension);
        };
}]);