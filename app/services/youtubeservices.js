'use strict';

/* Services */
/*
    You can use the $rootScope to interact with the scope of the
    application and modify variables that are relevant to the page
*/
var youtubeServices = angular.module('youtubeServices', ['ngResource', 'LocalStorageModule', 'pearsonServices']);

//Factories return the result of the function within
youtubeServices.factory("Video", [ "$filter", "QuerySolr", function($filter, QuerySolr){
    //Creating a video object that can be used to generate youtube videos and meta
    function getQuery(url){
        var vars    = {};
        var hashes  = url.slice(url.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++){
            vars[hashes[i].split('=')[0]] = hashes[i].split('=')[1];
        }
        return vars;
    }

    function secondsToMinutes(time){
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;

        return minutes + ":" + seconds;
    }

    function getId(url){
        if(typeof getQuery(url)["v"] == "undefined"){
            return url;
        }
        return getQuery(url)["v"]
    }

    return function(vidObject){
        //this.url        = url;
//    	var vidObj      = vidObject;
    	this.url        = vidObject.url;
        this.id         = getId(vidObject.url);
        this.thumbnail  = "";
        this.title      = "";
        this.duration   = "";
        this.caption	= vidObject.videoCaption;

        //Function call to get the rest of the metadata necessary for the object
        (function(that){
            QuerySolr.configQuery("http://gdata.youtube.com/feeds/api/videos/" + that.id + "?v=2&alt=json");
            QuerySolr.query({}, function(data){
                var meta        = data.entry.media$group;
                that.thumbnail   = meta.media$thumbnail[0].url;
                that.title       = meta.media$title.$t;
                that.duration    = meta.yt$duration;
                if(that.duration){
                    that.duration.milliseconds = that.duration.seconds * 1000;
                }
            })
        }(this))
    }
}]);

//Services create an instance of the object within as a singleton
youtubeServices.factory('VideoList', [ "Video", function(Video){
    var ListService = function(){};
    ListService.prototype = Array.prototype;
    ListService.prototype.createFromList = function(vidList) {
        var that = this;
        angular.forEach(vidList, function(entry){
            var vid = new Video(entry);
            that.push(vid);
        });
    }

    var listService = new ListService();

    return listService;
}]);