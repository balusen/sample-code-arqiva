'use strict';

/* Services */
/*
 You can use the $rootScope to interact with the scope of the
 application and modify variables that are relevant to the page
 */
var pearsonSearchServices = angular.module('pearsonSearchServices', ['ngResource']);

pearsonSearchServices.service('SearchResults', ['$resource', 'Query', '$rootScope',
    function ($resource, Query, $rootScope) {

        return {
            getDocuments: function (queryString, func) {
                var query = Query.createQuery("/services/pearson/search/GET.servlet");
                var queryFunc = query.get({queryString: queryString}, function (data) {
                    if (func){
                        func(data)
                    }
                });
            }
        }
    }
]);