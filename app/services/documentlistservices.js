'use strict';

/* Services */
/*
 You can use the $rootScope to interact with the scope of the
 application and modify variables that are relevant to the page
 */
var pearsonDocumentListServices = angular.module('pearsonDocumentListServices', ['ngResource']);

pearsonDocumentListServices.service('DocumentListDocuments', ['$resource', 'Query', '$rootScope',
    function ($resource, Query, $rootScope) {

        return {
            getDocuments: function (tags, func) {
                var query = Query.createQuery("/services/pearson/solr/GET.servlet");
                var queryFunc = query.get({fq: tags}, function (data) {
                    if (func){
                        func(data)
                    }
                });
            },
            getDocumentsById: function (documents, func) {
                var query = Query.createQuery("/services/pearson/solr/GET.servlet");
                var queryFunc = query.get({q: documents}, function (data) {
                    if (func){
                        func(data)
                    }
                });
            }
        }
    }
]);