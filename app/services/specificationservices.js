'use strict';

/* Services */
/*
    You can use the $rootScope to interact with the scope of the
    application and modify variables that are relevant to the page
*/
var pearsonSpecificationServices = angular.module('pearsonSpecificationServices', ['ngResource','pearsonServices']);

pearsonSpecificationServices.service('RelatedQualifications',['$resource', 'Query', function($resource, Query) {
	//Function is used as a shorthand to create inheritance between two objects
	function inheritsFrom( obj, parentClassOrObject ) {
    	if ( parentClassOrObject.constructor == Function ) {
    		//Normal Inheritance
    		obj.prototype = new parentClassOrObject;
    		obj.prototype.parent = parentClassOrObject.prototype;
    	} else {
    		//Pure Virtual Inheritance
    		obj.prototype = parentClassOrObject;
    		obj.prototype.parent = parentClassOrObject;
    	}

		obj.prototype.constructor = obj;

    	return obj;
    }

	// --------------------------------------------------------------------------------------//
	// ----------------------------------- List container object ----------------------------//
	// --------------------------------------------------------------------------------------//
	// This list container has methods that allow you to intelligently add elements to your list
	function ListContainer() {
		this.elements = [];
	}
	// Get an item from the list based on the elements
	ListContainer.prototype.getElementById = function(id) {
		for(var i = 0; i < this.elements.length; i++) {
			if(this.elements[i].id == id) {
				return this.elements[i];
			}
		}
	}
	// Check to see if an element is in the list based on the ID
	ListContainer.prototype.hasElementById = function(id) {
		//If it is empty then there is nothing
		return (this.getElementById(id)) ? true : false;
	}
	// Add an element to the list, it the element already exists in the list, based on the ID, do not add it,
	// instead return the element that we already have to the user
	ListContainer.prototype.addElement = function(item) {
		//if the element already exists, return it
		if(this.hasElementById(item.id)) {
			return this.getElementById(item.id);
		}
		//otherwise then add the item
		this.elements.push(item);
		return item;
	}

	// --------------------------------------------------------------------------------------//
    // ------------------------------------ Specification object ----------------------------//
    // --------------------------------------------------------------------------------------//
    //This object will pull the relevant metadata from the solr doc that can be used
    function Specification(item) {
		this.url = item.url;				this.title = item.title;
		this.id = item.url;					this.qualifSubject = item.title;
		this.accreditationFrom = "1970";	this.qfTitle = "";
		this.modalTitle = "";				this.modalDescription = "";

		//Check if the item has page properties
		if(item.properties) {
			//Set the modal title if there is one
			if(item.properties.modalTitle) {
				this.modalTitle = item.properties.modalTitle;
			}
			//If there is a modal description then use it
			if(item.properties.modalDescription) {
				this.modalDescription = item.properties.modalDescription;
			}
			
			
		}

		//Get the tag values - AFD
		var accreditationFrom = Pearson.wes.common.getCategoryValues("Pearson-UK:Accreditation-From-date/", item.category);
		if (accreditationFrom.length > 0) {
		   this.accreditationFrom = accreditationFrom[0];
		}
		//Get the tag values - QS
		var qualifSubject = Pearson.wes.common.getCategoryValues("Pearson-UK:Qualification-Subject/", item.category);
		if (qualifSubject.length > 0) {
		  	this.qualifSubject = qualifSubject[0];
		}
		//Get the tag values - QF
		var qfTitle = Pearson.wes.common.getCategoryValues("Pearson-UK:Qualification-Family/", item.category);
		if (qfTitle.length > 0) {
		   	this.qfTitle = qfTitle[0].replace('-',' ');
		}
    }

	// --------------------------------------------------------------------------------------//
	// ------------------------------------ Qualification object ----------------------------//
	// --------------------------------------------------------------------------------------//
	function Qualification(item, groupByFacet) {
		//Get ID and name from the facet
		this.id 		= item.replace(groupByFacet,'');
		this.title 		= this.id.replace('-',' ');
		this.subjects 	= this.elements 			= [];
	}
	//This will get the propertied from the spec container as well as functions into the Qualification object
	inheritsFrom(Qualification, ListContainer);

	// --------------------------------------------------------------------------------------//
	// ------------------------------------ Subject class -----------------------------------//
	// --------------------------------------------------------------------------------------//
	// Create subject - this will contain other specs
	function Subject(item) {
		 this.title 		= item.title;
		 this.qfSubject 	= item.qualifSubject;
		 this.qfValue 		= item.qfTitle;
		 this.qfTitle 		= item.qfTitle;
		 this.modal 		= false;
		 this.id 			= this.qfValue + ":" + this.qfSubject;
		 this.specificationsList 	= this.elements		= [];
	}
	inheritsFrom(Subject, ListContainer);
	// Override the base class "addElement" function so once there are more than one elements in the list,
	// it sets the modal to true and sets the correct title for the subject now that it points to multiple values
	Subject.prototype.addElement = function(item) {
		//Call the superType method which returns an element
		var element = this.parent.addElement.call(this, item);

		//If there is more than one spec this is now a modal, the title should now be the qfSubject
		if(this.elements.length > 1) {
			this.modal = true;
			this.title = this.qfSubject;
			//console.log('Modal title is --- ',this.modalTitle,this.elements);
		}

		return element;
	}

    return {
		getSOLRSearchResult: function(subjectTags) {
			var params = [
				"category:Pearson-UK:Page-Type/specification",
				"category:Pearson-UK:Page-Type/pathwayspecification"
			];
    		
    		var subjectTagObjects = subjectTags.split(",");
    		$.each(subjectTagObjects, function(i, item) {
    			params.push("category:"+item);
    		});
    		
            var query = Query.createQuery("/services/pearson/search/GET.servlet");
            return query.get({filterQuery: params, doSingleSearch: true, searchSubjectFamily: true}).$promise.then(function(data){
                 return data;
            });
        },

        // Get spec pages grouped by "groupByFacet" ignoring the "currentPage" and placing the spec page
        // in more than one group depending on "specInOneGroupOnly"
        getSpecificationsGroupedByFacet: function(solrDocuments, currentPage, groupByFacet, specInOneGroupOnly) {
         	   	var qualifications = new ListContainer();

         	   	//Iterate over all the docs, creating the object hierarchy as we go
         	   	$.each(solrDocuments, function(i, item) {
         		   	// Skip current page document or if the page in question is archived
         		   	if (item.id==currentPage || Pearson.wes.common.isArchived(item)) { return true; }

         		   	//Create a new specification item
         		   	var specification = new Specification(item);

					//Iterate over the tags - until you find a GroupBy facet
					$.each(item.category, function(j, category_item) {
						 //Found the matching category
						if (category_item.indexOf(groupByFacet) > -1) {
							//Create the qualification object
							var qual 	= new Qualification(category_item, groupByFacet);

							//Create a new subject object
							var subject = new Subject(specification);

							//add qual to qualifications, if the qual exists it returns the current qual in the list
							qual = qualifications.addElement(qual);

							//add subject to qual, if the subject exists it returns the current qual in the list
							subject = qual.addElement(subject);

							// ""
							specification = subject.addElement(specification);

							// break - no need to iterate over other category items
							if (specInOneGroupOnly) {
								return false;
							}
						}
					});
         		});

         	   	//return qualifications;
         	   	return qualifications;
           	}
    	}
 	}
 ]);