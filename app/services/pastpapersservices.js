'use strict';

/* Services */
/*
 You can use the $rootScope to interact with the scope of the
 application and modify variables that are relevant to the page
 */
var pearsonPastPapersServices = angular.module('pearsonPastPapersServices', ['ngResource', 'pearsonServices']);

pearsonPastPapersServices.service('QualificationFamily', ['$resource', 'Query', '$rootScope',
    function ($resource, Query, $rootScope) {

        return {
            getFamilies: function (tags) {
                var query = Query.createQuery("/services/pearson/search/GET.servlet");
                var queryFunc = query.get({filterQuery: tags, getQFFacets: true}, function (families) {
                    $rootScope.families = families;
                });
            }
        }
    }
]);

pearsonPastPapersServices.service('Subjects', ['$resource', 'Query', '$rootScope', '$filter',
    function ($resource, Query, $rootScope, $filter) {

        return {
            getSubjectsForQualification: function (tags) {
                var query = Query.createQuery("/services/pearson/search/GET.servlet");
                return query.get({filterQuery: tags, getSubjectFacets: true}).$promise.then(function (data) {
                    //$rootScope.subjects = data;
                    return data;
                });
            },

            getSubjectsGroupedByFirstLetter: function (subjects, facet) {

                var groupedSubjects = [];

                // initialise the A-Z group array
                for (var i = 65; i <= 90; i++) {

                    groupedSubjects.push({groupName: String.fromCharCode(i), subjectItems: []});

                }
                // push subject into a proper group
                angular.forEach(subjects, function (value, index) {
                    //only add a subject if the facet
                    var qualificationSubject = $filter('getQualificationSubjectTag')(value);
                    var subject = Pearson.wes.common.getFacetValue(qualificationSubject);

                    if (facet != undefined) {
                        if ($.inArray(facet, value.category) > -1) {
                            if (groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems.length > 0) {
                                var added = false;
                                for (var i = 0; i < groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems.length; i++) {
                                    if (groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems[i].subjectName === subject) {

                                        var tmplength = groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems[i].subjectsList.length;
                                        groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems[i].subjectsList[tmplength] = value;
                                        groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems[i].modal = true;
                                        added = true;
                                        break;

                                    }
                                }
                                if (!added) {
                                    groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems.push({subjectName: subject, modal: false, subjectsList: [value]});
                                }
                            } else {
                                groupedSubjects[subject.toUpperCase().charCodeAt(0) - 65].subjectItems.push({subjectName: subject, modal: false, subjectsList: [value]});
                            }


                        }

                    }
                    //else{
                    //groupedSubjects[subject.charCodeAt(0)-65].subjectItems.push(value);
                    //}
                })

                return groupedSubjects;

            }
        }
    }
]);

pearsonPastPapersServices.service('ExamSeries', ['$resource', 'Query', '$rootScope',
    function ($resource, Query, $rootScope) {

        $rootScope.showExamSeriesResults = true;

        return {
            getExamSeries: function (tags) {
                var query = Query.createQuery("/services/pearson/search/GET.servlet");
                var queryFunc = query.get({filterQuery: tags, getExamSeriesFacets: true}, function (examSeries) {
                    $rootScope.examSeries = examSeries;
                    $rootScope.showExamSeriesResults = ($rootScope.examSeries != undefined
                            && $rootScope.examSeries.searchResults.length > 0) ? true : false;
                });
            }
        }
    }
]);

pearsonPastPapersServices.service('PastPapersDocuments', ['$resource', 'Query', '$rootScope', 'resolveShowMore',
    function ($resource, Query, $rootScope, resolveShowMore) {

        return {
            getPastPapers: function (tags, func) {
                var query = Query.createQuery("/services/pearson/search/GET.servlet");
                var queryFunc = query.get({filterQuery: tags, doSingleSearch: true}, function (data) {
                    $rootScope.data = data;
                    $rootScope.STATE.resultsLength = data.searchResults.solrDocuments.length;
                    resolveShowMore();

                    if(func){
                        func(data);
                    }
                });
            }
        }
    }
]);


pearsonPastPapersServices.factory("pastPapersEvents", [ "$rootScope", "resolveShowMore", function($rootScope, resolveShowMore) {
    return {

        pageSetup: function (displayLimit) {
            $rootScope.STATE = ($rootScope.STATE || {});
            $rootScope.STATE.displayLimit = displayLimit;
            $rootScope.STATE.showmore = false;
        },
        showMore: function (displayLimit) {
            var inc = displayLimit;
            var limit = $rootScope.STATE.displayLimit;
            var resultLength = $rootScope.data.searchResults.solrDocuments.length;
            $rootScope.STATE.displayLimit = limit + inc;
            resolveShowMore();
        }
    }
}]);
