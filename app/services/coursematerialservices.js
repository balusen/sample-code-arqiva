'use strict';

/* Services
    You can use the $rootScope to interact with the scope of the
    application and modify variables that are relevant to the page
*/
var pearsonCourseMaterialServices = angular.module('pearsonCourseMaterialServices', ['ngResource', 'pearsonServices']);


pearsonCourseMaterialServices.factory('Documents',['$resource', 'Query','$rootScope', 'resolveShowMore', '$filter',  
     function($resource, Query, $rootScope, resolveShowMore, $filter){
        return {
             getDocuments: function(params, callback){
                 var query = Query.createQuery("/services/pearson/search/GET.servlet");
                 var queryFunc = query.get({filterQuery: params}, function(data){
                     //console.log(data);

                     if(callback){
						callback(data);
                     }
                 });
            }
        }
     }
 ]);

pearsonCourseMaterialServices.factory("FacetQuery", [ "$rootScope", "Query", function($rootScope, Query) {
	return {
    	getCategories: function(params, callback){
    		var query = Query.createQuery("/services/pearson/search/GET.servlet");
            var queryFunc = query.get({filterQuery: params, resourceTypeQuery: true}, function(data){
                //console.log(data);
                $rootScope.categoryFacets = data.resourceTypeFacetResults;
                if(callback){
                	callback(data);
                }
            });
    	}
    }
}]);

pearsonCourseMaterialServices.service("serializeForm", [ function() {
    return function(formId, option){
            var form = document.getElementById(formId) || document.forms[0];
            var elems = form.elements;
            var serialized = [], i, len = elems.length, str='';
            for (i = 0; i < len; i += 1) {
                var element = elems[i];
                switch(element.type) {
                    case 'text':
                    case 'radio':
                    case 'checkbox':
                    case 'textarea':
                    case 'select-one':
                        if (option == "primary" && jQuery(element).is(":visible")){
                            break;
                        } else if (option == "secondary" && !jQuery(element).is(":visible")){
                            break;
                        } else if (option == "courseMatFilters" && element.id == ""){
                            break;
                        }
                        
                        if (element.checked || element.outerHTML.indexOf("CHECKED") > -1) {
                            str = element.value;
                            if(str != "on" && str) {
                            	serialized.push(str);
                            }
                        }

                        break;
                    default:
                        break;
                }
            }
            return serialized;
        }
}]);

pearsonCourseMaterialServices.factory("events", [ "$rootScope", "resolveShowMore", "serializeForm", "FacetQuery", "Documents",
    function($rootScope, resolveShowMore, serializeForm, FacetQuery, Documents) {
    return {
        pageSetup: function(){
        	//Set the state that is going to be used in the other services
            $rootScope.STATE = ($rootScope.STATE || {});
            $rootScope.STATE.displayLimit = 20;
            $rootScope.STATE.showmore = false;
        },showMore: function(){
            var inc = 20;
            var limit = $rootScope.STATE.displayLimit;
            var resultLength = $rootScope.documents.length;

      		$rootScope.STATE.displayLimit = limit + inc;

            resolveShowMore();
        }
    }
}]);

pearsonCourseMaterialServices.service('resolveState', [ function(){
     return function($scope) {
        if ($scope.STATE.resultsLength > $scope.STATE.displayLimit) {
            $scope.STATE.showmore = true;
        } else {
            $scope.STATE.showmore = false;
            $scope.STATE.displayLimit = $scope.STATE.resultsLength;
        }

        return $scope;
    }
}]);