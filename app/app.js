'use strict';

/* App Module */
//AKA Course materials
var pearsonCourseMaterialsApp = angular.module('pearsonCourseMaterialsApp', ['pearsonCourseMaterialsController', 'pearsonCourseMaterialServices',
                                                           'pearsonServices', 'pearsonFilters','pearsonCourseMaterialFilters',
                                                           'pearsonCourseMaterialDirectives', 'CourseMaterialsConstants', 'progressiveDisclosureDirectives']);
var pearsonPastPapersApp = angular.module('pearsonPastPapersApp', ['pearsonPastPapersController', 'pearsonServices',
                                                                   'pearsonPastPapersServices', 'pearsonFilters',
                                                                   'pearsonPastPapersFilters', 'pearsonPastPapersDirectives', 'pearsonCourseMaterialFilters']);
var pearsonSpecificationApp = angular.module('pearsonSpecificationApp', ['pearsonSpecificationController', 'pearsonCourseMaterialsController',
                                                                         'pearsonServices', 'pearsonCourseMaterialServices',
                                                                         'pearsonSpecificationServices', 'pearsonFilters',
                                                                         'specificationDirectives', 'pearsonCourseMaterialDirectives',
                                                                         'pearsonCourseMaterialFilters', 'pearsonPastPapersFilters']);
var pearsonGradePapersApp = angular.module('pearsonGradePapersApp', ['pearsonGradePapersController','pearsonServices',
                                                                     'pearsonPastPapersServices', 'pearsonFilters',
                                                                     'pearsonPastPapersFilters', 'pearsonPastPapersDirectives', 'pearsonCourseMaterialFilters']);
var pearsonRecentGradeBoundairesApp = angular.module('pearsonRecentGradeBoundairesApp', ['pearsonRecentGradeBoundairesController','pearsonServices',
                                                                           'pearsonCourseMaterialFilters', 'pearsonPastPapersFilters']);

var pearsonProgressiveDisclosureApp = angular.module('pearsonProgressiveDisclosureApp', ['progressiveDisclosureControllers', 'pearsonFilters',
                                                                                        'progressiveDisclosureDirectives',
                                                                                        'progressiveDisclosureFilters', 'pearsonCourseMaterialFilters']);

var pearsonDocumentListApp = angular.module('pearsonDocumentListApp', ['pearsonDocumentListController', 'pearsonPastPapersFilters']);

var pearsonKeyDatesApp = angular.module('pearsonKeyDatesApp', ['pearsonKeyDatesController','pearsonServices','pearsonFilters',
                                                               	'pearsonCourseMaterialFilters', 'pearsonCourseMaterialServices',
                                                               	'progressiveDisclosureDirectives', 'progressiveDisclosureFilters']);
var pearsonNextKeyDatesApp = angular.module('pearsonNextKeyDatesApp', ['pearsonNextKeyDatesController','pearsonServices','pearsonFilters',
                                                                        'pearsonCourseMaterialFilters', 'pearsonCourseMaterialServices',
                                                                        'progressiveDisclosureDirectives', 'progressiveDisclosureFilters']);

var pearsonQualificationApp = angular.module('pearsonQualificationApp', ['pearsonQualificationController', 'pearsonQualificationFilters',
                                                                         'pearsonQualificationDirectives', 'pearsonServices', 'progressiveDisclosureDirectives' ]);

var pearsonNewsListApp = angular.module('pearsonNewsListApp', ['pearsonNewsListController','pearsonNewsListDirectives','pearsonNewsListFilters']);

var pearsonNewsComboApp = angular.module('pearsonNewsComboApp', ['pearsonNewsComboController','pearsonNewsListFilters','pearsonNewsComboFilters']);

var pearsonSearchApp = angular.module('pearsonSearchApp', ['searchController', 'pearsonFilters', 'pearsonServices', 'searchFilters']);

var pearsonSupportTopicSiblingsApp = angular.module('pearsonSupportTopicSiblingsApp', ['pearsonServices', 'pearsonSupportTopicSiblingsController']);

var pearsonSeeAlsoApp = angular.module('pearsonSeeAlsoApp', ['pearsonServices', 'pearsonCourseMaterialServices', 'pearsonSeeAlsoController']);

//location config for all the apps
angular.forEach([pearsonProgressiveDisclosureApp,
    pearsonSpecificationApp,
    pearsonSearchApp,
    pearsonNewsListApp,
    pearsonRecentGradeBoundairesApp,
    pearsonDocumentListApp,
    pearsonKeyDatesApp,
    pearsonNextKeyDatesApp,
    pearsonQualificationApp,
    pearsonNewsComboApp,
    pearsonCourseMaterialsApp,
    pearsonSupportTopicSiblingsApp,
    pearsonSeeAlsoApp], function(module){
    module.config([ "$locationProvider", function($locationProvider){
            $locationProvider.hashPrefix('');
            $locationProvider.html5Mode((window.history.replaceState) ? true : false);
        }]);
});

//pearsonCourseMaterialsApp.config([ "$locationProvider", "$provide", "CONST", function($locationProvider, $provide, CONST){
//	$locationProvider.hashPrefix('').html5Mode(true);
//}]);

pearsonPastPapersApp.config(["$locationProvider", "$provide", function($locationProvider, $provide) {
    $provide.decorator('$q', ['$delegate', '$rootScope', function($delegate, $rootScope) {

    $locationProvider.html5Mode((window.history.replaceState) ? true : false);

      var pendingPromisses = 0;
      $rootScope.$watch(
        function() { return pendingPromisses > 0; },
        function(loading) { $rootScope.loading = loading; }
      );
      var $q = $delegate;
      var origDefer = $q.defer;
      $q.defer = function() {
        var defer = origDefer();
        pendingPromisses++;
        defer.promise['finally'](function(){
          pendingPromisses--;
        });
        return defer;
      };
      return $q;
    }]);
}]);

pearsonGradePapersApp.config([ "$locationProvider", "$provide", function($locationProvider, $provide) {
    $provide.decorator('$q', ['$delegate', '$rootScope', function($delegate, $rootScope) {
        $locationProvider.html5Mode((window.history.replaceState) ? true : false);
        var pendingPromisses = 0;
        $rootScope.$watch(
            function() { return pendingPromisses > 0; },
            function(loading) { $rootScope.loading = loading; }
        );
        var $q = $delegate;
        var origDefer = $q.defer;
        $q.defer = function() {
            var defer = origDefer();
            pendingPromisses++;
            defer.promise['finally'](function() {
                pendingPromisses--;
            });
            return defer;
        };
        return $q;
    }]);
}]);