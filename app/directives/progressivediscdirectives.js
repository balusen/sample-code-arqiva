var progressiveDisclosureDirectives = angular.module('progressiveDisclosureDirectives', ['pearsonServices']);

//progressiveDisclosureDirectives.directive('trainingCourseEntry', [function(){
//    return {
//        scope:{ doc: '=' },
//        template:
//            '<div class="c1"><h3>Gb</h3></div>' +
//            '<div class="text-info c8">' +
//            '<h4>{{doc.title}} <span class="file-info">({{doc.size}} | <span class="file-type">{{doc.extension}}</span>)</span></h4>' +
//            '<div class="description">{{doc.description}}</div>' +
//            '</div>' +
//            '<span class="btn c3">Save to briefcase</span>'
//    }
//}]);

//This is a drop down directive that allows to call the changeDropdown
//function whenever the order property has changed
progressiveDisclosureDirectives.directive("trainingCourseOption", [ "$filter", function($filter) {
    return function(scope, element, attr) {
        var dropDown = jQuery(element).parent();

        //Change the value of the dropdown so long as the value when the "orderProp" value
        //is different from the value of the dropdown
        scope.$watch(attr.watch, function(newval, oldval) {
            if(newval == attr.value && dropDown.val() !== newval) {
                dropDown.val(attr.value);

                if(dropDown.is(":visible")) {
                    dropDown.change();
                }
            }
        });

        (dropDown).bind("change", function(event) {
            if($(this).val() == attr.value) {
                scope.changeDropDown(attr.value, attr.type, $(this));
            }

            if(!scope.$$phase) {
                scope.$apply();
            }
        });
    }
}]);

progressiveDisclosureDirectives.directive("wesBindHtml", [function(){
    return function(scope, element, attr){
        var text = attr.html;
        jQuery(element).html(text);
    }
}]);

progressiveDisclosureDirectives.directive("resetLimits", [ "$filter", function($filter){
    return {
        transclude: true,
        link: function(scope, element, attr){
            scope.$watch("resultsFilter", function(newval, oldval){
                if(scope.resetLimit){
                    scope.resetLimit(scope.filteredDocuments);
                }else{
                    scope.STATE.resultsLength = scope.filteredDocuments.length;
                    if(scope.STATE.resultsLength > scope.STATE.displayLimit){
                        scope.STATE.showmore = true;
                    }else{
                        scope.STATE.showmore = false;
                        scope.STATE.displayLimit = scope.STATE.resultsLength;
                    }
                }
            });
        }
    }
}]);


//This modal is to be used a configurable piece of the training courses page
progressiveDisclosureDirectives.directive("trainingModal", [ "$rootScope", "$filter", function($rootScope, $filter){
    return {
        scope:{
            doc: '=doc'
        },
        templateUrl: "/etc/designs/ped/pei/uk/pearson-uk-new/angularJS/app/partials/progdisc/training/modal-training-course.html",
        link: function(scope, element, attr){

            var sixDays = 518400000;
            //Define the display of the modal based on whether there is the "Open" event status
            scope.open = $filter('hasLooseMatch')(scope.doc.category, 'EventStatus/Open');

            //Calculate the difference between todays date and the date of the event
            var dateDiff = new Date(scope.doc.fromdate).getTime() - new Date().getTime();

            //If the event is in 6 or more days time you can use i don't have a centre number part
            if(sixDays <= dateDiff){
                scope.showCentreNo = true;
            }else{
                scope.showCentreNo = false;
            }

            //Get the coursecode and to send the services to the Edexcel online site
            var eventId = $filter('getLooseMatch')(scope.doc.category, 'Pearson-UK:EventId/', true);
            eventId = eventId.split("/")[1];
            scope.eventId = eventId;

            var eventCode = $filter('getLooseMatch')(scope.doc.category, 'Pearson-UK:EventCode/', true);
            var eventCode = eventCode.substr(eventCode.indexOf('/')+1);
            scope.eventCode = eventCode;

            var eventLocation = $filter('getLooseMatch')(scope.doc.category, 'Pearson-UK:Location/', true);
            var eventLocation = eventLocation.substr(eventLocation.indexOf('/')+1);
            scope.eventLocation = eventLocation;

            var eventTitle = scope.doc.title;
            scope.eventTitle = eventTitle;

            scope.eventDate = scope.doc.fromdate;

            scope.edexcelLink = "/services/pearson/login.services?forwardResource=https://www.edexcelonline.com/TrainingBookings/ViewEventDetails.aspx?Function=2&CourseEventId="
                + eventId;

            scope.edexcelLinkDirect = "https://www.edexcelonline.com/TrainingBookings/ViewEventDetails.aspx?Function=2&CourseEventId="
                + eventId;

            scope.openForm = function() {
                //show the form
                $('.modal-cover .modal .form-content').removeClass("hidden");
                $(window).trigger("resize");
            }

            scope.sendEmail = function (event, eventId) {

                event.preventDefault();

                $.ajax({
                    type: 'POST',
                    url: '/services/pearson/email/training.service',
                    data: $("." + eventId).serializeArray(),
                    success: function(msg) {
                        if (msg === 'success') {
                            $(".modal-cover .modal #booking-form").html("<h1>Thank you</h1> Thank you for your booking request, which we'll aim to confirm within 72 hours.<br><br>" +

                            "If you do not hear back from us within this time, please contact us directly on <a href=\"mailto:trainingbookings@pearson.com\">trainingbookings@pearson.com</a><br><br>" +

                            "Please note this is a booking request only and does not guarantee a place on an event.");
                        } else {
                            $(".modal-cover .modal #booking-form").html("Sorry, our system is currently unavailable. Please try again later.");
                        }
                        $(window).trigger('resize');
                    },
                    error: function(xhr, text, error) {
                        $(".modal-cover .modal #booking-form").html("Sorry, our system is currently unavailable. Please try again later.");
                        $(window).trigger('resize');
                    }
                });
            };
        }
    }
}]);

progressiveDisclosureDirectives.directive("listDisplay", [ "pageConfig", function(pageConfig){
    return {
        transclude : true,
        templateUrl: pageConfig.listDisplayUrl,
        link: function(scope, element, attr){ }
    }
}]);

progressiveDisclosureDirectives.directive("sortDisplay", [ "pageConfig", function(pageConfig){
    return {
        transclude : true,
        templateUrl: pageConfig.sortDisplayUrl,
        link: function(scope, element, attr){ }
    }
}]);