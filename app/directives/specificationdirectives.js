var specificationDirectives = angular.module('specificationDirectives', []);

specificationDirectives.directive("setHideApp", [ "$filter", function($filter){
	return function(scope, element, attr){
//			console.log("I am in this directicve");
			scope.$watch("results", function(newval, oldval){
				if(newval.length < 1){
					scope.setHide(true);
				}else {
					scope.setHide(false);
				}
			});
		}
}]);

specificationDirectives.directive("relatedSortOption", [ function(){
    return {
        transclude: true,
        scope: { sortClick:'=', mode: "=" },
        replace: true,
        template: '<a ng-transclude></a>',
        link: function(scope, elemenet, attr){
            var type    = attr.type;
            var parent  = (elemenet).closest("li");

            scope.$watch("mode", function(newval, oldval){
                if(type == newval){
                    (parent).addClass("current");
                }else{
                    (parent).removeClass("current");
                }
            });

            (parent).on("click", function(){
                scope.sortClick(type);
            });
        }
    };
}]);

specificationDirectives.directive('modalLinks', [function () {
    return {
        transclude: true,
        scope:{
            "spec":"=spec"
        },
        template: "{{spec.title | cleanTitle }} ({{spec.qfTitle | titleCaseFilter}})",
        link: function (scope, element, attr) {
        element.attr("href", "#" + scope.spec.title.replace(" ", "-"));
    }
    }
}]);

specificationDirectives.directive('qualificationLinks', [function () {
    return {
        transclude: true,
        scope:{
            "spec":"=spec"
        },
        template: "{{spec.title | cleanTitle }}",
        link: function (scope, element, attr) {
        element.attr("href", "#" + scope.spec.title.replace(" ", "-"));
    }
    }
}]);