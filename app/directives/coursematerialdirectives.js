'use strict';

/* Directives */
var pearsonCourseMaterialDirectives = angular.module('pearsonCourseMaterialDirectives', ['pearsonCourseMaterialFilters', 'pearsonServices']);

pearsonCourseMaterialDirectives.directive('filterFacet', [ "serializeForm", "$location", "appScope",
    function(serializeForm, $location, $appScope){
    return {
        transclude: true,
        scope: {
            facet:  '=facet',
            group:  '=group',
            type:   '=type'
        },
        template:   '<input type="radio" class="filter" name="filterQuery"' +
                    'value="category:{{facet.facet}}" id="{{facet.facet}}">' +

                    '<input type="checkbox" class="filter" name="filterQuery"' +
                    'value="category:{{facet.facet}}" id="{{facet.facet}}">' +

                    '<label>' +
                    '{{ facet.formattedFacetValue | cleanUnit }}' +
                    ' ({{facet.count}})</label>',
        link: function(scope, element, attr){
            //Fix is for IE8, since we are not able to programatically change the type of the input box
            //we include both of them and then based on the scope.type we remove the non-applicable element
            $(element).find(".filter[type!='" + scope.type + "']").remove();

            //Init variables
            var facet       = scope.facet;
            var appScope    = $appScope.getScope();
            scope.radio     = scope.type == "radio";
            var parent      = jQuery(element).parent(),
                parentLi    = jQuery(element).closest("li"),
                input       = jQuery(element).find("input"),
                label       = jQuery(element).find("label");

            scope.$watch("facet",  function (newval, oldval){
               if(newval != undefined){
            	   var hash;
            	   	if ($location.hash() != "") {
            	   		hash = $location.hash();
            	   	} else {
            	   		hash = window.location.hash.replace("#/","");
            	   	}

            	   	var cleanFacet = facet.facet.replace(/\(/g, '\\(').replace(/\)/g, "\\)");

                    //To use variables you need to create a regex object
                    var endLine = new RegExp(cleanFacet + "$", "g"); //Check if the "whole" facet is at the end of the line
                    var preAnd  = new RegExp(cleanFacet + "&", "g"); //Check if the "whole" facet is in between variables

                    //Check to see if it has the whole value at the end of the string or before the "&"
                    if(hash.match(endLine) || hash.match(preAnd)){
                        (input).prop("checked", true);

                        if(scope.radio && newval) {
                            appScope.setSort(facet.facet);
                            (parentLi).siblings().removeClass("current");
                            (parentLi).addClass("current");
                        }
                    }

                    if(scope.facet.count < 1) {
                        (parent).addClass("option-disabled");
                        (input).prop("disabled", true);
                    } else {
                        (parent).removeClass("option-disabled");
                        (input).prop("disabled", false);
                    }
                }
            });

            //Bind the click to the children
            (element).children().bind('click', function(event) {
                if(event.target.nodeName == "LABEL") {
                  jQuery(input).prop("checked", !jQuery(input).prop("checked"));
                }

                //Set the facet object to be checked based on the input boxes status
                scope.facet.checked = jQuery(input).prop("checked");

                //Set the sort option for the component
                if(scope.radio) {
                    appScope.setSort(facet.facet);
                }

                //Use the facet click function to initiate a change in the url which will update the component
                appScope.facetClick(scope.facet);
            });
        }
    }
}]);

pearsonCourseMaterialDirectives.directive('sortButton', [ function(){
    return function(scope, element, attr) {
            scope.$watch("orderProp",  function (newval, oldval){
                if (newval == attr.option)  {
                    element.addClass("current");
                } else {
                    element.removeClass("current");
                }
            });

            element.bind("click", function(event) {
               event.preventDefault();
               //Set the model to do many things
               scope.sortFunction(attr.option);
               //scope.orderProp = ;
               scope.$apply();
            });
        }
}]);

pearsonCourseMaterialDirectives.directive('allFacet', [ "appScope", "$location",
    function($appScope, $location){
    return {
            transclude: true,
            scope:{
                'group':'=group'
            },
            template:'<input id="{{group.id}}" type="checkbox">'+
                     '<label for="{{group.id}}">All</label>',
            link: function(scope, element, attr){
                var input = element.find("input");
                var label = element.find("label");
                scope.$watch(function () {
                    return $location.hash();
                },  function (newval, oldval){
                    input.prop("checked", scope.group.allCheck);
                    input.prop("disabled", scope.group.allCheck);
                });

                (element).children().bind("click", function(event){
                    event.preventDefault();
                    var child = jQuery(element).parent().find("input.filter");
                    jQuery(child).each(function(ele){
                        jQuery(this).prop("checked", false);
                    });

                    $appScope.getScope().facetClick(scope.group.coll[0]);
                });
            }
        }
}]);

pearsonCourseMaterialDirectives.directive('lessBtn', [ "$location", function($location){
    return {
            transclude: true,
            scope:{
                group: "="
            },
            template: '<a href="#"></a>',
            link: function(scope, element, attr){
                if(scope.group.coll.length > scope.group.displayLimit && scope.group.showAll){
                    element.show();
                }else{
                    element.hide();
                }

                var anchor = element.find("a");
                anchor.html("Show more");
                anchor.bind("click", function(event){
                    event.preventDefault();
                    if(anchor.html() == "Show more"){
                        anchor.html("Show less");
                        scope.group.displayLimit = scope.group.coll.length;
                    }else{
                        anchor.html("Show more");
                        scope.group.displayLimit = 4;
                    }
                    scope.$apply();
                });
            }
        }
}]);

//Allows you to call a non argument function from the directive
pearsonCourseMaterialDirectives.directive('repeatCallback', [ "$location", "appScope", function($location, appScope){
    return function(scope, element, attr){
            if (scope.$last) {
                appScope.getScope()[attr.repeatCallback]();
            }
        }
}]);
