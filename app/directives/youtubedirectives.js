'use strict';

/* Directives */
var pearsonYoutubeDirectives = angular.module('pearsonYoutubeDirectives', ['pearsonServices']);

pearsonYoutubeDirectives.directive('ytVideo', [ "$sce", "$location", "appScope",
    function($sce, $location, appScope){
    return {
        scope: { play:'=' },
        replace: true,
        template: '<div><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="{{url}}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>',
        link: function (scope, element) {
            if(scope.play) {
                scope.url = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + scope.play.id + "?rel=0&fs=1");
                (element).show();
            }else{
                scope.url = $sce.trustAsResourceUrl("http://www.youtube.com/embed/MkyK4aaYzpc?rel=0&fs=1");
                (element).hide();
            }
        }
    };
}]);

pearsonYoutubeDirectives.directive('ytPlaylistEntry', [ "$sce", "$location", "appScope", function($sce, $location, appScope){
    return function (scope, element, attr) {
        var $appScope   = appScope.getScope();

        jQuery(element).on("click", "a", function(e){
            e.preventDefault();
            $appScope.changeVideo(scope.vid.id);
        });

        scope.$watch( function(){ return $appScope.play }, function(newval, oldval){
            //Check to see if I am the selected video
            if(newval == scope.vid.id){
                //Set the current class on the element
                jQuery(element).addClass("hidden");
            }else{
                jQuery(element).removeClass("hidden");
            }
        });
    };
}]);


//###### DEPRECIATED #######
pearsonYoutubeDirectives.directive('ytPlaylist', [ "appScope", "$window", "$timeout",
    function(appScope, $window, $timeout) {
    return function (scope, element, attr) {
        var $appScope   = appScope.getScope();
        var video       = jQuery(element).find(".video");
        //init the view and display of the app depending on the width of the container
        toggleClass(jQuery(element).width());
        //Since the class may have been updated before the dom responds the width will be off,
        //need to calculate the real width
        scaleVideo((video).width() * ($appScope.videoclass.match(/\d+/g)[0] / 12));

        jQuery($window).bind('resize', function() {
            toggleClass(jQuery(element).width());
            scaleVideo((video).width());

            scope.$apply();
        });

        function toggleClass(width){
            if(width < 720){
                $appScope.videoclass       = "col-md-12";
                $appScope.playlistclass    = "col-md-12";
            }else{
                $appScope.videoclass       = "col-md-9";
                $appScope.playlistclass    = "col-md-3";
            }
        }

        function scaleVideo(width){
            $appScope.videoHeight = width * 0.75;
        }
    }
}]);