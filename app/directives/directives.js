/**
 * Created by daniespr on 03/07/2014.
 */

var pearsonDirectives = angular.module("pearsonDirectives", ['pearsonServices']);


pearsonDirectives.directive("include", [ '$compile', '$http', '$templateCache',
    function($compile, $http, $templateCache){
        return function (scope, element, attr){
            $http.get(scope[attr.url], {cache: $templateCache}).then(function(html){
                element.html($compile(html.data)(scope));
            });

            scope.$watch(function(){ return scope[attr.url] }, function(newval, oldval){
                $http.get(newval, {cache: $templateCache}).then(function(html){
                    element.html($compile(html.data)(scope));
                });
            });
        }
    }
]);
