'use strict';

/* Directives */
var pearsonNewsListDirectives = angular.module('pearsonNewsListDirectives', ['pearsonServices']);

pearsonNewsListDirectives.directive('filterFacet', [ "serializeForm", "$location", "appScope",
    function(serializeForm, $location, appScope) {
	return function(scope, element, attr) {
		
		var parentScope = appScope.getScope();
		
		//uncheck all checkboxes
		jQuery(element).find("input").prop("checked",false);
		
		//then check the default selected one
		var defaultFacet = parentScope.defaultFacet;
		var currentFacet = attr.facet;
		if(currentFacet == defaultFacet || defaultFacet.indexOf(currentFacet) > -1){
			//if all is selected already do not select sub facet as well
			if(jQuery("input:checkbox[id='all']").prop("checked") == false) {
				jQuery(element).find("input").prop("checked",true);
				var facetCount = parseInt(attr.facetCount);
				if(facetCount < parentScope.STATE.displayLimit) {
					parentScope.STATE.resultsLength = facetCount;
					parentScope.STATE.displayLimit = facetCount;
				}else{
					parentScope.STATE.resultsLength = facetCount;
				}
			}
		}
		    	
		jQuery(element).find("label").bind("click", function(event) {
        	var facetType = attr.facetType;
        	var that = jQuery(element).find("input");   
        	event.preventDefault();
        	if(facetType == "all") {
        		parentScope.STATE.resultsLength = parentScope.newsList.length;
        		parentScope.STATE.displayLimit = 20;
        		//loop through facets and uncheck them
                for(var i = 0; i < parentScope.facets.length; i++){
                	var facetCheckbox = jQuery(element).siblings().find("#" + parentScope.facets[i].facet);
                	jQuery(facetCheckbox).prop("checked", false);
                }
                //then check and disable all
                jQuery(that).prop("checked", true);
                jQuery(that).prop("disabled", true);
        	} else {
        		//1. check/un-check that selected facet
        		jQuery(that).prop("checked", function( i, val ) {
                    return !val;
                });
        		//2. Check at least one facet is selected otherwise check all
            	var allCheckbox = jQuery(element).siblings(".all-filter").find("input");
            	var checked = jQuery(element).parent().parent().find("input:checked").not("#all");
            	if(checked.length > 0){
            		//a facet is selected so make sure all is unchecked and enabled
	            	jQuery(allCheckbox).prop("checked",false);
	            	jQuery(allCheckbox).prop("disabled", false);
	            	//loop through checked facets and add totals
	            	var facetCount = 0;
	                for(var i = 0; i < checked.length; i++){
	                	var countString = jQuery(checked[i]).siblings().find(".count").text();
	                	countString = countString.substring(1, (countString.length-1));
	                	facetCount = facetCount + parseInt(countString);
	                }
	                parentScope.STATE.resultsLength = facetCount;
	                parentScope.STATE.displayLimit = 20;
                } else {
                    jQuery(allCheckbox).prop("checked",true);
	            	jQuery(allCheckbox).prop("disabled", true);
	            	parentScope.STATE.resultsLength = parentScope.newsList.length;
	            	parentScope.STATE.displayLimit = 20;
                }
        	}
        	parentScope.resolveState();
        	parentScope.facetClick();
        });
		
    }	
}]);

pearsonNewsListDirectives.directive('sortButton', [ "resolveShowMore", "$location", function(resolveShowMore, $location){
    return {
        transclude: true,
        template: '<a href="#">default</a>',
        link: function(scope, element, attr){
            element.find("a").html(attr.title);

            scope.$watch("orderProp",  function (newval, oldval){
                if (newval == attr.option)  {
                    element.addClass("current");
                } else {
                    element.removeClass("current");
                }
            });

            element.bind("click", function(event){
               event.preventDefault();
               //Set the model to do many things
               scope.sortFunction(attr.option);
               //scope.orderProp = ;
               scope.$apply();
            });
        }
    }
}]);

pearsonNewsListDirectives.directive('lessBtn', [ "resolveShowMore", "$location",
    function(resolveShowMore, $location){
    return {
            transclude: true,
            scope:{
                group: "="
            },
            template: '<a href="#"></a>',
            link: function(scope, element, attr){
                if(scope.group.coll.length > scope.group.displayLimit){
                    element.show();
                }else{
                    element.hide();
                }

                var anchor = element.find("a");
                anchor.html("Show more");
                anchor.bind("click", function(event){
                    event.preventDefault();
                    if(anchor.html() == "Show more"){
                        anchor.html("Show less");
                        scope.group.displayLimit = scope.group.coll.length;
                    }else{
                        anchor.html("Show more");
                        scope.group.displayLimit = 4;
                    }
                    scope.$apply();
                });
            }
        }
}]);