'use strict';

/* Directives */
var pearsonQualificationDirectives = angular.module('pearsonQualificationDirectives', []);

pearsonQualificationDirectives.directive('sortButton', [function () {
    return {
        transclude: true,
        template: '<a href="#">default</a>',
        link: function (scope, element, attr) {
            scope.$watch("groupByFacet", function (newval, oldval) {
                element.find("a").html(attr.title);
                if (newval == attr.facet) {
                    element.addClass("current");
                } else {
                    element.removeClass("current");
                }
            });

            element.bind("click", function (event) {
                event.preventDefault();
                scope.groupByFacet = attr.facet;
                
                scope.$apply();
            });
        }
    }
}]);

pearsonQualificationDirectives.directive('descSwitch', [function () {
    return function (scope, element, attr) {
            scope.$watch("showDescription", function (newval, oldval) {
                //TODO: check to see the default format to show or to hide
                //Set the text accordingly

                showDescription(newval);

            });

            function showDescription(show){
                if (show) {
                    element.html("Hide qualification details");
                    $(".qf-desc").slideDown();

                    element.addClass("ctn-open");
                    element.removeClass("ctn-closed");
                } else {
                    element.html("Show qualification details");
                    $(".qf-desc").slideUp();

                    element.removeClass("ctn-open");
                    element.addClass("ctn-closed");
                }
            }

            element.bind("click", function(event){
                event.preventDefault();
                scope.showDescription = !scope.showDescription;
                showDescription(scope.showDescription);
                scope.$apply();
            });
        }
}]);

//Allows you to call a non argument function from the directive
pearsonQualificationDirectives.directive('repeatCallback', [ "appScope", function(appScope){
    return function(scope, element, attr){
        if (scope.$last) {
            appScope.getScope()[attr.repeatCallback]();
        }
    }
}]);