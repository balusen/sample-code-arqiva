'use strict';

/* Directives */
var pearsonPastPapersDirectives = angular.module('pearsonPastPapersDirectives', []);

pearsonPastPapersDirectives.directive('sortButton', [ "resolveShowMore", function(resolveShowMore){
    return {
        transclude: true,
        template: '<a href="#">default</a>',
        link: function(scope, element, attr){
            scope.$watch("headerVal",  function (newval, oldval){
                element.find("a").html(attr.title);
                if (newval == attr.facet) {
                    element.addClass("current");
                } else {
                    element.removeClass("current");
                }
            });

            var displayLimit = scope.$eval(attr.sortDisplayLimit);

            element.bind("click", function(event){
               event.preventDefault();
               scope.setSort(attr.facet);
               scope.STATE.displayLimit=displayLimit;
               resolveShowMore();
               scope.$apply();
            });
        }
    }
}]);

pearsonPastPapersDirectives.directive('statusButton', [ "Subjects", function(Subjects){
    return {
        transclude: true,
        template: '<a href="#">default</a>',
        link: function(scope, element, attr){
            scope.$watch("subjectStatus",  function (newval, oldval){
                element.find("a").html(attr.title);
                if (newval == attr.facet)  {
                    element.addClass("current");
 

                } else {
                    element.removeClass("current");
                }
            });

            element.bind("click", function(event) {
                event.preventDefault();
                scope.subjectStatus = attr.facet;
                //When this is clicked, i.e the subject status becomes the same as this button
                //get new grouped subjects based on this facet

                var tagsArray = []; tagsArray = tagsArray.concat(scope.pastPapersFacet);
                tagsArray.push(scope.qualificationFamilyVar, scope.subjectStatus);
                Subjects.getSubjectsForQualification(tagsArray).then(function(subjects){
                    scope.showSubjectResults = (subjects.searchResults.solrDocuments.length > 0) ? true : false;
                    scope.subjectsData = subjects;
                    scope.groupedSubjects = Subjects.getSubjectsGroupedByFirstLetter(subjects.searchResults.solrDocuments,
                        scope.subjectStatus);
                });
                
                scope.$apply();
            });
        }
    }
}]);

pearsonPastPapersDirectives.directive('modalLink', [function () {
    return {
        transclude: true,
        scope:{
            "subject":"=subject"
        },
        template: "{{subject.subjectName | cleanTitle }}",
        link: function (scope, element, attr) {
        element.attr("href", "#" + scope.subject.subjectName.replace(" ", "-"));
    }
    }
}]);
