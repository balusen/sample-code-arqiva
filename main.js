webshim.setOptions('basePath', '/etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/js/js-webshim/minified/shims/');
webshim.setOptions("forms", {
    replaceValidationUI: true
});
webshim.polyfill('forms');

var Pearson = (Pearson || {});

Pearson.initCarousel = function(carousel) {
    // Carousel
    var carouselOuter = (carousel).parents('.carousel-outer'),
        initialAutoplaySpeed = 3000, // default
        autoplaySpeed = 3000,
        slideCount = 3; // default

    if((carousel).attr('data-initialautoplayspeed')) {
        initialAutoplaySpeed = parseInt((carousel).attr('data-initialautoplayspeed'), 10);
    }

    if((carousel).attr('data-autoplayspeed')) {
        autoplaySpeed = parseInt((carousel).attr('data-autoplayspeed'), 10);
    }

    if((carousel).attr('data-slidecount')) {
        slideCount = parseInt((carousel).attr('data-slidecount'), 10);
    }

    if((carousel).hasClass('small-carousel')) {
        (carousel).slick({
            dots: true,
            autoplay: true,
            fade: true,
            autoplaySpeed: autoplaySpeed,
            slidesToShow: slideCount,
            slidesToScroll: slideCount
        });

    } else {
        (carousel).slick({
            dots: true,
            autoplay: true,
            fade: true,
            slidesToShow: 1,
            autoplaySpeed: autoplaySpeed
        });
    }

    setTimeout(function() {
        (carousel).slickSetOption('autoplaySpeed', autoplaySpeed, true);
    }, initialAutoplaySpeed);
};

Pearson.Cookie = {
    setCookie: function(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setMonth(date.getMonth()+days);
            expires = "; expires="+date.toGMTString();
        }
        document.cookie = name+"="+value+expires+"; path=/";
    },
    getCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
};

Pearson.init = function($) {
    var Cookie = Pearson.Cookie;

    // localStorage - simple fallback to do nothing in unsupported browsers
    if (!('localStorage' in window)) {
        var lsOptions = ['getItem', 'setItem'];
        window.localStorage = {};

        for (var S = 0; S < lsOptions.length; S++) {
            window.localStorage[lsOptions[S]] = function (){};
        }
    }

    // sessionStorage - simple fallback to do nothing in unsupported browsers
    if (!('sessionStorage' in window)) {
        var stOptions = ['getItem', 'setItem'];
        window.sessionStorage = {};

        for (var S = 0; S < stOptions.length; S++) {
            window.sessionStorage[stOptions[S]] = function (){};
        }
    }

    // Helper variable
    var isMobile = $('.mobile-expand').is(':visible');

    // Helper function to ensure all items in a collection are the same height as the tallest item
    $.fn.equalizeHeights = function() {
        if(isMobile) {
            return false;
        }

        var maxHeight = this.map(function(i, e) {
            return $( e ).height();
        }).get();
        return this.css('min-height', Math.max.apply(this, maxHeight));
    };

    // Remove no-js class from html tag
    document.documentElement.className = document.documentElement.className.replace("no-js","js");

    var specfinder = $(".specificationfinder");
    // Since the function calls are not being included in the jsp/ keep the
    // function from being called on a page without the "Specification Finder component"
    if (specfinder.attr("class") !== undefined){
        qualInit(specfinder);
        qualificationsDropDown(specfinder);
        qualificationsSubmit(specfinder);
    }

    Pearson.initUserTabs();
    Pearson.initSupportTopicPageTabs();
    Pearson.initSimpleList();
    Pearson.initReadMore();
    Pearson.initPersonalization();
    Pearson.initGeoUtils();

    // Initiate all carousels on page
    // We need to loop over the dom to wait for the carousel markup to appear on the page
    var carouselRetryCount = 1,
        carouselTimer = null,
        carouselFound = false;
    (function initEachCarousel() {
        var eachCarousel = $('.carousel');
        (eachCarousel).each(function() {
            if(($(this)).length > 0) {
                Pearson.initCarousel($(this));
                carouselFound = true;
                if(carouselTimer) { // clear loop if we've already got our value
                    clearTimeout(carouselTimer);
                }
            }
        });
        carouselTimer = setTimeout(function() {
            if(carouselRetryCount <= 10 && !carouselFound) { // limit to 10 seconds
                initEachCarousel();
            } else {
                clearTimeout(carouselTimer);
            }
            carouselRetryCount++;
        }, 300);
    })();

    // Add scrolling to a letter in the subject list
    var scrollNode = $.browser.webkit ? $('body') : $('html');
    $('.index-list').on('click', 'a', function(e) {
        e.preventDefault();
        var targetNode = $('a[name="'+$(this).attr('href').replace('#','')+'"]');
        (scrollNode).animate({scrollTop: ((targetNode.offset().top)-10)}, 500, function() {});
    });

    // Back to top button
    $('.top-button').on('click', function(e) {
        e.preventDefault();
        var targetNode = $('#'+$(this).attr('href').replace('#',''));
        (scrollNode).animate({scrollTop: (targetNode.offset().top)}, 300, 'swing');
    });

    // Standfirst mobile show more/less
    var standFirst = $('.standfirst');
    (standFirst).on('touchstart click', '.show-moreless', function(e) {
        e.preventDefault();
        var that = $(this),
            targetNode = (that).parent();

        (that).parent().toggleClass('mobile-expand');
        if((that).find('span').text() === 'Read more') {
            (that).find('span').text('Read less');
        } else {
            (that).find('span').text('Read more');
        }
    });

    // Hide 'Read more' functionality if standfirst text less than 170 characters
    if((standFirst).find('p').text().length < 170) {
        (standFirst).addClass('small');
    }


    // Mobile menu on standalone past papers (http://localhost:4502/content/past-papers/home.html?wcmmode=disabled)
   $('.navbar-primary').on('touchstart click', '.mobile-expand', function(e) {
       e.preventDefault();
        var that = $(this);
        var navCover = $('<div />', {
              'class': 'nav-cover'
        }).appendTo('body');
        navCover.on('touchstart click', function(e) {
            e.preventDefault();
            (navCover).remove();
            (that).siblings('ul').removeClass('mobile-open');
        });
        
            (that).siblings('ul').toggleClass('mobile-open');       
    });

    // Alert - dismiss alert
    $('.alerts').on('click', '.close', function(e) {
        e.preventDefault();
        var that = $(this);
        (that).parent().fadeOut();
    });

    // Pathway accordion - expand first when there is only one pathway
    if($('.cq-wcm-edit').length) {
        // Expand all when in edit mode
        $('.cq-wcm-edit .content-tabs .pathwayaccordion .accordion .accordion-toggle').click();
    }
    else if($('.content-tabs.current .pathwayaccordion').length == 1) {
        $('.content-tabs .pathwayaccordion:first-child .accordion .accordion-toggle').click();
    }

    //Close all accordion while the other is clicked
    $('.content-tabs.current .pathwayaccordion').live('click', function(e) {
        var openAccordion = $(this).siblings().find('.open .accordion-toggle');
        openAccordion.each(function() {
            openAccordion.click();
        });
    });

    // Initiate FastClick to remove the tap delay on mobile
    // Removed FastClick because it interferes with Angular on iOS
    // FastClick.attach(document.body);

    // Popover
    var bodyModal = null;
    var pageScrollPos = 0;
    var modalCover = $('<div />', {'class': 'modal-cover'}).appendTo('body');
    var closeModal = function() {
            (scrollNode).removeClass('modal-enabled');

            (modalCover).fadeOut('fast', function() {
                // If the content has a data-cookieval attribute then set the value of that cookie to true
                if((modalCover).find('.modal').attr('data-cookieval')) {
                    Cookie.setCookie((modalCover).find('.modal').attr('data-cookieval'), true, null);
                }

                (modalCover).html('');
            });
            bodyModal = null;

            (scrollNode).animate({scrollTop: pageScrollPos}, 0, function() {});
    };

    $('#content-main, #content, .subject-az-list').on('click', 'a.modal-link', function(e){ modalScreen(e, $(this), null, null); });

    function modalScreen(e, that, content, callback) {
        if(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        var thatModal = (content) ? $(content) : (that).siblings('.modal[id="'+(that).attr('href').replace('#','')+'"]'),
            modalTop = 0;

        //Disable certain elements of the modal functionality based on classes set on the modal
        //Disable clicking off the modal will not close it
        var disableClickOff = (thatModal).hasClass("click-off-disabled");
        //Get rid off the close button
        var disableCloseBtn = (thatModal).hasClass("close-btn-disabled");
        //Stop the modal from being able to be closed at all
        var disableClose    = (thatModal).hasClass("close-disabled");

        // Re-intialise form polyfills for when modal loads new forms
        $('.form-content').updatePolyfill();

        pageScrollPos = $(document).scrollTop();

        if(modalScreen && modalScreen.length > 0) {
            (scrollNode).off('click');

            // duplicate and add to body
            if(bodyModal) {
                (bodyModal).remove();
                bodyModal = null;
            }

            var modalContent = (thatModal).clone(true);
            if(!modalContent.is(":visible")) {
                modalContent.show();
            }
            bodyModal = modalContent.appendTo(modalCover);

            //If the disable close and disable close button are false then create the close button
            if(!disableClose && !disableCloseBtn) {
                $('<a />', {
                    'class': 'close',
                    'html': 'close <span>x</span>',
                    'href': '#'
                }).on('click', function(x) {
                    x.preventDefault();
                    closeModal();
                }).appendTo((bodyModal));
            }

            // Show everything
            (scrollNode).addClass('modal-enabled');
            $('.modal-cover').css('top', (scrollNode).scrollTop());

            (modalCover).fadeIn('fast', function() {
                var modalObject = $('.modal-cover .modal');

                // Calculate center position of modal (vertical and horizontal)
                if(modalObject.outerHeight() > (scrollNode).height()) { // If height of modal is greater than
                    (modalObject).css('top', 0);
                    (modalObject).css('margin-top', 0);
                } else {
                    (modalObject).css('top', '50%');
                    (modalObject).css('margin-top', -(modalObject.outerHeight()/2));
                }
                (modalObject).css('margin-left', -(modalObject.outerWidth()/2));
                (modalObject).css('visibility', 'visible');

                $(window).resize(function() {
                    var modalObject = $('.modal-cover .modal');

                    // Calculate center position of modal (vertical and horizontal)
                    if(modalObject.outerHeight() > (scrollNode).height()) { // If height of modal is greater than
                        (modalObject).css('top', 0);
                        (modalObject).css('margin-top', 0);
                    } else {
                        (modalObject).css('top', '50%');
                        (modalObject).css('margin-top', -(modalObject.outerHeight()/2));
                    }
                    (modalObject).css('margin-left', -(modalObject.outerWidth()/2));
                });

                if(callback) {
                    callback.call(this);
                }
            });

            // Scroll modal to top if on mobile
            if($('.modal-enabled').css('overflow') == 'visible') {
                (scrollNode).animate({scrollTop: 0}, 0, function() {});
            }

            //Capture 'a' tag clicks within the modal in question rather than the "$('.modal')"
            $(bodyModal).delegate('a', 'click', function(e) {
                if($(this).hasClass('read-more')){
                    Pearson.readMore(e, $(this));
                } else {
                    if(!disableClose && !disableCloseBtn) {
                        closeModal();
                    }
                }
            });

            // Remove modal box on click
            (scrollNode).on('click', function(x) {
                var clickedModal = false,
                    localDisableClickOff = disableClickOff || false,
                    localDisableClose = disableClose || false;

                if($(x.target).is('.modal') || $(x.target).parents().is('.modal')) {
                    clickedModal = true;
                }
                if(!$(x.target).is(that) && !clickedModal) {
                    //if both disable close click off and disable close button are false then closeModal
                    if(!localDisableClickOff && !localDisableClose) {
                        closeModal();
                    }
                    (scrollNode).off('click');
                }
            });
        }
    }

    // Remove modal box on escape key
    $(document).on('keyup', function(e) {
        if (e.keyCode == 27 && bodyModal) {
            //if both disable close click off and disable close button are false then closeModal
            if(!disableClickOff && !disableClose) {
                closeModal();
            }
        }
    });

    // Open drawer
    // - facets
    if($('.search-page .facets').length) {
        var filterPageScrollPos = 0,
            openDrawer = function() {
            // Move the facet list in the dom
            var facetList = $('.facets').detach(),
                content = $('#content');
                // contentCover = $('<div />', { 'class': 'content-cover' } ).css('top', (scrollNode).scrollTop()+'px').appendTo('body');

            filterPageScrollPos = $(document).scrollTop();

            (facetList).insertBefore(content);

            // Add body CSS hook
            $('body').addClass('open-drawer');

            // Add click event to #content to reappear
            // setTimeout(function(){
            //     (contentCover).on('click', function(x) {
            //         closeDrawer();
            //     });
            // }, 0);
        };
        var closeDrawer = function() {
            $('.facets').detach().insertBefore($('.search-page .results')); // Move facets back to original location in DOM
            $('.facets').show();
            $('body').removeClass('open-drawer');
            (scrollNode).animate({scrollTop: filterPageScrollPos}, 0, function() {});
            // $('.content-cover').remove();
        };

        // Add "Filter results" button
        $('<a />', {
            'href': '#',
            'class': 'filter-drawer-toggle',
            'text': 'Filter results'
        }).on('click', function(e){
            e.preventDefault();
            openDrawer();
        }).insertBefore($('.results .inner'));

        // Add "Update results" and "cancel" button
        var updateCancelContainer = $('<div />', {
            'class': 'update-cancel-container'
        }).on('click', function(e){
            e.preventDefault();
        });

        // Filter title
        $('<h1 />', {
            'text': 'Filter results'
        }).appendTo(updateCancelContainer);

        // Update results
        $('<a />', {
            'href': '#',
            'class': 'cancel-button btn btn-default',
            'text': 'Cancel'
        }).on('click', function(e){
            e.preventDefault();
            closeDrawer();
        }).appendTo(updateCancelContainer);

        // Cancel
        $('<a />', {
            'href': '#',
            'class': 'update-results btn btn-primary',
            'text': 'Save'
        }).on('click', function(e){
            e.preventDefault();
            closeDrawer();
        }).appendTo(updateCancelContainer);

        (updateCancelContainer).appendTo($('.facets'));
    }

    // Expand full description on step selection component on training courses
    $('.step-selection.training-courses').delegate('a.show-more', 'click', function(e) {
        e.preventDefault();

        var that = $(this),
            originalText = ((that).data('original-text') && (that).data('original-text').length > 1) ? (that).data('original-text') : (that).text(),
            toggleText = ((that).data('toggle-text') && (that).data('toggle-text').length > 1) ? (that).data('toggle-text') : 'Show less',
            originalContent = (that).siblings('.part-description'),
            hiddenContent = (that).siblings('.full-description'),
            isHidden = (hiddenContent).is(':hidden');

        // Save original text of link
        if(!(that).data('original-text')) {
             (that).data('original-text', (that).text());
        }

        if(isHidden) {
            (originalContent).hide();
            (hiddenContent).show();
            (that).text(toggleText);
            (that).removeClass('ctn-closed');
            (that).addClass('ctn-open');
        } else {
            (originalContent).show();
            (hiddenContent).hide();
            (that).text(originalText);
            (that).removeClass('ctn-open');
            (that).addClass('ctn-closed');
        }
    });


// Expand full description on step selection component on Published Resources
    $('.search-page.course-materials').delegate('a.show-more', 'click', function(e) {
        e.preventDefault();

        var that = $(this),
            originalText = ((that).data('original-text') && (that).data('original-text').length > 1) ? (that).data('original-text') : (that).text(),
            toggleText = ((that).data('toggle-text') && (that).data('toggle-text').length > 1) ? (that).data('toggle-text') : 'Show less',
            originalContent = (that).siblings('.part-description'),
            hiddenContent = (that).siblings('.full-description'),
            isHidden = (hiddenContent).is(':hidden');

        // Save original text of link
        if(!(that).data('original-text')) {
             (that).data('original-text', (that).text());
        }

        if(isHidden) {
            (originalContent).hide();
            (hiddenContent).show();
            (that).text(toggleText);
            (that).removeClass('ctn-closed');
            (that).addClass('ctn-open');
        } else {
            (originalContent).show();
            (hiddenContent).hide();
            (that).text(originalText);
            (that).removeClass('ctn-open');
            (that).addClass('ctn-closed');
        }
    });

    // Tooltips
    $('.tooltip').on('hover, click', function(e) {
        e.preventDefault();

        var that = $(this),
            pos = (that).offset();

        if(pos.left > 690) { // if tooltip is too far right then flip the x-axis
            (that).addClass('flip');
            (that).removeClass('non-flip');
        } else {
            (that).addClass('non-flip');
            (that).removeClass('flip');
        }

        // if opening on mobile position in center of screen
        if(isMobile) {
            (that).find('.tip').css('top', '10px');
            (that).find('.tip').css('left', -(~~(pos.left) - 10));
        }

        $('.tooltip').not(that).removeClass('tooltip-open');
        (that).toggleClass('tooltip-open');
    });

    // Support topics selector
    var supportCategoryListing = $('#content-main'),
        supportCategoryListingLinks = (supportCategoryListing).find('.subjects-container-drawer .link-list a span, .subject-list h4 a span');

    //setTimeout(function() {
       // (supportCategoryListing).each(function() {
           //var that = $(this),
                //subjectList = (that).find('.subject-az-list');

           // if((subjectList).length > 0) {
                // Ensure link-list height is same as container
                //(subjectList).css('min-height', (that).find('.link-list').height());
            //}
       // });
    //}, 3000);

    var supportCategoryListingActivate = function(e, that) {
        e.preventDefault();

        var id = (that).attr('href'),
            linkTarget = id.replace('#', ''),
            replaceString = ((id.indexOf('find-qual-') > -1) ? 'find-qual' : 'support-topic'),
            linkTargetContent = id.replace(replaceString+'-', replaceString+'-content-'),
            assocPanelHolder = (that).parents('.subjects-container'),
            subjectList = (assocPanelHolder).find('.subject-az-list');

        // Remove current class from link
        (that).parent().siblings('li').removeClass('current');

        // Add current class to clicked link
        (that).parent().addClass('current');

        // Remove current class from content
        (assocPanelHolder).find('.subject-az-list li').removeClass('current');

        // Add current class to associated content
        (assocPanelHolder).find('.subject-az-list '+linkTargetContent).addClass('current');

        // Ensure link-list height is same as container
        (subjectList).css('min-height', (assocPanelHolder).find('.link-list').height());

        // include hash fragment in url
        window.location.hash = linkTarget;
    };

    var supportTopicsActivate = function(e, that) {

        var id = (that).attr('href'),
            linkTarget = id.replace('#', ''),
            replaceString = ((id.indexOf('find-qual-') > -1) ? 'find-qual' : 'support-topic'),
            linkTargetContent = id.replace(replaceString+'-', replaceString+'-content-'),
            assocPanelHolder = (that).parents('.subjects-container'),
            subjectList = (assocPanelHolder).find('.subject-az-list');
            
    // Remove current class from link
        (that).parent().siblings('li').removeClass('current');

        // Add current class to clicked link
        (that).parent().addClass('current');

        // Remove current class from content
        (assocPanelHolder).find('.subject-az-list li').removeClass('current');

        // Add current class to associated content
        (assocPanelHolder).find('.subject-az-list '+linkTargetContent).addClass('current');

        // Ensure link-list height is same as container
        //(subjectList).css('min-height', (assocPanelHolder).find('.link-list').height());

    };
    // Attach appropriate events to selectors
    //(supportCategoryListing).on('hover', '.subjects-container-drawer .link-list a span', function(e) { supportCategoryListingActivate(e, $(this).parent()) });
    //(supportCategoryListing).on('click', '.subjects-container-drawer .link-list a', function(e) { supportCategoryListingActivate(e, $(this)) });
    (supportCategoryListing).on('hover', '.subjects-container-drawer .link-list a span', function(e) {  supportTopicsActivate(e, $(this).parent()) });
    (supportCategoryListing).on('click', '.subjects-container-drawer .link-list a', function(e) {  supportTopicsActivate(e, $(this)) });
    
    (supportCategoryListing).on('click', '.subject-list h4 a', function(e) { supportCategoryListingActivate(e, $(this)) });

    // - If window has a hash fragment starting with #support-topic- then trigger a click
    if(supportCategoryListingLinks && supportCategoryListingLinks.length > 0 && window.location.hash.length > 0 && (window.location.hash.indexOf('support-topic-') > -1 || window.location.hash.indexOf('find-qual-') > -1)) {
        (supportCategoryListingLinks).each(function(){
            var that = $(this).parent(),
                linkTarget = (that).attr('href');

            if(window.location.hash === linkTarget) {
                (that).trigger('click');
            }
        });
    }

    // "On this page" component
    if($('#otp-content').length) {
        //Path: etc/designs/ped/pei/uk/pearson-uk-new/clientlibs/js/onthispage.js
        Pearson.initOTP();
    }

    // Fit embedded youtube to correct scale
    $("#content-main").fitVids({ ignore: '#mySurvey'});

    // Welcome screen
    if($('.welcomemodal_TEST').length) { // inactive for now
        var welcomeModal = $('.welcomemodal'),
            welcomeModalCookieName = (welcomeModal).attr('data-cookieval')

        if(!Cookie.getCookie(welcomeModalCookieName) || (Cookie.getCookie(welcomeModalCookieName) && Cookie.getCookie(welcomeModalCookieName).length < 1)) {
            modalScreen(null, null, '.welcomemodal', function() {
                $('.welcomemodal input[name="location"][value="'+$('.countrySelector').val()+'"]').attr('checked', true);
                $('.welcomemodal').on('submit', 'form', function(e) {
                    e.preventDefault();
                    $('.countrySelector').val($(this).find('input[name="location"]:checked').val());
                    $('.countrySelector').trigger('change');
                    closeModal();
                });
            });
        }
    }

    // Ensure all buttons in disclosure tabs are same height
    $('.responsiverow .disclosure-tabs .nav-tabs > li a').equalizeHeights();

    // Ensure spec columns are same height
    $('.specificationinfo .course-details .inner, .specificationinfo .related-links .inner').equalizeHeights();
    setTimeout(function(){
        $('.specificationinfo .course-details .inner, .specificationinfo .related-links .inner').equalizeHeights();
    }, 500);

    // Overcome issue with wide tables being cut off
    // eg. http://www.alpha.onepearson.com/content/demo/en/support/support-topics/exams/exam-timetables.html
    $.fn.fixHiddenTables = function() {
        this.map(function(i, e) {
            if($(e).find('table').length == 1) {
              $(e).css('height', $(e).height());
              $(e).find('table').css('position', 'absolute');
            }
        }).get();
        return true;
    };
    $.fn.hideFixedHiddenTables = function() {
        this.map(function(i, e) {
            $(e).css('height', 'auto');
            $(e).find('table').css('position', 'static');
        }).get();
        return true;
    };

    // Side scrolling - only enable if we are browsing on small screen
    // We can simply test the visiblity of a mobile only page element
    if(isMobile) {
        // Enable side dragging on top sub nav
        if($('.secondary-nav .child-page-links').length > 0) {
            (function() {
                var navScroll = new IScroll('.secondary-nav .child-page-links', {
                    scrollX: true,
                    scrollY: false,
                    click: true,
                    useTransition: false,
                    bounce: false
                });

                // Arrows
                var container = $('.secondary-nav .child-page-links').parent(),
                    arrowLeft = (container).find('.left-arrow-container'),
                    arrowRight = (container).find('.right-arrow-container'),
                    leftLimit = false,
                    rightLimit = false,
                    scrollJumpAmout = 80;

                // (arrowLeft).on('click', function(){
                //     if(!leftLimit) {
                //         var gap = navScroll.wrapperWidth - navScroll.scrollerWidth,
                //             scrollJump = (Math.abs(navScroll.x) < scrollJumpAmout) ? 0 : (navScroll.x + scrollJumpAmout);
                //
                //         navScroll.scrollTo(scrollJump, 0, 50, IScroll.utils.ease.quadratic);
                //     }
                // });
                // (arrowRight).on('click', function(){
                //     if(!rightLimit) {
                //         var gap = navScroll.wrapperWidth - navScroll.scrollerWidth,
                //             scrollJump = (Math.abs(navScroll.x - gap) < scrollJumpAmout) ? gap : (navScroll.x - scrollJumpAmout);
                //
                //         navScroll.scrollTo(scrollJump, 0, 50, IScroll.utils.ease.quadratic);
                //     }
                // });

                // Now scroll to current item
                navScroll.scrollToElement('.secondary-nav .child-page-links li.current', null, true, null, false);
                navScroll.on('scrollEnd', function () {
                    var scrollLimit = this.wrapperWidth - this.scrollerWidth;

                    if(this.x == 0) {
                        (arrowLeft).fadeOut('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = true;
                        rightLimit = false;
                    } else if(this.x == scrollLimit) {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeOut('fast');
                        leftLimit = false;
                        rightLimit = true;
                    } else {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = false;
                        rightLimit = false;
                    }
                });

                var scrollLimit = navScroll.wrapperWidth - navScroll.scrollerWidth;

                // Setup visibility of arrows
                if(navScroll.x == 0) {
                    (arrowLeft).fadeOut('fast');
                    (arrowRight).fadeIn('fast');
                } else if(navScroll.x == scrollLimit) {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeOut('fast');
                } else {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeIn('fast');
                }
            })();
        }

        // Enable side dragging spec page specification tabs
        if($('.spec-nav-tabs').length > 0) {
            (function() {
                var navScroll = new IScroll('.spec-nav-tabs', {
                    scrollX: true,
                    scrollY: false,
                    click: true,
                    useTransition: false,
                    bounce: false
                });

                // Arrows
                var container = $('.spec-nav-tabs').parent().find('.spec-nav-tabs-arrows'),
                    arrowLeft = (container).find('.left-arrow-container'),
                    arrowRight = (container).find('.right-arrow-container'),
                    leftLimit = false,
                    rightLimit = false,
                    scrollJumpAmout = 80;

                // (arrowLeft).on('click', function(){
                //     if(!leftLimit) {
                //         var gap = navScroll.wrapperWidth - navScroll.scrollerWidth,
                //             scrollJump = (Math.abs(navScroll.x) < scrollJumpAmout) ? 0 : (navScroll.x + scrollJumpAmout);
                //
                //         navScroll.scrollTo(scrollJump, 0, 50, IScroll.utils.ease.quadratic);
                //     }
                // });
                // (arrowRight).on('click', function(){
                //     if(!rightLimit) {
                //         var gap = navScroll.wrapperWidth - navScroll.scrollerWidth,
                //             scrollJump = (Math.abs(navScroll.x - gap) < scrollJumpAmout) ? gap : (navScroll.x - scrollJumpAmout);
                //
                //         navScroll.scrollTo(scrollJump, 0, 50, IScroll.utils.ease.quadratic);
                //     }
                // });

                // Now scroll to current item
                navScroll.scrollToElement('.spec-nav-tabs li.current', null, true, null, false);
                navScroll.on('scrollEnd', function () {
                    var scrollLimit = this.wrapperWidth - this.scrollerWidth;

                    if(this.x == 0) {
                        (arrowLeft).fadeOut('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = true;
                        rightLimit = false;
                    } else if(this.x == scrollLimit) {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeOut('fast');
                        leftLimit = false;
                        rightLimit = true;
                    } else {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = false;
                        rightLimit = false;
                    }
                });

                var scrollLimit = navScroll.wrapperWidth - navScroll.scrollerWidth;

                // Setup visibility of arrows
                if(navScroll.x == 0) {
                    (arrowLeft).fadeOut('fast');
                    (arrowRight).fadeIn('fast');
                } else if(navScroll.x == scrollLimit) {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeOut('fast');
                } else {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeIn('fast');
                }
            })();
        }


    // Enable side dragging spec page specification tabs
        if($('.usertabs-nav-tabs').length > 0) {
            (function() {
                var navScroll = new IScroll('.usertabs-nav-tabs', {
                    scrollX: true,
                    scrollY: false,
                    click: true,
                    useTransition: false,
                    bounce: false
                });

                // Arrows
                var container = $('.usertabs-nav-tabs').parent().find('.usertabs-nav-tabs-arrows'),
                    arrowLeft = (container).find('.left-arrow-container'),
                    arrowRight = (container).find('.right-arrow-container'),
                    leftLimit = false,
                    rightLimit = false,
                    scrollJumpAmout = 80;

                // Now scroll to current item
                navScroll.scrollToElement('.usertabs-nav-tabs li.current', null, true, null, false);
                navScroll.on('scrollEnd', function () {
                    var scrollLimit = this.wrapperWidth - this.scrollerWidth;

                    if(this.x == 0) {
                        (arrowLeft).fadeOut('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = true;
                        rightLimit = false;
                    } else if(this.x == scrollLimit) {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeOut('fast');
                        leftLimit = false;
                        rightLimit = true;
                    } else {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = false;
                        rightLimit = false;
                    }
                });

                var scrollLimit = navScroll.wrapperWidth - navScroll.scrollerWidth;

                // Setup visibility of arrows
                if(navScroll.x == 0) {
                    (arrowLeft).fadeOut('fast');
                    (arrowRight).fadeIn('fast');
                } else if(navScroll.x == scrollLimit) {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeOut('fast');
                } else {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeIn('fast');
                }
            })();
        }



    }

    // Ensure bootstrap 'Sort by:' dropdowns have current value set
    // we have to use a timer because of intermittent delays of the dom being setup via an ajax call
    var retryCount = 1,
        dropdownTimer = null,
        valFound = false;
    (function setBootstrapDropdownValues() {
        var bootDropdowns = $('.sort-by-dropdown:visible');
        (bootDropdowns).each(function() {
            var that = $(this),
                textPlaceholder = (that).find('input.form-control[type="text"]'),
                currentSelection = (that).find('.dropdown-menu .current').not('.ng-hide');

            if((currentSelection).length > 0) {
                (textPlaceholder).val((currentSelection).text().trim());
                valFound = true;
                if(dropdownTimer) { // clear loop if we've already got our value
                    clearTimeout(dropdownTimer);
                }
            }
        });

        dropdownTimer = setTimeout(function() {
            if(retryCount <= 10 && !valFound) { // limit to 10 seconds
                setBootstrapDropdownValues();
            } else {
                clearTimeout(dropdownTimer);
            }
            retryCount++;
        }, 1000);
    })();

    (supportCategoryListing).on('click', '.find-a-qual-subjects-drawer .nav-sort a', function(e) {
    // $('.find-a-qual-subjects-drawer .nav-sort').on('click', 'a', function(e) {
        e.preventDefault();
        var that = $(this),
            target = (that).attr('href').replace('#sortby-', '');

        // Clear all "current" classes and set current
        (that).parent().siblings('li').removeClass('curr');
        (that).parent().addClass('curr');

        // Toggle visible content
        (that).parents('ul').siblings('.quallist').removeClass('curr');
        (that).parents('ul').siblings('.quallist-'+target).addClass('curr');
    });

    // Loading image for AJAX loaded content
    // Notes:
    // 1) this relies on the height of the container changing after the content has loaded
    // don't forget to include a clearfix if the element only contains floated items
    // 2) Set a min-height (200px - same height as loading animation) if it's initially very small
    // 3) Set holding dom to position relative in order to position the loading gif correctly
    setTimeout(function() {
        var loadingAreas = $('.find-qualification, .specificationlist, .support-topics, .supportcategorylisting');
        (loadingAreas).each(function() {
            var loadingAreaTimer = null,
                findQualLoadingIcon = null,
                timercount = 0;

            (function loadingDetector(loadingArea) {
                if((loadingArea).find('.campaign').length > 0) {
                    var initialHeightData = (loadingArea).data('initialHeight'),
                        outerHeight = (loadingArea).outerHeight();

                    if((loadingArea).find('.content-loading').length) {
                        findQualLoadingIcon = (loadingArea).find('.content-loading');
                    } else {
                        findQualLoadingIcon = (loadingArea).append($('<div class="content-loading">LOADING</div>')).find('.content-loading');
                    }

                    // Set initial height
                    (loadingArea).data('initialHeight', (loadingArea).outerHeight());

                    loadingAreaTimer = setTimeout(function(){
                        if((loadingAreaTimer && (initialHeightData && outerHeight) && (initialHeightData !== outerHeight)) || (timercount >= 100)) { // limit to 10 seconds of waiting
                            clearTimeout(loadingAreaTimer);
                            findQualLoadingIcon.remove();
                        } else {
                            timercount++;
                            loadingDetector(loadingArea);
                        }
                    }, 100);
                }
            })($(this));
        });
    }, 100);

/*
    Function sets an update event so that when the page loads it enable the country selector dropdown once the page
    has loaded.

    When the country selector dropdown is changed then it is disabled until there are no more ajax request to return

    Sets functions to update the styles of secure content components on the page
*/
$(document).ready(function() {
//    var dropDownTimeOut, styleUpdateTimeOut;
//    //The dropdown is disabled on page render
//    var countryDD = $("select.cntrySelector");
//
//    //When the dropdown changes it needs to be disabled until the page loads
//    countryDD.change(function() {
//        //Not sure why we need to hide this  - this may have been corrected in Johns fix
//        disableDropdownUntilLoad();
//    });
//
//    //Some geo-located elements render incorrectly, these need to be corrected and set to zero
//    function initGeolocationStyles() {
//        //There are some instances where the execution of this needs to be delayed
//        $('.responsiverow .disclosure-tabs .nav-tabs > li a').equalizeHeights();
//        Pearson.initUserTabs();
//    }
//
//    //Function returns a timeout function that will reset set the country selector to be accessible
//    function updateDropdown() {
//        return setTimeout(function() {
//            initGeolocationStyles();
//
//            //clear the interval that corrects the styles on the page
//            clearInterval(styleUpdateTimeOut);
//
//            //If the function passes in here we terminate the watch events
//            $(document).unbind("ajaxStop.drpdown");
//            //enable the dropdown
//            (countryDD).attr("disabled", false);
//            (countryDD).removeClass("disable");
//        }, 2500);
//    }
//
//    function disableDropdownUntilLoad() {
//        //Disable the country selector
//        (countryDD).attr("disabled", true);
//        (countryDD).addClass("disable");
//
//        //Correct the styling on the page - delay by 500ms
//        initGeolocationStyles();
//
//        //some elements styling gets set incorrectly by geolocation, so we need to correct it
//        styleUpdateTimeOut = setInterval(function(){ $(".campaign").attr("style", "") }, 200);
//
//        //Call the timeout
//        dropDownTimeOut = updateDropdown();
//        $(document).on("ajaxStop.drpdown", function() {
//            //Since another ajax call has clear the current timeout and wait again
//            clearTimeout(dropDownTimeOut);
//
//            //Correct the styling on the page
//            initGeolocationStyles();
//
//            //Call the timeout and wait to change the dropdown again once it completes
//            dropDownTimeOut = updateDropdown();
//        });
//    }
//
//    $(window).load(function() {
//        disableDropdownUntilLoad();
//    });
});

    // Cookie policy
    var policyCookieName = 'cookiepolicy',
        policyCookie = Cookie.getCookie(policyCookieName),
        policyCookieBox = $('#cookiemsgbox');

    if(!policyCookie) {
        // Show cookie message
        (policyCookieBox).fadeIn('slow');

        // Click event to close message and set cookie
        (policyCookieBox).on('click', '.close-message', function(e) {
            Cookie.setCookie(policyCookieName, true, null);

            (policyCookieBox).fadeOut();
        });
    }
};

jQuery(document).ready(function() {
    Pearson.init(jQuery);
});



// Delay the flyout menu on hover
 $(function() {
        var megaDD = $(".navbar-primary .nav").find(".mega-dropdown");
        megaDD.on('mouseenter', function(e){ 
            $(this).siblings('li').removeClass('open');
            $(this).stop(true, true).delay(200).queue(function(){
                $(this).addClass('open').dequeue();
            }); 
        });
        megaDD.on('touchstart', function(e){ 
            megaDD.hover(function(){   
                $(this).siblings('li').removeClass('open');
                $(this).addClass('open');
            }, false);
         });

        megaDD.on('mouseleave', function(e){ 
            $(this).stop(true, true).delay(200).queue(function(){
                $(this).removeClass('open').dequeue();
            }); 
        });
});


 
/*

    // Expand specification course details 
    $('.specificationinfo .course-details').delegate('a.read-more', 'click', function(e) {
        e.preventDefault();

        var that = $(this),
            originalText = ((that).data('original-text') && (that).data('original-text').length > 1) ? (that).data('original-text') : (that).text(),            
            toggleText = ((that).data('toggle-text') && (that).data('toggle-text').length > 1) ? (that).data('toggle-text') : 'Show less';
            hiddenContent = (that).siblings('dl').find('.more-specs'),
            isHidden = (hiddenContent).is('.hidden');

// Save original text of link
        if(!(that).data('original-text')) {
             (that).data('original-text', (that).text());
        }

        if(isHidden) {
            hiddenContent.removeClass('hidden');
            hiddenContent.addClass('not-hidden');
            (that).text(toggleText);
            (that).removeClass('ctn-closed');
            (that).addClass('ctn-open');
        } else {

            hiddenContent.removeClass('not-hidden');
            hiddenContent.addClass('hidden');
            (that).text(originalText);
            (that).removeClass('ctn-open');
            (that).addClass('ctn-closed');
        }
    });

*/
   



   if($('.usertabs-nav-tabs ul li').length > 0) {
            
                var navScroll = new IScroll('.usertabs-nav-tabs', {
                    scrollX: true,
                    scrollY: false,
                    click: true,
                    useTransition: false,
                    bounce: false
                });

                // Arrows
                var container = $('.usertabs-nav-tabs').parent().find('.usertabs-nav-tabs-arrows'),
                    arrowLeft = (container).find('.left-arrow-container'),
                    arrowRight = (container).find('.right-arrow-container'),
                    leftLimit = false,
                    rightLimit = false,
                    scrollJumpAmout = 80;

                // Now scroll to current item
                navScroll.scrollToElement('.usertabs-nav-tabs li.current', null, true, null, false);
                navScroll.on('scrollEnd', function () {
                    var scrollLimit = this.wrapperWidth - this.scrollerWidth;

                    if(this.x == 0) {
                        (arrowLeft).fadeOut('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = true;
                        rightLimit = false;
                    } else if(this.x == scrollLimit) {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeOut('fast');
                        leftLimit = false;
                        rightLimit = true;
                    } else {
                        (arrowLeft).fadeIn('fast');
                        (arrowRight).fadeIn('fast');
                        leftLimit = false;
                        rightLimit = false;
                    }
                });

                var scrollLimit = navScroll.wrapperWidth - navScroll.scrollerWidth;

                // Setup visibility of arrows
                if(navScroll.x == 0) {
                    (arrowLeft).fadeOut('fast');
                    (arrowRight).fadeIn('fast');
                } else if(navScroll.x == scrollLimit) {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeOut('fast');
                } else {
                    (arrowLeft).fadeIn('fast');
                    (arrowRight).fadeIn('fast');
                }

                if($('.usertabs-nav-tabs .nav-tabs').width() < $('.usertabs').width()) {
                    $('.usertabs .left-arrow-container').hide();
                  $('.usertabs .right-arrow-container').hide();
                }

                $(window).resize(function(){
                    if($('.usertabs-nav-tabs .nav-tabs').width() < $('.usertabs').width()) {
                        $('.usertabs .left-arrow-container').hide();
                        $('.usertabs .right-arrow-container').hide();
                    }

                });
        
        }


   
 
  

